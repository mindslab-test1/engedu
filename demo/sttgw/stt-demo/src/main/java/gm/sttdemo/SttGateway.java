package gm.sttdemo;

import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.List;

import javax.websocket.MessageHandler;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.catalina.manager.util.SessionUtils;

import com.fasterxml.jackson.core.base.GeneratorBase;
import com.google.protobuf.ByteString;

@ServerEndpoint(value="/websocket/stt")
public class SttGateway {
	
	private HashMap<Session, SttClient> clients = new HashMap<Session, SttClient>();
	
	@OnOpen
	public void handleOpen(Session session) {
		session.setMaxBinaryMessageBufferSize(8192);
		System.out.println("Open Conn:" + session + ",");
		List<String> langList = session.getRequestParameterMap().get("lang");
		String lang = langList.get(0);
		
		SttClient sttclient = new SttClient("10.122.66.72", 9801, lang);
		sttclient.init(session);
		clients.put(session, sttclient);
		System.out.println("handleOpen:" + session);
//		session.removeMessageHandler(MessageHandler.Partial<ByteBuffer>);
//		session.addMessageHandler(new MessageHandler.Partial<String>() {
//			
//			private Session ss = session;
//			
//			@Override
//			public void onMessage(String base64Audio, boolean last) {
//				
//				System.out.println("onMessage:" + ss);
////				System.out.println("size="+base64Audio.length());
////				System.out.println("data="+base64Audio.split(",")[1]);
//				Decoder decoder = Base64.getDecoder();
//				byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
////				sttclient.send(decodedByte);
//				clients.get(ss).send(decodedByte);
//				System.out.println("On Partial Message:"+ decodedByte.toString());	
//
//			}
//		});
	}	
	
	@OnClose
	public void onClose(Session session) {
		try {
			clients.get(session).shutdown();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		clients.remove(session);
		System.out.println("Close Conn:" + session);
	}
	
//	@OnMessage
//	public void onMessage(byte[] pcm, boolean last, Session session) {
//		System.out.println("onMessage:" + session);
//		Decoder decoder = Base64.getDecoder();
////		byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
//		clients.get(session).send(pcm);
//		System.out.println("On Partial Message:"+ pcm.toString());	
//	}
	
	@OnMessage
	public void onMessage(String base64Audio, boolean last, Session session) {
		//System.out.println("onMessage:" + session);
		System.out.println("SttGateway onMessage:" + base64Audio);
		
		Decoder decoder = Base64.getDecoder();
		byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
		clients.get(session).sendSttClient(decodedByte);
		System.out.println("On Partial Message:"+ decodedByte.toString());	
	}
	
	@OnError
	public void onError(Throwable e) {
		e.printStackTrace();
	}
}

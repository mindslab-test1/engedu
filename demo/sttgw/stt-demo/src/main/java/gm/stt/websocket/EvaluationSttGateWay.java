package gm.stt.websocket;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.websocket.MessageHandler;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.catalina.manager.util.SessionUtils;
import org.apache.commons.collections4.CollectionUtils;

import com.fasterxml.jackson.core.base.GeneratorBase;
import com.google.protobuf.ByteString;

import gm.stt.controller.SimpleTTSController;
import lombok.extern.slf4j.Slf4j;

import gm.stt.sttclient.CommonSttClient;

@ServerEndpoint(value="/stt/websocket/evaluationStt")
@Slf4j
public class EvaluationSttGateWay {
	
	private HashMap<Session, CommonSttClient> clients = new HashMap<Session, CommonSttClient>();
	
	@OnOpen
	public void handleOpen(Session session) {
		session.setMaxBinaryMessageBufferSize(8192);
		System.out.println("Open Conn:" + session + ",");
		
		Map<String, List<String>> requestMap = session.getRequestParameterMap();
		
		String v =    			CollectionUtils.isEmpty( requestMap.get("v") ) ? "" :    requestMap.get("v").get(0);
		String biz =  			CollectionUtils.isEmpty( requestMap.get("biz") ) ? "" :    requestMap.get("biz").get(0); 
		String channel = 		CollectionUtils.isEmpty( requestMap.get("channel") ) ? "" :    requestMap.get("channel").get(0); 
		String language = 		CollectionUtils.isEmpty( requestMap.get("language") ) ? "" :    requestMap.get("language").get(0); 
		String service = 		CollectionUtils.isEmpty( requestMap.get("service") ) ? "" :    requestMap.get("service").get(0); 
		String userId =  		CollectionUtils.isEmpty( requestMap.get("userId") ) ? "" :    requestMap.get("userId").get(0); 
		String lectureId = 		CollectionUtils.isEmpty( requestMap.get("lectureId") ) ? "" :    requestMap.get("lectureId").get(0); 
		String chapterId = 		CollectionUtils.isEmpty( requestMap.get("chapterId") ) ? "" :    requestMap.get("chapterId").get(0); 
		String contentId = 		CollectionUtils.isEmpty( requestMap.get("contentId") ) ? "" :    requestMap.get("contentId").get(0); 
		String sequence =  		CollectionUtils.isEmpty( requestMap.get("sequence") ) ? "" :    requestMap.get("sequence").get(0); 
		String answerText = 	CollectionUtils.isEmpty( requestMap.get("answerText") ) ? "" :    requestMap.get("answerText").get(0); 
		String recordYn = 		CollectionUtils.isEmpty( requestMap.get("recordYn") ) ? "" :    requestMap.get("recordYn").get(0);  
		
		
		Map<String, String> sttClientParam = new HashMap<>();
		sttClientParam.put("v", v);
		sttClientParam.put("biz", biz);
		sttClientParam.put("channel", channel);
		sttClientParam.put("language", language);
		sttClientParam.put("userId", userId);
		sttClientParam.put("lectureId", lectureId);
		sttClientParam.put("chapterId", chapterId);
		sttClientParam.put("contentId", contentId);
		sttClientParam.put("sequence", sequence);
		sttClientParam.put("answerText", answerText);
		sttClientParam.put("recordYn", recordYn);
		
		
		log.info(this.getClass().getSimpleName()+" v:"+v);
		log.info(this.getClass().getSimpleName()+" biz:"+biz);
		log.info(this.getClass().getSimpleName()+" channel:"+channel);
		log.info(this.getClass().getSimpleName()+" language:"+language);

		String ip="";
		int port=0;
		try {
			Properties prop = new Properties();
			InputStream inputStream =  CommonSttClient.class.getClassLoader().getResourceAsStream("config.properties");
		
			prop.load(inputStream);
			ip = prop.getProperty("ttsServerIP");
			port  = Integer.parseInt( prop.getProperty("ttsServerPort") )  ;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		SttClient sttclient = new SttClient("10.122.66.72", 9801, language);
		CommonSttClient sttclient = new CommonSttClient(ip, port, language,"E",sttClientParam);
		sttclient.init(session);
		clients.put(session, sttclient);
		System.out.println("handleOpen:" + session);
//		session.removeMessageHandler(MessageHandler.Partial<ByteBuffer>);
//		session.addMessageHandler(new MessageHandler.Partial<String>() {
//			
//			private Session ss = session;
//			
//			@Override
//			public void onMessage(String base64Audio, boolean last) {
//				
//				System.out.println("onMessage:" + ss);
////				System.out.println("size="+base64Audio.length());
////				System.out.println("data="+base64Audio.split(",")[1]);
//				Decoder decoder = Base64.getDecoder();
//				byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
////				sttclient.send(decodedByte);
//				clients.get(ss).send(decodedByte);
//				System.out.println("On Partial Message:"+ decodedByte.toString());	
//
//			}
//		});
	}	
	
	@OnClose
	public void onClose(Session session) {
		try {
			clients.get(session).shutdown();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		clients.remove(session);
		System.out.println("Close Conn:" + session);
	}
	
//	@OnMessage
//	public void onMessage(byte[] pcm, boolean last, Session session) {
//		System.out.println("onMessage:" + session);
//		Decoder decoder = Base64.getDecoder();
////		byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
//		clients.get(session).send(pcm);
//		System.out.println("On Partial Message:"+ pcm.toString());	
//	}
	
	@OnMessage
	public void onMessage(String base64Audio, boolean last, Session session) {
		//System.out.println("onMessage:" + session);
		System.out.println("SttGateway onMessage:" + base64Audio);
		
		Decoder decoder = Base64.getDecoder();
		byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
		clients.get(session).sendSttClient(decodedByte);
		System.out.println("On Partial Message:"+ decodedByte.toString());	
	}
	
	@OnError
	public void onError(Throwable e) {
		e.printStackTrace();
	}
}

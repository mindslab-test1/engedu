package gm.stt.vo;

import lombok.Data;

@Data
public class SttResponse {
	
	private String resCode;
	private String resMsg;
	private Result result;

}

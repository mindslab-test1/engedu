package gm.stt.vo;

import lombok.Data;

@Data
public class Result {
	
	private String resultId="";
	private String score="";
	private String scoreDetail="";
	private String answerText="";
	private String userText="";
	private String recordUrl="";
	private String ttsUrl="";
	private String ttsText="";
	private String extType="";
	private String extData="";
	
	
}

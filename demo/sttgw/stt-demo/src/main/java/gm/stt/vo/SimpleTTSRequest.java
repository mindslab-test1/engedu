package gm.stt.vo;

import lombok.Data;

@Data
public class SimpleTTSRequest {

	private String v;
	private String biz;
	private String channel;
	private String language;
	
	private String userId;
	private String lectureId;
	private String chapterId;
	private String contentId;
	private String script;
	
}

package gm.stt.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import gm.stt.vo.Result;
import gm.stt.vo.SimpleTTSResponse;
import gm.stt.vo.SimpleTTSRequest;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping(value="/tts")
public class SimpleTTSController {

	@RequestMapping(value="/simpleTts", method=RequestMethod.POST)
	@ResponseBody
	public SimpleTTSResponse simpleTTS(SimpleTTSRequest simpleTtsRequest, HttpServletRequest request) {
		log.info(this.getClass().getSimpleName()+" simpleTTS");
		log.info(this.getClass().getSimpleName()+" simpleTTS ttsRequest.ttsRequest():"+simpleTtsRequest.toString());
		
		String scheme = request.getScheme();  
		String serverName = request.getServerName();  
		int serverPort  = request.getServerPort();
		String contextPath = request.getContextPath();
		
		StringBuilder url = new StringBuilder();
	    url.append(scheme).append("://").append(serverName);
	    
	    if (serverPort != 80 && serverPort != 443) {
	        url.append(":").append(serverPort);
	    }
	    
	    url.append(contextPath);
	    url.append("/SampleAudio.mp3");
	    
	    log.info(this.getClass().getSimpleName()+" full url:"+ url.toString());
		
		SimpleTTSResponse simpleTTSResponse = new SimpleTTSResponse();
		simpleTTSResponse.setResCode("200");
		simpleTTSResponse.setResMsg("SUCCESS");
		
		Result result = new Result();
		result.setTtsUrl("uapi/SampleAudio.mp3");
		
		simpleTTSResponse.setResult(result);
		
		return simpleTTSResponse;
	}
}

#ifndef __ENG_EV_SERVER_H__
#define __ENG_EV_SERVER_H__

#include "cinclude.h"
#include "cm_log.h"
#include "cm_cfg.h"
#include "queue.h"

#define LOGDIR          "../logs"
#define LOGFILE_NAME    "ees"

#endif // __ENG_EV_SERVER_H__

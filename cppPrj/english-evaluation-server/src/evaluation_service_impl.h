#ifndef  __EVALUATION_SERVICE__ 
#define __EVALUATION_SERVICE__ 

#include "cinclude.h"
#include "channel.h"
#include <grpc++/grpc++.h>
#include "english_evaluation.grpc.pb.h"


using engedu::eng_evaluation::EvaluationService;
using ::grpc::ServerContext;
using ::grpc::ServerReader;
using ::grpc::Status;
using ::grpc::ServerReaderWriter;
using ::grpc::ClientReaderWriter;
using ::engedu::eng_evaluation::EvaluationRequest;
using ::engedu::eng_evaluation::EvaluationResponse;

#define MAXCHAR 1024
#define MAX_LEN_PATHNAME 1024

typedef void Laser;
/*
typedef struct
{
    void* m_arec;
    void** m_agset;
    void* m_gbuilder;
    struct _VocabSet* m_vs;
    void* m_uv;

    struct _pRuleSet* m_ruleA;
    unsigned int m_opt;

    float m_sentthresh;
#ifdef _USE_PTHREAD_MUTEX_
    pthread_mutex_t m_mutex;
#endif
} CONFMEASURE;
*/

typedef struct 
{
    float* m_buf;
    int m_sz;
    int m_rp;
    int m_wp;
    int m_eof;
#ifdef _USE_PTHREAD_MUTEX_
    pthread_mutex_t m_mutex;
#endif
} FEAT_RING;


typedef struct
{
    Laser* m_pLASER_csr;
    Laser* m_pLASER_cwr;
    //void *m_proneval;
    char *m_servercfg;
    pthread_mutex_t m_mutex_cm;
    int m_nbest;
    int m_haswb;
} MASTERCFG;

typedef struct
{
    FEAT_RING* m_feat;
    unsigned int m_featdim;
    MASTERCFG* m_master;
    Laser* m_pChild_csr;
    Laser* m_pChild_cwr;
    CONFMEASURE *m_cm;
    char m_inpcmdir[MAX_LEN_PATHNAME];
    pthread_mutex_t m_mutex_cm;
    char* m_logid;
    char* m_type;
    int m_laserDNN;
    int m_miniBatch;
    int m_status;
} DECODERCFG;

class EvaluationServiceImpl final: public EvaluationService::Service {
    private:
    void* pronStruct;
    char myName[32];
    ChDev **chDevice;
    int  maxChannel;
    int  reqCounter;

    //ETRI_SpeechPlot_IntonationStruct *p_intonationStruct;


	void logError(int, const char*, int, const char*, ...);
    void initialize();
    ChDev* getAvailableChDev();

    public:
    EvaluationServiceImpl();
    virtual ~EvaluationServiceImpl();

    Status SimpleAnalyze(ServerContext* context, const EvaluationRequest* request, 
                         EvaluationResponse* response);
    Status GrammerAnalyze(ServerContext* context, const EvaluationRequest* request, 
                          EvaluationResponse* response);
    Status PronAnalyze(ServerContext* context, const EvaluationRequest* request, 
                       EvaluationResponse* response);

};

#endif

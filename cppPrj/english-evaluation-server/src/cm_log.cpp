/////////////////////////////////////////////////////////////////////////////
// cm_log.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////

#include "cm_log.h"

//#define BUFFERED_LOG 
#define LOG_FILENAME_LINE


Log* Log::__log = NULL;


_CRITICAL_SECTION    log_lock;


const char *logTypeName[] = {
    "DEBUG", 
    "INFO ", 
    "WARN ", 
    "ERROR", 
    "FATAL", 
    "EXIT " 
};


/////////////////////////////////////////////////////////////////////////////
// Log class
/////////////////////////////////////////////////////////////////////////////
Log::Log()
{
    logBuf1 = NULL;
    logBuf2 = NULL;
    logBuf3 = NULL;

    int  size = 4*128*1024;

    logQueue.makeBuf(size);
    logBuf1 = new char[size+1024];
    logBuf2 = new char[size+1024];
    logBuf3 = new char[size+1024];

    _initializeCriticalSection(&log_lock);

    logFile = NULL; 

    logThreshold = levelInfo;
    logBuffered  = 0;
    for(int i=0 ; i<24 ; i++)
        logUpdateTime[i] = -1;
    logUpdateTime[0] = 3;
    errorCount = 0;
}

Log::~Log()
{
    if(logBuf1 != NULL)  delete[] logBuf1;
    if(logBuf2 != NULL)  delete[] logBuf2;
    if(logBuf3 != NULL)  delete[] logBuf3;
}

/////////////////////////////////////////////////////////////////////////////
// Log class member function implemetation
/////////////////////////////////////////////////////////////////////////////
void Log::start(int threshold, const char *logname)
{
    strcpy(logFileName, logname);
    updateFile();

    logEnable = 1;
    logThreshold = threshold;

    pthread_t  tid;
    pthread_create(&tid, NULL, logOnFile, this);
}

void Log::stop()
{
    logEnable = 0;
}

void Log::setLogThreshhold(int th)
{
    _enterCriticalSection(&log_lock);
    logThreshold = th;
    _leaveCriticalSection(&log_lock);
}

void Log::setLogBuffered(int buffered)
{
    _enterCriticalSection(&log_lock);
    logBuffered = buffered;
    _leaveCriticalSection(&log_lock);
}
void Log::setLogUpdateTime(int* period)
{
    _enterCriticalSection(&log_lock);
    for(int i = 0 ; i < 24 ; i++) {
        logUpdateTime[i] = period[i];
    }
    _leaveCriticalSection(&log_lock);
}

char *Log::vlogError(int level, const char* fileName, int lineNum, int ch,
	const char *name, const char *errorFmt, va_list args) 
{
    _enterCriticalSection(&log_lock);

    struct timeval tp;

    gettimeofday(&tp, NULL);
    memmove(&dateTime, localtime(&tp.tv_sec), sizeof(struct tm));

    if(logThreshold >= levelInfo) {
	sprintf(logBuf1, "%02d:%02d:%02d %s [%s] ", 
		dateTime.tm_hour, dateTime.tm_min, dateTime.tm_sec,
		logTypeName[level], name);
    }
    else {
	sprintf(logBuf1, "%4d %02d:%02d:%02d.%02d %s [%s] ", 
		lineNum, 
		dateTime.tm_hour, dateTime.tm_min, dateTime.tm_sec,
		tp.tv_usec/(10*1000), logTypeName[level], name);
    }


    vsprintf(logBuf2, errorFmt, args);
    strcat(logBuf1, logBuf2);
    strcat(logBuf1, "\n");

    int len = strlen(logBuf1);
    if(logBuffered) {
	logQueue.push(logBuf1, len);
    }
    else {
	if(logFile != NULL) {
	    fputs(logBuf1, logFile);
	    fflush(logFile);
	}
    }

    if(level >= levelError) {
	++errorCount;
    }

    printf(logBuf1);

    _leaveCriticalSection(&log_lock);

    return logBuf1;
}

void Log::externalLog(char *logString)
{
    logQueue.push(logString, strlen(logString));
}

void Log::writeOnFile()
{
    int  size;

    while(logEnable) {
	sleep(1);

	if( (logBuffered) &&
		((size=logQueue.getDataSize()) > 0) ) {
	    logQueue.pop(logBuf3, size);
	    logBuf3[size] = 0;
	    if(logFile != NULL) {
		fputs(logBuf3, logFile);
	    }
	}

	updateFile();
    }

    if(logFile != NULL) {
	fclose(logFile);
    }

    //pthread_exit();
}

void Log::updateFile() 
{
    _enterCriticalSection(&log_lock);

    time_t longTime;
    time(&longTime);
    memmove(&dateTime, localtime(&longTime), sizeof(struct tm));
    timeUpdateFlag = 0;
    
    if(dateTime.tm_min == 0) { 
        for(int i = 0 ; i < 24 ; i++ ) {
            if(dateTime.tm_hour == logUpdateTime[i]) {
                timeUpdateFlag = 1;
                break;
            }
        }
    }


    if((logFile!=NULL) && 
       ((updatedFlag!=0) || !timeUpdateFlag || (dateTime.tm_min!=0))) { 
       //((updatedFlag!=0) || (dateTime.tm_hour!=3) || (dateTime.tm_min!=0))) { 
	if(dateTime.tm_min != 0) {
	    updatedFlag = 0;
	}
	_leaveCriticalSection(&log_lock);
	return;
    }

    if(logFile != NULL) {
	if( fclose(logFile) )
	    printf("logFile(%d) was not closed", logFile);
    }

    char filename[256];
    sprintf(filename,  "%s[%4d%02d%02d_%02d%02d%02d].log", 
	    logFileName,
	    dateTime.tm_year+1900, dateTime.tm_mon+1, dateTime.tm_mday, 
	    dateTime.tm_hour, dateTime.tm_min, dateTime.tm_sec);

    logFile = fopen(filename, "w");	
    if(logFile == NULL) {
	printf("file(%s) open error\n", filename);
	perror("file open error");
    }

    updatedFlag = 1;

    _leaveCriticalSection(&log_lock);
}

/////////////////////////////////////////////////////////////////////////
void *Log::logOnFile(void *_this) 
{
    ((Log*)_this)->writeOnFile();
}

void Log::flush()
{
    if(logFile != NULL) {
        fputs(logBuf1, logFile);
        fflush(logFile);
    }
}

///////////////////////////////////////////////////////////////////////
void logError(int level, const char* fileName, int lineNum, 
	const char* errorFmt, ...)
{
    va_list args;
    va_start(args, errorFmt);
    logger.vlogError(level, fileName, lineNum, 0, "@@@", errorFmt, args);
    va_end(args);
}

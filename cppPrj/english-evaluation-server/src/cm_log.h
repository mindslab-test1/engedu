// mylog.h : header file
//
#ifndef __MYLOG_H__
#define __MYLOG_H__


#include "cinclude.h"
#include "cm_thread.h"
#include "queue.h"

void logError(int level, const char* fileName, int lineNum, 
	const char* errorFmt, ...);

#define logger (Log::instance())

/////////////////////////////////////////////////////////////////////////
// Log class
/////////////////////////////////////////////////////////////////////////

class Log 
{
    protected:
	Log();	// standard constructor
	virtual ~Log();

    public:
	static Log& instance()
	{
	    if(__log == NULL)
		__log = new Log;
	    return *__log;
	}

	static void free()
	{
	    if(__log != NULL) {
		delete __log;
		__log = NULL;
	    }
	}

	void start(int, const char *);
	void stop();

        int  getLogThreshold() { return logThreshold; }    
        int  getLogBuffered() { return logBuffered; }    
	void setLogThreshhold(int th);
	void setLogBuffered(int buffered);
        void setLogUpdateTime(int*);

	char *vlogError(int, const char*, int, int, const char *, const char *,
		       va_list args);

	void externalLog(char *);
	void writeOnFile();
	void updateFile();
        void flush();

	static void *logOnFile(void *);

    private:
        static Log *__log;

        int     logEnable;
	FILE*	logFile;
	char    logFileName[256];
	int     updatedFlag;
	int     timeUpdateFlag;

	int	logBuffered;	
	int	logThreshold;	
        int     logUpdateTime[24];
	char    *logType;
	char    *logBuf1, *logBuf2;

	DataQueue  logQueue;
	char       *logBuf3;

	struct tm dateTime;

	unsigned long errorCount;
};




///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
enum {
    levelDebug,		// no error, just debugging message
    levelInfo,
    levelWarning,	// warning message, can be ignored
    levelError,         // error
    levelFatal,		// fatal error, further operation not guaranteed
    levelExit		// fatal error and exit. 
};


///////////////////////////////////////////////////////////////////////
// Info#() macros:
//

#define Info(errorMsg) \
{ if (levelInfo>=logger.getLogThreshold())  \
    logError(levelInfo, __FILE__, __LINE__, errorMsg ); } 

#define Info1(errorMsg, arg1) \
{ if (levelInfo>=logger.getLogThreshold())  \
    logError(levelInfo, __FILE__, __LINE__, errorMsg, arg1 ); } 

#define Info2(errorMsg, arg1, arg2) \
{ if (levelInfo>=logger.getLogThreshold()) \
    logError(levelInfo, __FILE__, __LINE__, errorMsg, arg1, arg2 ); }

#define Info3(errorMsg, arg1, arg2, arg3) \
{ if (levelInfo>=logger.getLogThreshold()) \
    logError(levelInfo, __FILE__, __LINE__, errorMsg, arg1, arg2, arg3); }

#define Info4(errorMsg, arg1, arg2, arg3, arg4) \
{ if (levelInfo>=logger.getLogThreshold()) \
    logError(levelInfo, __FILE__, __LINE__, errorMsg, arg1, arg2, arg3, arg4 ); }

#define Info5(errorMsg, arg1, arg2, arg3, arg4, arg5) \
{ if (levelInfo>=logger.getLogThreshold()) \
    logError(levelInfo, __FILE__, __LINE__, errorMsg, \
	    arg1, arg2, arg3, arg4, arg5 ); }



///////////////////////////////////////////////////////////////////////
// Debug#() macros:
//

#define Debug(errorMsg) \
{ if (levelDebug>=logger.getLogThreshold())  \
    logError(levelDebug, __FILE__, __LINE__, errorMsg ); } 

#define Debug1(errorMsg, arg1) \
{ if (levelDebug>=logger.getLogThreshold())  \
    logError(levelDebug, __FILE__, __LINE__, errorMsg, arg1 ); } 

#define Debug2(errorMsg, arg1, arg2) \
{ if (levelDebug>=logger.getLogThreshold()) \
    logError(levelDebug, __FILE__, __LINE__, errorMsg, arg1, arg2 ); }

#define Debug3(errorMsg, arg1, arg2, arg3) \
{ if (levelDebug>=logger.getLogThreshold()) \
    logError(levelDebug, __FILE__, __LINE__, errorMsg, arg1, arg2, arg3); }

#define Debug4(errorMsg, arg1, arg2, arg3, arg4) \
{ if (levelDebug>=logger.getLogThreshold()) \
    logError(levelDebug, __FILE__, __LINE__, errorMsg, arg1, arg2, arg3, arg4 ); }

#define Debug5(errorMsg, arg1, arg2, arg3, arg4, arg5) \
{ if (levelDebug>=logger.getLogThreshold()) \
    logError(levelDebug, __FILE__, __LINE__, errorMsg, \
	    arg1, arg2, arg3, arg4, arg5 ); }



///////////////////////////////////////////////////////////////////////////
// Error Message
//
#define Error(errorMsg) \
{ if (levelError>=logger.getLogThreshold())  \
    logError(levelError, __FILE__, __LINE__, errorMsg ); } 

#define Error0(errorMsg) \
{ if (levelError>=logger.getLogThreshold())  \
    logError(levelError, __FILE__, __LINE__, errorMsg ); } 

#define Error1(errorMsg, arg1) \
{ if (levelError>=logger.getLogThreshold())  \
    logError(levelError, __FILE__, __LINE__, errorMsg, arg1 ); } 

#define Error2(errorMsg, arg1, arg2) \
{ if (levelError>=logger.getLogThreshold()) \
    logError(levelError, __FILE__, __LINE__, errorMsg, arg1, arg2 ); }

#define Error3(errorMsg, arg1, arg2, arg3) \
{ if (levelError>=logger.getLogThreshold()) \
    logError(levelError, __FILE__, __LINE__, errorMsg, arg1, arg2, arg3); }

#define Error4(errorMsg, arg1, arg2, arg3, arg4) \
{ if (levelError>=logger.getLogThreshold()) \
    logError(levelError, __FILE__, __LINE__, errorMsg, arg1, arg2, arg3, arg4 ); }

#define Error5(errorMsg, arg1, arg2, arg3, arg4, arg5) \
{ if (levelError>=logger.getLogThreshold()) \
    logError(levelError, __FILE__, __LINE__, errorMsg, \
	    arg1, arg2, arg3, arg4, arg5 ); }


#endif // __MYLOG_H__

#include <iostream>
#include <stdio.h>

#include <grpc++/grpc++.h>
#include "tts_gw.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

using engedu::tts_gw::TtsService;
using engedu::tts_gw::TtsRequest;
using engedu::tts_gw::TtsResponse;

using namespace std;

int main(int argc, char** argv) {
    auto channel = grpc::CreateChannel("127.0.0.1:9994", grpc::InsecureChannelCredentials());
    auto stub = TtsService::NewStub(channel);
    if(argc < 2) {
        cout << "Usage: tts_client lang[ENG|KOR] script filenpath" << endl;
        return 0;
    }

    ClientContext ctx;

    TtsResponse resp;

    TtsRequest req;
    req.set_lang(argv[1]);
    req.set_script(argv[2]);
    req.set_filename(argv[3]);

    Status status = stub->SimpleTts(&ctx, req, &resp);

    if(status.ok()) {
//        std::cout << "result: " << resp.result_code << " , " << resp.score << " , " << resp.sentence
//        << std::endl;
        std::cout << resp.DebugString() << std::endl;
    } 
    else {
        std::cout << "failed: " << status.error_message() << std::endl;
    }
    return 0;
}

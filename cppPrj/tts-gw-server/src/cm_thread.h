
#ifndef __MYTHREAD_H__
#define __MYTHREAD_H__

#include <stdlib.h>
#include <unistd.h>

#define _CRITICAL_SECTION   	        pthread_mutex_t 	

#ifdef MULTITHREAD
#define _initializeCriticalSection(x)   pthread_mutex_init(x,NULL) 	
#define _enterCriticalSection(x)        pthread_mutex_lock(x)	
#define _leaveCriticalSection(x)        pthread_mutex_unlock(x)	
#else
#define _initializeCriticalSection(x)   {}	
#define _enterCriticalSection(x)        {}	
#define _leaveCriticalSection(x)        {}	
#endif

#endif // __MYTHREAD_H__


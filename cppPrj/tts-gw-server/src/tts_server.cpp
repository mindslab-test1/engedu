#include <iostream>
#include <unistd.h>
#include <getopt.h>
#include <grpc++/grpc++.h>
#include <csignal>

#include "tts_server.h"
#include "tts_service_impl.h"


using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using namespace std;

int checkDuplication(int);
int atoaddr(const char *address, struct in_addr *addr);

volatile std::sig_atomic_t g_signal_status;

void HandleSignal(int signal) {
    Debug1("handling %d", signal);
    g_signal_status = signal;
    exit(0);
}

void RunServer() {
    std::string server_address("0.0.0.0:");
    server_address += to_string(cmCfg.getSystemPort());
    Debug1("System URL=%s", server_address.c_str());
    int grpc_timeout = 2147483647;

    TtsServiceImpl service;
    ServerBuilder builder;
    builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_IDLE_MS, grpc_timeout);
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

    builder.RegisterService(&service);

    unique_ptr<Server> server(builder.BuildAndStart());
    if(server) {
        Info2("%s listening on %s", "tts_server", server_address.c_str());
        server->Wait();
    }
}

int init() {
    if(cmCfg.readCfg() < 0) {
        return -1;
    }
    return 0;
}

int main(int argc, char *argv[]) {

    if(argc == 2) {
        if(strcmp(argv[1], "-v") == 0) {
            printf("%s %s\n", __DATE__, __TIME__);
            return 0;
        }
    }

    FILE *f;
    char *p, logPath[256];

    if((f=fopen("../conf/tts.cfg", "rt")) == NULL) {
        if((p=getenv("ENG_EV_HOME")) == NULL) {
            printf("file open error : tts.cfg");
            return 0;
        }
        sprintf(logPath, "%s/%s/", p, LOGDIR);
    }
    else {
        fclose(f);
        sprintf(logPath, "./%s/", LOGDIR);
    }
    printf("logfile directory = %s\n", logPath);

    mkdir(logPath, S_IRWXU|S_IRWXG|S_IRWXO);
    strcat(logPath, LOGFILE_NAME);

    logger.start(levelDebug, logPath);

    if(init() < 0) {
        return -1;
    }

    if(checkDuplication(cmCfg.getSystemPort()) != 0) {
        printf("TTS already running...\n");
        return 0;
    }

    std::signal(SIGINT, HandleSignal);
    std::signal(SIGSEGV, HandleSignal);
    std::signal(SIGBUS, HandleSignal);
    std::signal(SIGTERM, HandleSignal);


    if(cmCfg.getLogLevel() == 1) {
        logger.setLogThreshhold(levelDebug);
    }
    else {
        logger.setLogThreshhold(levelInfo);
    }
    
    // INIT socket
    StartWSA();

    RunServer();

    CleanWSA();

    return 0;
}

int checkDuplication(int portNumber)
{
    int  sockfd;

    if((sockfd=socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("socket create error, %s\n", strerror(errno));
        return -1;
    }

    int                 result;
    struct in_addr      addr;
    struct sockaddr_in  servaddr;

    if((result=atoaddr("localhost", &addr)) < 0) {
        printf("error : atoaddr() = %d\n", result);
        close(sockfd);
        return -1;
    }
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = addr.s_addr;  
    servaddr.sin_port = htons(portNumber);

    result = connect(sockfd, (struct sockaddr *) &servaddr,
            sizeof(servaddr));

    if(result < 0) {
        printf("connect() failed, %s\n", strerror(errno));
        close(sockfd);
        return 0;
    }

    close(sockfd);
    return 1;   
}

int atoaddr(const char *address, struct in_addr *addr)
{
    struct hostent *host;
    struct in_addr *p;

    if((addr->s_addr = inet_addr(address)) != -1)
        return 0;

    if((host = gethostbyname(address)) == NULL) {
        printf("error : gethostbyname(%s) = NULL\n", address);
        return -1;
    }

    p = (struct in_addr *)*(host->h_addr_list);
    addr->s_addr = p->s_addr; 

    return 0;
}


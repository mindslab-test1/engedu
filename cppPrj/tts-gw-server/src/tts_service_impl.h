#ifndef  __EVALUATION_SERVICE__ 
#define __EVALUATION_SERVICE__ 

#include "cinclude.h"
#include "channel.h"
#include <grpc++/grpc++.h>
#include "tts_gw.grpc.pb.h"


using engedu::tts_gw::TtsService;
using ::grpc::ServerContext;
using ::grpc::ServerReader;
using ::grpc::Status;
using ::grpc::ServerReaderWriter;
using ::grpc::ClientReaderWriter;
using ::engedu::tts_gw::TtsRequest;
using ::engedu::tts_gw::TtsResponse;

#define MAXCHAR 1024
#define MAX_LEN_PATHNAME 1024


class TtsServiceImpl final: public TtsService::Service {
    private:
    char myName[32];
    ChDev **chDevice;
    int  maxChannel;
    int  reqCounter;

	void logError(int, const char*, int, const char*, ...);
    void initialize();
    ChDev* getAvailableChDev();

    public:
    TtsServiceImpl();
    virtual ~TtsServiceImpl();

    Status SimpleTts(ServerContext* context, const TtsRequest* request, 
                         TtsResponse* response);
    Status Tts(ServerContext* context, const TtsRequestEx* request,
                         TtsResponse* response);
};

#endif

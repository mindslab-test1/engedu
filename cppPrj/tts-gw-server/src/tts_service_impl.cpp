#include "tts_service_impl.h"
#include "cm_log.h"
#include "cm_cfg.h"
#include "cm_thread.h"
#include <thread>

using std::string;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::Status;
using grpc::ServerReaderWriter;
using grpc::ClientReaderWriter;
using ::engedu::tts_gw::TtsRequest;
using ::engedu::tts_gw::TtsResponse;

using namespace engedu::tts_gw;

_CRITICAL_SECTION   ch_lock;

TtsServiceImpl::TtsServiceImpl() {
    _initializeCriticalSection(&ch_lock);
    strcpy(myName, "tts");
    Info("TtsServiceImpl START");
    initialize();

}

TtsServiceImpl::~TtsServiceImpl() {
    Info("TtsServiceImpl END");
}

void TtsServiceImpl::initialize() {
    reqCounter = 0;
    chDevice = NULL;
    maxChannel = cmCfg.getLimitMaxChannel();

    chDevice = (ChDev**)new ChDev[maxChannel];
    for(int i=0; i < maxChannel; i++) {
        chDevice[i] = new ChDev;
        chDevice[i]->openChDev(i+1);
    }
}

ChDev* TtsServiceImpl::getAvailableChDev() {

    _enterCriticalSection(&ch_lock);

    for(int i=0; i<maxChannel; i++) {
        if(chDevice[i]->isAvailable() == TRUE) {
            chDevice[i]->setWorking(TRUE);
            _leaveCriticalSection(&ch_lock);
            return chDevice[i];
        }
    }

    _leaveCriticalSection(&ch_lock);

    return NULL;
}

Status TtsServiceImpl::SimpleTts(ServerContext* context, const TtsRequest* request, TtsResponse* response) {
    reqCounter ++;
    if(reqCounter % 10 == 0) {
        int workingChannel = 0;
        for(int i=0 ; i<maxChannel ; i++) {
            if(chDevice[i]->isAvailable() == FALSE) {
                workingChannel ++;
            }
        }
        Info2("Total Channel:%d, Working Channel:%d", maxChannel, workingChannel);
        reqCounter = 0;
    }

    Info("SimpleTts START");

    ChDev* chdev = getAvailableChDev();
    if(chdev != NULL) {
        chdev->doSimpleTts(request, response);
        chdev->setWorking(FALSE);
    }
    else {
        response->set_result_code(-1);
        response->set_result_msg("FAIL:NO AVAILABLE CHANNEL");
        response->set_script(request->script());
        response->set_filename("");
    }

    Info1("SimpleTts END:\n%s", response->DebugString().c_str());

    return Status::OK;
}

Status TtsServiceImpl::Tts(ServerContext* context, const TtsRequestEx* requestEx, TtsResponse* response) {
    reqCounter ++;
    if(reqCounter % 10 == 0) {
        int workingChannel = 0;
        for(int i=0 ; i<maxChannel ; i++) {
            if(chDevice[i]->isAvailable() == FALSE) {
                workingChannel ++;
            }
        }
        Info2("Total Channel:%d, Working Channel:%d", maxChannel, workingChannel);
        reqCounter = 0;
    }

    Info("Tts START");

    ChDev* chdev = getAvailableChDev();
    if(chdev != NULL) {
        chdev->doTts(requestEx, response);
        chdev->setWorking(FALSE);
    }
    else {
        response->set_result_code(-1);
        response->set_result_msg("FAIL:NO AVAILABLE CHANNEL");
        response->set_script("");
        response->set_filename("");
    }

    Info1("Tts END:\n%s", response->DebugString().c_str());

    return Status::OK;
}

void TtsServiceImpl::logError(int level, const char* fileName, int lineNum,
        const char* errorFmt, ...)
{
    va_list args;
    va_start(args, errorFmt);
    logger.vlogError(level, fileName, lineNum, 0, myName, errorFmt, args);
    va_end(args);
}


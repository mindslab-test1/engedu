<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />

    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />

    <script  src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script  src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>
	<script type="text/javascript" src="${path}/resources/js/common.js"></script>


</head>
<body>
<script>

    function inintEvent(){

        $("#searchBtn").on('click', function(e){
            searchDick();
        });


        $("#searchText").on('keyup', function(e){
            if(e.keyCode == 13){
                searchDick();
            }
        });


        $('.lyr_add').on('click',function(){
			$('.lyrWrap').fadeIn(300);
			$('#lyr_add').show();
			$('#lyr_modify').hide();
			$('#lyr_delete').hide();
		});

    }


    function createGrid(){
        $("#grid").jqGrid({
            url : serverUrl('view/game/speed/diclist'),
            mtype : "POST",
            datatype : "json",
            postData : {},
            colModel : [
                {label : 'No.', name : 'speedId', width : '10%', align : 'center'},
                {label : '단어', name : 'word', width : '30%', align : 'center'},
                {label : 'Model', name : 'sttModel', width : '30%', align : 'center'},
                {label : '사용여부', name : 'useYn', width : '10%', align : 'center', sortable: false},
                {label : 'Action', name : '', width : '20%', align : 'center', sortable: false,  formatter : updateFormatter}
            ],
            rowNum : 50,
            rowList : [50, 100, 150],
            width : '1250',
            height : '350',
            pager : '#pager',
            gridview : true,
            autoencode:true,
            shrinkToFit : true,
            sortname:'speedId',
            sortorder: 'asc',
            loadComplete : function(data) {
                 customResize('grid');
            }
        });
    }


    function updateFormatter(cellvalue, options, rowObject){


        var updateImage = '<%=request.getContextPath()%>/resources/images/ico_edit_bk.png';
        var delImage = '<%=request.getContextPath()%>/resources/images/ico_delete_bk.png';
        var html  ='';

        html +=  "<ul class=\"btn_lst\">" +
			       "<li><button type=\"button\" onclick=\"showUpdateView('"+rowObject.speedId+"');\" class=\"btn_type_w lyr_mfy\"><img src="+updateImage+" alt=\"수정\">수정</button></li>"
			   +"<li><button type=\"button\" onclick=\"deleteSpeedGameAdmin('"+rowObject.speedId+"');\" class=\"btn_type_w lyr_mfy\"><img src="+delImage+" alt=\"삭제\">삭제</button></li></ul>"

		return  html;
    }

    function searchDick(){
        var param ={
            searchType : $("#ex_select option:selected").val(),
            searchText:  $("#searchText").val()
        };
        reloadGrid(param);
    }


    function reloadGrid(param){
        console.log(param);
        $('#grid').clearGridData();
        $('#grid').setGridParam({
            postData: param
        }).trigger("reloadGrid");
    }
    

    function showUpdateView(speedId){

        $('.lyrWrap').fadeIn(300);
        $('#lyr_modify').show();
        $('#lyr_add').hide();
        $('#lyr_delete').hide();

        var param ={
            searchKey :"speedId",
            searchValue: speedId
        }

        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/game/speed/selectDetail"),
            data : param,
            success: function(data){

                $( '#data_tbody').empty();

                var result = data['data'];
                var $table = $("#data_tbody");

                var $tr = null;
                var $td = null;
                var $th = null;

                $.each(result, function(key, value){
                    $tr = $('<tr/>');
                    $td = $('<td/>');
                    $th = $('<th/>');

                    $th.attr('scope', 'row');

                    if(key =='speedId'){
                        $th.text('No.');
                    }else if(key =='word'){
                        $th.text('단어');
                    }else if(key =='sttModel'){
                        $th.text('Model');
                    }else if(key =='useYn') {
                        $th.text('사용여부');
                    }

                    if(value == null || value == 'undefined'){
                        value ='';
                    }

                    if(key =='speedId'){
                        $td.html('<span id="'+key +'">'+ value +'</span>');
                    }else if(key =='word' || key =='sttModel' ){
                        $td.html('<input type="text"  id="'+ key +'" class="ipt_txt" value="'+value+'">');
                    }else if(key =='useYn'){
                        var $select = $('<select id="'+ key +'"/>');
                        var yOpt = $('<option></option>');
                        var NOpt = $('<option></option>');

                        yOpt.attr({'value' : 'Y'});
                        yOpt.append('Y');

                        NOpt.attr({'value' : 'N'});
                        NOpt.append('N');

                        if(value.toUpperCase() ==='Y'){
                            yOpt.attr("selected", "selected");
                        }else{
                            NOpt.attr("selected", "selected");
                        }

                        $select.append(yOpt);
                        $select.append(NOpt);
                        $td.append($select);

                    }else{
                        $td.html('<input type="text" id="'+ key +'" class="ipt_txt" value="'+value+'">');
                    }

                    $tr.append($th);
                    $tr.append($td);
                    $table.append($tr);

                });


            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }



    function cancelUpdateDic(type){


        if(type === 'add'){

            $('#add_word').val('');
            $('#add_sttModel').val('');
            $('#add_useYn option:eq(0)').prop('selected', true);
            $('.lyrWrap').fadeOut(300);
            $('#lyr_add').hide();

        }else{

            $('.lyrWrap').fadeOut(300);
            $('#lyr_modify').hide();
            $( '#data_tbody').empty();
        }
    }

    function updateSpeedGameAdmin(type){

        var param ={};
        var server_url = '';
        var successMsg ='';
        var failMsg = '';

        if(type === 'update') {

            server_url = serverUrl("view/game/speed/updateSpeedGameAdmin");
            successMsg ="업데이트 되었습니다.";
            failMsg = '업데이트에 실패하였습니다.';

            var speedId = $("#speedId").text();
            var word = $("#word").val();
            var sttModel = $("#sttModel").val();
            var useYn = $("#useYn option:selected").val();

            if($.trim(word).length == 0){
                alert('단어를 입력해 주십시오');
                return false;
            }else if($.trim(sttModel).length == 0){
                alert('모델 값을 입력해 주십시오.');
                return false;
            }

            param = {
                speedId: speedId,
                word : word,
                sttModel : sttModel,
                useYn: useYn
            };

        }else{

            server_url = serverUrl("view/game/speed/insertSpeedGameAdmin");
            successMsg ="모델이 추가되었습니다.";
            failMsg = '모델추가에 실패하였습니다.';

            var word = $("#add_word").val();
            var sttModel = $("#add_sttModel").val();
            var useYn = $("#add_useYn option:selected").val();

            if($.trim(word).length == 0){
                alert('단어를 입력해 주십시오');
                return false;
            }else if($.trim(sttModel).length == 0){
                alert('모델 값을 입력해 주십시오.');
                return false;
            }

            var addData = {

                word : word,
                sttModel : sttModel,
                useYn: useYn
            };

            param ={
                data : JSON.stringify(addData)
            }
        }

        $.ajax({

            type:"post",
            dataType: "json",
            url: server_url,
            data : param,
            success: function(data){
                var result = data.result;
                if(result.code =='200'){

                    alert(successMsg);

                    $('.lyrWrap').fadeOut(300);
                    $('#lyr_modify').hide();
                    $("#grid").trigger("reloadGrid");
                    $( '#data_tbody').empty();

                }else{
                    alert(failMsg);
                }
            },
            fail:function(data){
                alert('update fail');
                console.log(data)
            },
            error: function(data, status, error){
                alert('update error');
                console.log(error);
            }
        });
    }


    function deleteSpeedGameAdmin(speedId){
        if(confirm('정말 삭제하시겠습니까?') ==true){

            var param={
                speedId :speedId
            }

            $.ajax({
                type:"post",
                dataType: "json",
                url: serverUrl("view/game/speed/deleteSpeedGameAdmin"),
                data : param,
                success: function(data){
                    var result = data.result;
                    if(result.code =='200'){
                        alert('삭제되었습니다.');
                        $("#grid").trigger("reloadGrid");

                    }else{
                        alert('삭제에 실패하였습니다.');
                    }
                },
                fail:function(data){console.log(data)},
                error: function(data, status, error){
                    console.log(error);
                }
            });
        }
    }



    $(document).ready(function(){
        initPage();
        createGrid();
        inintEvent();
        customResize('grid');
    });

     $(window).on('resize', function() {
          customResize('grid');
   	}).trigger('resize');



    function initPage(){
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    }

</script>
<div id="page_ldWrap" class="page_loading">
      <div class="loading_itemBox">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
      </div>
</div>
<!-- .lyrWrap -->
<div class="lyrWrap">
	<div class="lyr_bg"></div>
    <div id="lyr_add" class="lyrBox">
    	<div class="lyr_top">
        	<h3>추가하기</h3>
            <button type="button"  onclick="cancelUpdateDic('add');" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">단어</th>
                        <td><input type="text" class="ipt_txt" id="add_word" value=""></td>
                    </tr>
                    <tr>
                        <th scope="row">model</th>
                        <td><input type="text" class="ipt_txt" id="add_sttModel" value=""></td>
                    </tr>
                    <tr>
                        <th scope="row">사용여부</th>
                        <td>
                            <select id="add_useYn">
                                <option value="Y" selected="selected">Y</option>
                                <option value="N">N</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" onclick="updateSpeedGameAdmin('add');" class="btn_clr">저장</button></li>
                <li><button type="button" onclick="cancelUpdateDic('add');" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_modify" class="lyrBox" >
    	<div class="lyr_top">
        	<h3>수정하기</h3>
            <button type="button" onclick="cancelUpdateDic('update');" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody id="data_tbody"></tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" onclick="updateSpeedGameAdmin('update');" class="btn_clr">저장</button></li>
                <li><button type="button" onclick="cancelUpdateDic('update');" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_delete" class="lyrBox02">
		<input type="hidden" id="del_target" value=""/>
    	<div class="lyr_top">
        	<h3>삭제하기</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <div class="txtBox">
            	<div class="imgBox"><img src="${path}/resources/images/ico_warning_c.png" alt="주의"></div>
            	<p class="txt">삭제 하시겠습니까?</p>
            </div>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" onclick="deleteRowData();" class="btn_clr">확인</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
</div>
<!-- //.lyrWrap -->
        	<!-- .titArea -->
<div class="titArea">
 <h3>탕수육게임</h3>
 <div class="path">
    <span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
	 <span>DB 관리</span>
	 <span>탕수육게임</span>
 </div>
</div>
<!-- //.titArea -->
<!-- .srchArea -->
<div class="srchArea">
	<!-- .fl -->
	<div class="fl">
		<div class="selectbox">
			<label for="ex_select">선택</label>
			<select id="ex_select">
                <option value="">선택</option>
                <option value="word">단어</option>
			</select>
		</div>
		<div class="srchbox">
			<input type="text" id="searchText" class="ipt_txt" placeholder="검색어를 입력해 주세요.">
			<button type="button" id="searchBtn" class="btn_srch"><img src="${path}/resources/images/ico_srch_bk.png" alt="조건검색">조건검색</button>
		</div>
	</div>
	<!-- //.fl -->
	<!-- .fr -->
	<div class="fr">
		<ul class="btn_lst">
			<li><button  type="button" class="btn_clr lyr_add"><img src="${path}/resources/images/ico_add_bk.png" alt="삭제">모델추가</button></li>
		</ul>
	</div>
	<!-- //.fr -->
</div>
<!-- //.srchArea -->
<!-- .content -->
<!-- .stn -->
<div class="content">
  <div class="stn">
	<table id="grid"  class="tbl_lst"></table>
	<div id="pager"></div>
  </div> 	<!-- //.stn -->
</div>
</body>
</html>
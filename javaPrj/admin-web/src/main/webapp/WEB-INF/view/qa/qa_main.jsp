<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />

    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />

    <script  src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script  src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>

</head>
<body>
  <div id="page_ldWrap" class="page_loading">
      <div class="loading_itemBox">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
      </div>
    </div>
<div class="titArea">
    <h3>Q&amp;A 관리</h3>
    <div class="path">
        <span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
        <span>Q&amp;A 관리</span>
    </div>
    <ul class="tab_nav">
        <li><a href="javascript:void(0)" onclick="loadSubPage('list');"  class="active">Q&amp;A List</a></li>
        <li><a href="javascript:void(0)" onclick="loadSubPage('index');">Q&amp;A Index</a></li>
        <li><a href="javascript:void(0)" onclick="loadSubPage('domain');">Q&amp;A Domain</a></li>
    </ul>
</div>

<div id ="subContents">

</div>

</body>
<script>

    function loadSubPage(type){
        console.log(type);
        var url ='';
        switch (type) {
            case 'list':
                url=serverUrl("view/qa/tab/list");
                break;
            case 'index':
                url=serverUrl("view/qa/tab/index");
                break;
            case 'domain':
                url=serverUrl("view/qa/tab/domain");
                break;
        }

        $('#subContents').load(url, null);
    }

    $(document).ready(function(){
        loadSubPage('list');
    });


</script>

</html>

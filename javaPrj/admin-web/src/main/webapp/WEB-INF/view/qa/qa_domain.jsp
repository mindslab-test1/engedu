<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />

    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.theme.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />

    <script  src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script  src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>
    <script src="${path}/resources/js/jqgrid/src/grid.treegrid.js"></script>
    <script type="text/javascript" src="${path}/resources/js/common.js"></script>



</head>
<body>
<script>
     //그리드생성
    function createGrid(){

        $("#grid").jqGrid({
            url : serverUrl('view/qa/domain/domainList'),
            mtype : "POST",
            datatype : "json",
            colModel : [
                {label : '이름', name : 'domainName', width : '30%', align :'left', sortable : false},
                {label : 'depth', name : 'depth', width : '15%', align : 'center',  sortable : false},
                {label : '설명', name : 'descript', width : '30%', align : 'center',  sortable : false},
                {label : 'Action', name : 'Action', width : '25%', align : 'right',  sortable : false, formatter: actionFormatter},
                {label : 'domainId' ,name:'domainId'  ,width:0, key:true ,hidden:true}
            ],
            width : '1200',
            height : '360',
            treeGrid: true,
            treeGridModel: 'adjacency',
            treeIcons: {
                plus : 'ui-icon-plusthick',
                minus : 'ui-icon-minusthick',
				leaf : 'ui-icon-blank'
            },
            treeReader : {
                  level_field: "zlevel",
                  parent_id_field: "parentDomainId",
                  leaf_field: "lev",
                  expanded_field: "expanded"
            },

            ExpandColumn : 'domainName',
            viewrecords: true,
            gridview: false,
            autoencode:true,
            shrinkToFit : true,
            rownumbers:false,

            loadComplete : function(data) {
               var rData = $("#grid").jqGrid('getGridParam', 'data');
               setTimeout(function(){
                   $.each(rData, function(index, value){
                       if(value.zlevel == 0 && value.loaded == false){
                           $("#grid").jqGrid('expandNode', value);
                       }
                   });
               }, 10);

               customResize('grid');
			}
        });

        setTreeGroupChangeDefine();
    }


     function  actionFormatter(cellvalue, options, rowObject){
        var addImage = '<%=request.getContextPath()%>/resources/images/ico_add_bk.png';
        var editImage = '<%=request.getContextPath()%>/resources/images/ico_edit_bk.png';
        var deleteImage = '<%=request.getContextPath()%>/resources/images/ico_delete_bk.png';

        var  html =  "<ul class=\"btn_lst\">"

        var depth = rowObject.depth;

        if(depth < 3){
            html += "<li><button type=\"button\" onclick=\"showAddSubModal('"+rowObject.domainId+"', '"+rowObject.depth+"');\" class=\"btn_type_w lyr_lowRank\"><img src="+addImage+" alt=\"하위 추가\">하위 추가</button></li>"
        }

        html += "<li><button type=\"button\" onclick=\"showUpdateModal('"+rowObject.domainId+"');\" class=\"btn_type_w lyr_mfy\"><img src="+editImage+" alt=\"수정\">수정</button></li>"
             + "<li><button type=\"button\" onclick=\"showDeleteModal('"+rowObject.domainId+"');\" class=\"btn_type_w lyr_del\"><img src="+deleteImage+" alt=\"삭제\">삭제</button></li></ul>";

		return  html;
	}


      $(document).ready(function(){
        initPage();
        createGrid(0);
        customResize('grid');

    })


    function setTreeGroupChangeDefine(){
        $.fn.jqGrid.expandNode = defaultExpandNode;
        $.fn.jqGrid.collapseNode = defaultCollapseNode;

        /*트리 접고 필때....*/
        $.jgrid.extend({
            expandNode : function(rc) {
                var rowId = rc.domainId;
                if (!rc.lev) {
                    if(rc.parentDomainId || rc.parentDomainId == '-1'){
                        var gridObj = $(this);
                        return defaultExpandNode.call(this, rc);
                    }
                }
            },

            collapseNode : function(rc) {
                if (!rc.lev) {
                    if(rc.parentDomainId || rc.parentDomainId == '-1'){
                        return defaultCollapseNode.call(this, rc);
                    }
                }
            }
        });
    }


    function showAddSubModal(parentId, depth){

        $('.lyrBox').hide();
        $('.lyrWrap').fadeIn(300);
        $('#lyr_lowRank').show();

         $('#sub_domainNameA').val('');
         $('#sub_dscriptA').val('');

        $('#superDomainId').val(parentId);
        $('#sub_levelA').val(Number(depth)+1);
    }


      function showUpdateModal(domainId){

        $('.lyrBox').hide();
        $('.lyrWrap').fadeIn(300);
        $('#lyr_modify').show();

        var param ={
            domainId : domainId
        }

         $.ajax({
             type:"post",
             dataType: "json",
             data : param,
             url: serverUrl("view/qa/domain/selectDomain"),
             success: function(data){
                 var domain = data.domain;
                 $('#modify_domainName').val(domain.domainName);
                 $('#modify_descript').val(domain.descript);
                 $('#modify_dept').val(domain.depth);
             },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });

    }

     function showDeleteModal(domainId){

        $('#domainId').val(domainId);

        $('.lyrBox').hide();
        $('.lyrWrap').fadeIn(300);
        $('#lyr_delete').show();

     }


     function addDoamin(){

        var domainName = $('#domainName').val();
        var descript   =$('#dscript').val();

        if($.trim(domainName).length == 0){
            alert('도메인 이름을 입력해 주십시오');
            return false;
        }

        var param ={
            parentId : -1,
            depth : 1,
            domainName : domainName,
            descript : descript
        };


         $.ajax({
             type:"post",
             dataType: "json",
             data : param,
             url: serverUrl("view/qa/domain/addDomain"),
             success: function(data){
                 var result = data.result;

                 if (result.code == '200') {
                     alert('도메인이 추가 되었습니다.');

                     $('.lyrWrap').fadeOut(300);
                     $('#lyr_lowRank').hide();

                     $("#grid").trigger("reloadGrid");
                 } else {
                     alert('도메인 추가에 실패하였습니다.');
                 }
             },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }

    function addSubDoamin(){

        var domainName = $('#sub_domainNameA').val();
        var descript   =$('#sub_dscriptA').val();
        var level = $('#sub_levelA').val();
        var depth = level;

        if($.trim(domainName).length == 0){
            alert('도메인 이름을 입력해 주십시오');
            return false;
        }

        var param ={
            parentId : $('#superDomainId').val(),
            depth : depth,
            domainName : domainName,
            descript : descript
        };


         $.ajax({
             type:"post",
             dataType: "json",
             data : param,
             url: serverUrl("view/qa/domain/addDomain"),
             success: function(data){
                 var result = data.result;

                 if (result.code == '200') {
                     alert('도메인이 추가 되었습니다.');

                     $('.lyrWrap').fadeOut(300);
                     $('#lyr_lowRank').hide();

                     $("#grid").trigger("reloadGrid");
                 } else {
                     alert('도메인 추가에 실패하였습니다.');
                 }
             },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }

    function updateDomain(){

        var domainName = $('#modify_domainName').val();
        var descript   =$('#modify_descript').val();

        if($.trim(domainName).length == 0){
            alert('도메인 이름을 입력해 주십시오');
            return false;
        }

        var param ={
            domainId : $('#domainId').val(),
            domainName : domainName,
            descript : descript
        };


         $.ajax({
             type:"post",
             dataType: "json",
             data : param,
             url: serverUrl("view/qa/domain/updateDomain"),
             success: function(data){
                 var result = data.result;

                 if (result.code == '200') {
                     alert('도메인이 수정 되었습니다.');

                     $('.lyrWrap').fadeOut(300);
                     $('#lyr_modify').hide();
                     $("#grid").trigger("reloadGrid");
                 } else {
                     alert('도메인 수정에 실패하였습니다.');
                 }
             },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }


    function deleteDomain(){

        var param ={
            domainId : $('#domainId').val()
        };

         $.ajax({
             type:"post",
             dataType: "json",
             data : param,
             url: serverUrl("view/qa/domain/delDomain"),
             success: function(data){
                 var result = data.result;

                 if (result.code == '200') {
                     alert('도메인이 삭제 되었습니다.');

                     $('.lyrWrap').fadeOut(300);
                     $('#lyr_delete').hide();
                     $("#grid").trigger("reloadGrid");
                 } else {
                     alert('도메인 삭제에 실패하였습니다.');
                 }
             },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }


    $(window).on('resize', function () {
        customResize('grid');
    }).trigger('resize');

    function initPage(){
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });


        $('.lyr_add').on('click',function(){
			$('.lyrBox').hide();
			$('.lyrWrap').fadeIn(300);
			$('#lyr_add').show();
		});

    }



</script>
<div class="lyrWrap">
    <div class="lyr_bg"></div>
    <input type="hidden" id="superDomainId" value=""/>
    <input type="hidden" id="domainId" value=""/>
	<div id="lyr_add" class="lyrBox">
    	<div class="lyr_top">
        	<h3>도메인 생성</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">도메인 이름</th>
                        <td><input type="text" class="ipt_txt" id="domainName" placeholder="도메인 이름을 입력해 주세요."></td>
                    </tr>
                    <tr>
                        <th scope="row">도메인 설명</th>
                        <td><input type="text" class="ipt_txt" id="descript" placeholder="도메인 설명을 입력해 주세요."></td>
                    </tr>
                    <tr>
                        <th scope="row">레벨</th>
                        <td><input type="text" class="ipt_txt disabled" id="depth" value="1" disabled></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" class="btn_clr" onclick="addDoamin();" >저장</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_lowRank" class="lyrBox">
    	<div class="lyr_top">
        	<h3>하위 도메인 생성</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">도메인 이름</th>
                        <td><input type="text" class="ipt_txt" id="sub_domainNameA" placeholder="도메인 이름을 입력해 주세요."></td>
                    </tr>
                    <tr>
                        <th scope="row">도메인 설명</th>
                        <td><input type="text" class="ipt_txt" id="sub_dscriptA" placeholder="도메인 설명을 입력해 주세요."></td>
                    </tr>
                    <tr>
                        <th scope="row">레벨</th>
                        <td><input type="text" class="ipt_txt disabled" id="sub_levelA" value="2" disabled></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" class="btn_clr" onclick="addSubDoamin();">저장</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_modify" class="lyrBox">
    	<div class="lyr_top">
        	<h3>도메인 수정</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">도메인 이름</th>
                        <td><input type="text" class="ipt_txt" id="modify_domainName" placeholder="도메인 이름을 입력해 주세요."></td>
                    </tr>
                    <tr>
                        <th scope="row">도메인 설명</th>
                        <td><input type="text" class="ipt_txt"  id="modify_descript" placeholder="도메인 설명을 입력해 주세요."></td>
                    </tr>
                    <tr>
                        <th scope="row">레벨</th>
                        <td><input type="text" class="ipt_txt disabled"id="modify_dept" value="" disabled></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
                <li><button type="button" class="btn_clr" onclick="updateDomain();">확인</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_delete" class="lyrBox">
    	<div class="lyr_top">
        	<h3>삭제하기</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <div class="txtBox">
            	<div class="imgBox"><img src="${path}/resources/images/ico_warning_c.png" alt="주의"></div>
            	<p class="txt">삭제 하시겠습니까?</p>
            </div>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" class="btn_clr" onclick="deleteDomain();">확인</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
</div>
<div class="srchArea">
    <!-- .fr -->
    <div class="fr">
        <ul class="btn_lst">
            <li><button type="button" class="lyr_add"><img src="${path}/resources/images/ico_add_bk.png" alt="추가">도메인 생성</button></li>
        </ul>
    </div>
    <!-- //.fr -->
</div>
 <div class="content">
     <!-- .stn -->
     <div class="stn">
         <table id="grid"  class="tbl_lst"></table>
     </div>
 </div>

</body>
</html>

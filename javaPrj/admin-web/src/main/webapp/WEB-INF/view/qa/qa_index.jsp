<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />

    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />

    <script  src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script  src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>
 <script type="text/javascript" src="${path}/resources/js/jquery.treeview.js"></script>
    <script type="text/javascript" src="${path}/resources/js/common.js"></script>



</head>
<body>
<script>


    function createGrid(){

          $("#grid").jqGrid({
            url : serverUrl('view/qa/index/getlist'),
            mtype : "POST",
            datatype : "json",
            postData : {},
            colModel : [
                {label : 'No.', name : 'historyId', width : '10%', align : 'center'},
                {label : 'total', name : 'total', width : '25%', align : 'center'},
                {label : '타입', name : 'htype', width : '25%', align : 'center'},
                {label : '인덱싱 시작시간', name : 'indexStartTime', width : '25%', align : 'center'},
				{label : '인덱싱 완료시간', name : 'committedTime', width : '10%', align : 'center'},
                {label : '소요시간', name : 'takenTime', width : '10%', align : 'center'}
            ],
            rowNum : 50,
            rowList : [50, 100, 150],
            width : '1250',
            height : '300',
            pager : '#pager',
            gridview : true,
            autoencode:true,
            shrinkToFit : true,
			sortname:'historyId',
			sortorder: 'desc',
            loadComplete : function(data) {
               customResize('grid');
			}
        });

    }


    $(document).ready(function(){
        initPage();
        createGrid();
        customResize('grid');
    });

    function initPage(){
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });

        $('#allIndexBtn').on('click', function(){
           startAllIndexing();
        });


        $('#addIndexBtn').on('click', function(){
            startAddIndexing();
        })

    }

    function startAllIndexing(){

        $.ajax({

           type:"post",
           dataType: "json",
           url: serverUrl("view/qa/index/fullindex"),
           success: function(data){
               var result = data.result;

               if (result.code == '200') {
                   alert('완료 되었습니다.');
                   $("#grid").trigger("reloadGrid");
               } else {
                   alert('실패하였습니다.');
               }
            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });

    }


    function startAddIndexing(){

        $.ajax({

           type:"post",
           dataType: "json",
           url: serverUrl("view/qa/index/addIndex"),
           success: function(data){
               var result = data.result;
               if (result.code == '200') {
                   alert('완료 되었습니다.');
                   $("#grid").trigger("reloadGrid");
               } else {
                   alert('실패하였습니다.');
               }
            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });

    }



    $(window).on('resize', function(){
         customResize('grid');
    });

</script>
  <div id="page_ldWrap" class="page_loading">
      <div class="loading_itemBox">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
      </div>
    </div>
<!-- .indexArea -->
<div class="indexArea">
    <p class="txt">전체 인덱싱 또는 추가 인덱싱 선택하여 인덱싱을 시작하세요.</p>
    <ul class="btn_index">
        <li><button type="button" id="allIndexBtn">전체 인덱싱</button></li>
        <li><button type="button" id="addIndexBtn">추가 인덱싱</button></li>
    </ul>
    <div class="gaugeBox">
        <span class="gauge"></span>
    </div>
</div>
<!-- //.indexArea -->
<!-- .content -->
<div class="content" id="gridContents">
	<div class="stn">
		<table id="grid"  class="tbl_lst"></table>
		<div id="pager"></div>
  	</div> 	<!-- //.stn -->
</div>

</body>
</html>

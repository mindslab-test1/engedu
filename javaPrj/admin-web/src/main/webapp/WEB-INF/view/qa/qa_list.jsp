<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />

    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />

    <script  src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script  src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="${path}/resources/js/jquery.treeview.js"></script>
    <script type="text/javascript" src="${path}/resources/js/common.js"></script>



</head>
<body>
<script>
    var path ='${path}';  //스크립트에 path 쓰기 위해서 만듬
    var answerCount = 0; // 답변추가할때 rowCount 세기 위해서 만듬
    var deleteAnswer =[];
    //그리드생성
    function createGrid(domainId){
        $("#domainId").val(domainId);
        $("#grid").jqGrid({
            url : serverUrl('view/qa/list/qalist'),
            mtype : "POST",
            datatype : "json",
            colModel : [
                {label : '질문', name : 'question', width : '15%', align : 'left'},
                {label : '응답', name : 'answer', width : '40%', align : 'left',  formatter: formatter_answer},
                {label : '수정일', name : 'updateTime', width : '15%', align : 'center'},
                {label : 'Action', name : 'voiceName', width : '20%', align : 'center', sortable : false,  formatter: updateFormatter},
                {label : 'questionId' ,name:'questionId'  ,width:0, key:true ,hidden:true},
                {label : 'domainId' ,name:'domainId'  ,width:0  ,hidden:true}
            ],
            rowNum : 50,
            rowList : [50, 100, 150],
            width : '900',
            height : '350',
            pager : '#pager',
            gridview : true,
            autoencode:true,
            shrinkToFit : true,
            sortname:'questionId',
            sortorder: 'asc',
            loadComplete : function(data) {
                customTreeResize('grid');
            }
        });
    }
    function formatter_answer(cellvalue, options, rowObject){
        var str ='' + cellvalue;
        var strArray = str.split("▤");
        var resultStr = '';
        var count = 0;
        $.each(strArray, function(i, obj){
            count = (i+1);
            if(obj != null &&  obj != 'null' && obj.length > 0){
                if(count !== strArray.length){
                    resultStr += '     '+ count +'. ' + obj + '<br/>';
                }else{
                    resultStr += '     '+ count +'. ' + obj;
                }
            }else{
                resultStr = '';
            }
        });
        return resultStr;
    }
    function updateFormatter(cellvalue, options, rowObject){
        var updateImage = '<%=request.getContextPath()%>/resources/images/ico_edit_bk.png';
        var delImage = '<%=request.getContextPath()%>/resources/images/ico_delete_bk.png';
        var html  ='';
        html +=  "<ul class=\"btn_lst\">" +
            "<li><button type=\"button\" onclick=\"showUpdateView('"+rowObject.questionId+"');\" class=\"btn_type_w lyr_mfy\"><img src="+updateImage+" alt=\"수정\">수정</button></li>"
            +"<li><button type=\"button\" onclick=\"deleteView('"+rowObject.questionId+"');\" class=\"btn_type_w lyr_mfy\"><img src="+delImage+" alt=\"삭제\">삭제</button></li></ul>"
        return  html;
    }
    function selectedDomain(domainId){
        $("#domainId").val(domainId);
        var param ={
            searchType : $("#searchType option:selected").val(),
            searchText:  $("#searchText").val(),
            domainId : domainId
        };
        reloadGrid(param);
    }
    function reloadGrid(param){
        $('#grid').clearGridData();
        $('#grid').setGridParam({
            postData: param
        }).trigger("reloadGrid");
    }
    function searchQaList(){
        var param ={
            searchType : $("#searchType option:selected").val(),
            searchText:  $("#searchText").val(),
            domainId :  $("#domainId").val()
        };
        reloadGrid(param);
    }
    function showAddView(){
        $('.lyrBox').hide();
        $('.lyrWrap').fadeIn(300);
        $('#lyr_modify').hide();
        $('#lyr_delete').hide();
        $('#lyr_file').hide();
        $('#lyr_add').show();
        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/qa/list/getdomainList"),
            success: function(data){
                drawDomainforAdd(data);
                var  answerDiv =$('#add_answer');
                answerDiv.empty();
                answerCount = 0;
                addAnswer('add');
            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }
    function showUpdateView(questionId){
        $('.lyrWrap').fadeIn(300);
        $('.lyrBox').hide();
        $('#lyr_add').hide();
        $('#lyr_delete').hide();
        $('#lyr_file').hide();
        $('#lyr_modify').show();
        var param ={
            questionId: questionId
        }
        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/qa/list/getQAlistDettail"),
            data : param,
            success: function(data){
                answerCount = 0;
                drawDoaminForModify(data);
                drawAnswerList(data);
            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }
    function drawDomainforAdd(data){
        var doamin = data.domain;
        var add_domain = $("#add_domain");
        var label = $('#add_domain_lable');
        add_domain.empty();
        doamin.forEach(function(obj, index){
            var opt = $('<option></option>');
            opt.attr({'value' : obj.domainId});
            opt.append(obj.domainName);
            if(index === 0){
                opt.attr("selected", "selected");
                label.text(obj.domainName);
            }
            add_domain.append(opt);
        });
    }
    function drawDoaminForModify(data){
        var doamin = data.domain;
        var vo = data.vo;
        var modify_domain = $("#modify_domain");
        var label = $('#modify_domain_lable');
        modify_domain.empty();
        doamin.forEach(function(obj, index){
            var opt = $('<option></option>');
            opt.attr({'value' : obj.domainId});
            opt.append(obj.domainName);
            if(obj.domainId === vo.domainId){
                opt.attr("selected", "selected");
                label.text(obj.domainName);
            }
            modify_domain.append(opt);
        });
        setQuestion(vo);
    }
    function setQuestion(vo){
        $('#modify_question').val(vo.question);
        $('#modify_question_type').val(vo.questionType);
        $('#questionId').val(vo.questionId);
    }
    function drawAnswerList(data){
        var  answerDiv =$('#modify_answer');
        var vo = data.vo;
        var answer = null;
        var answerId = null;
        var clientcode = null;
        answerDiv.empty();
        if(vo.clientMenuCd !=null && vo.clientMenuCd !='null' && vo.clientMenuCd.length > 0){
            clientcode = vo.clientMenuCd.split('▤');
        }
        if(vo.answer !=null && vo.answer !='null' && vo.answer.length > 0){
            answer = vo.answer.split('▤');
            answerId = vo.answerId.split('▤');
        }
        if(answer !=null){
            answer.forEach(function(obj, index){
                answerCount++;
                var li =$('<li></li>');
                var title ='답변'+ answerCount;
                var titleSpan = $('<span>'+title+'</span>');
                var menuCodeSpan = $('<span>Client Code</span>');
                var titleInputSpan = $('<span></span>');
                var menuCode = $('<span></span>');
                var buttonSpan = $('<span></span>');
                var answerTextId = 'answerText' + answerCount;
                var menuCodeId =  'menuCode'+ answerCount;
                var answerIdHidden =$('<span><input type="hidden" id="'+ (answerId[index] +'_' +answerCount) +'" name="answerId" class="ipt_txt" value="'+answerId[index]+'"></span>');
                titleInputSpan.html('<input type="text" id="'+ answerTextId +'" name="answerText" class="ipt_txt" value="'+obj+'">');
                buttonSpan.html("<button type='button' class='btn_ico' onclick=\"removeAnswer("+ answerId[index]+ ", this);\"><img src=" +path+"/resources/images/ico_wbasket_bk.png" + "  alt=\"삭제\"/></button>");
                li.append(answerIdHidden);
                li.append(titleSpan);
                li.append(titleInputSpan);
                if(clientcode !=null){
                    menuCode.html('<input type="text" id="'+ menuCodeId +'" name="menuCode" class="ipt_txt" value="'+clientcode[index]+'">');
                    li.append(menuCodeSpan);
                    li.append(menuCode);
                }else{
                    menuCode.html('<input type="text" id="'+ menuCodeId +'" name="menuCode" class="ipt_txt">');
                    li.append(menuCodeSpan);
                    li.append(menuCode);
                }
                li.append(buttonSpan);
                answerDiv.append(li);
            });
        }else{
            answerCount++;
            var li =$('<li></li>');
            var title ='답변'+answerCount;
            var titleSpan = $('<span>'+title+'</span>');
            var titleInputSpan = $('<span></span>');
            var menuCodeSpan = $('<span>Client Code</span>');
            var menuCode = $('<span></span>');
            var buttonSpan = $('<span></span>');
            var answerTextId = 'answerText' + answerCount;
            var menuCodeId =  'menuCode'+ answerCount;
            var answerIdHidden =$('<span><input type="hidden" id="'+ ("new"+'_' +answerCount) +'" name="answerId" class="ipt_txt" value="new"></span>');
            titleInputSpan.html('<input type="text" id="'+ answerTextId +'"  name="answerText"  class="ipt_txt">');
            menuCode.html('<input type="text" id="'+ menuCodeId +'" name="menuCode" class="ipt_txt">');
            buttonSpan.html("<button type='button' class='btn_ico' onclick=\"removeAnswer(null, this);\"><img src=" +path+"/resources/images/ico_wbasket_bk.png" + "  alt=\"삭제\"/></button>");
            li.append(answerIdHidden);
            li.append(titleSpan);
            li.append(titleInputSpan);
            li.append(menuCodeSpan);
            li.append(menuCode);
            li.append(buttonSpan);
            answerDiv.append(li);
        }
    }
    function addAnswer(type){
        var answerDiv = null;
        if(type =='modify'){
            answerDiv =$('#modify_answer');
        }else{
            answerDiv =$('#add_answer');
        }
        answerCount++;
        var li =$('<li></li>');
        var title ='답변'+answerCount;
        var titleSpan = $('<span>'+title+'</span>');
        var titleInputSpan = $('<span></span>');
        var menuCodeSpan = $('<span>Client Code</span>');
        var menuCode = $('<span></span>');
        var buttonSpan = $('<span></span>');
        var answerTextId = 'answerText' + answerCount;
        var menuCodeId =  'menuCode'+ answerCount;
        var answerIdHidden =$('<span><input type="hidden" id="'+ ("new"+'_' +answerCount) +'" name="answerId" class="ipt_txt" value="new"></span>');
        titleInputSpan.html('<input type="text" id="'+ answerTextId +'"  name="answerText"  class="ipt_txt">');
        menuCode.html('<input type="text" id="'+ menuCodeId +'" name="menuCode" class="ipt_txt">');
        buttonSpan.html("<button type='button' class='btn_ico' onclick=\"removeAnswer(null, this);\"><img src=" +path+"/resources/images/ico_wbasket_bk.png" + "  alt=\"삭제\"/></button>");
        li.append(answerIdHidden);
        li.append(titleSpan);
        li.append(titleInputSpan);
        li.append(menuCodeSpan);
        li.append(menuCode);
        li.append(buttonSpan);
        answerDiv.append(li);
    }
    function addQAandAnswer(){
        if(validation('add')) {
            var data = makeDataParam('add');
            console.log("data==="+data);
            var param = {
                data: JSON.stringify(data)
            };
            $.ajax({
                type: "post",
                dataType: "json",
                data: param,
                url: serverUrl("view/qa/list/addQA"),
                success: function (data) {
                    var result = data.result;
                    if (result.code == '200') {
                        alert('등록 되었습니다.');
                        $('.lyrWrap').fadeOut(300);
                        $('#add_question').val('');
                        $('#add_answer').empty();
                        $('#lyr_add').hide();
                        $("#grid").trigger("reloadGrid");
                    } else {
                        alert('등록을 실패하였습니다.');
                    }
                },
                fail: function (data) {
                    console.log(data)
                },
                error: function (data, status, error) {
                    console.log(error);
                }
            });
        }
    }
    function updateQAandAnswer(){
        if(validation('modify')) {
            var data = makeDataParam('modify');
            var param = {
                data: JSON.stringify(data)
            };
            $.ajax({
                type: "post",
                dataType: "json",
                data: param,
                url: serverUrl("view/qa/list/updateQA"),
                success: function (data) {
                    var result = data.result;
                    if (result.code == '200') {
                        alert('업데이트 되었습니다.');
                        $('.lyrWrap').fadeOut(300);
                        $('#modify_question').val('');
                        $('#modify_answer').empty();
                        $('#lyr_modify').hide();
                        $("#grid").trigger("reloadGrid");
                    } else {
                        alert('업데이트가 실패하였습니다.');
                    }
                },
                fail: function (data) {
                    console.log(data)
                },
                error: function (data, status, error) {
                    console.log(error);
                }
            });
        }
    }
    function makeDataParam(type){
        var domainTypeName = type + '_'+ 'domain';
        var questionTypeName = type + '_'+ 'question';
        var questionType="";
        var data = {}
        var questionId = 0;
        if(type === 'modify'){
            questionId = $('#questionId').val();
            questionType = $("#modify_question_type").val();
        }
        var domainId = $("#"+ domainTypeName +" option:selected").val();
        var questionText = $('#' + questionTypeName).val();
        var answerInput = $('input[name="answerText"]');
        var answerIdInput = $('input[name="answerId"]');
        var menuCode = $('input[name="menuCode"]');
        var deleteAnswerId = '';
        if (deleteAnswer.length > 0) {
            deleteAnswerId = deleteAnswer.join(',');
        }
        if (answerInput.length == 1) {
            data = {
                domainId: domainId,
                questionId: questionId,
                questionType : questionType,
                questionText: questionText,
                answerCount: 1,
                deleteAnswerId: deleteAnswerId,
                answerData: {
                    questionId: questionId,
                    answerId: answerIdInput.val(),
                    answer: answerInput.val(),
                    clientMenuCd: menuCode.val()
                }
            }
        } else {
            var answerArray = [];
            $('input[name="answerText"]').each(function (index) {
                var $this = $(this);
                var val = $this.val();
                var answer = {
                    questionId: questionId,
                    answerId: answerIdInput[index].value,
                    answer: val,
                    clientMenuCd: menuCode[index].value
                };
                answerArray.push(answer);
            });
            data = {
                domainId: domainId,
                questionId: questionId,
                questionType : questionType,
                questionText: questionText,
                answerCount: answerArray.length,
                deleteAnswerId: deleteAnswerId,
                answerData: answerArray
            };
        }
        return data;
    }
    function validation(type){
        var isValid = true;
        var questionType = type + '_' + 'question';
        var question = $('#'+questionType).val();
        console.log(question);
        if($.trim(question).length == 0 || question === '' ){
            alert('질문을 입력해 주십시오.');
            $('#'+questionType).focus();
            return false;
        }
        if($('input[name="answerText"]').length ==0){
            isValid = false;
            alert('질문에 대한 답이 필요합니다.');
            return false;
        }
        $('input[name="answerText"]').each(function(index) {
            var $this = $(this);
            var val = $this.val();
            if($.trim(val).length ==0 || val ===''){
                isValid = false;
                alert('답변을 모두 입력해주십시오.');
                $this.focus();
                return false;
            }
        });
        if(!isValid){
            return false;
        }
        return true;
    }
    function removeAnswer(answerId, obj){
        console.log(answerId);
        $(obj).parent().parent().remove();
        answerCount--;
        if(answerId !=null || answerId !='null'){
            deleteAnswer.push(answerId);
        }
    }
    function deleteView(questionId){
        $('.lyrWrap').fadeIn(300);
        $('#lyr_modify').hide();
        $('#lyr_add').hide();
        $('#lyr_file').hide();
        $('#lyr_delete').show();
        $('#questionId').val(questionId);
    }
    function deleteRowData(){
        var questionId = $('#questionId').val();
        var param={
            questionId :questionId
        }
        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/qa/list/endlist"),
            data : param,
            success: function(data){
                var result = data.result;
                if(result.code =='200'){
                    alert('삭제되었습니다.');
                    $('.lyrWrap').fadeOut(300);
                    $('#lyr_delete').hide();
                    $("#grid").trigger("reloadGrid");
                }else{
                    alert('삭제에 실패하였습니다.');
                }
            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }
    function excelDownLoad(){
        setCookie("fileDownload","false"); //호출
        checkDownloadCheck();
        //$('.wrap-loading').removeClass('display-none');
        loadSpinner();
        var domainId = $('#domainId').val();
        $("#excelDownSearchType").val($("#searchType option:selected").val());
        $("#excelDownSearchText").val($("#searchText").val());
        $("#excelDomainId").val(domainId);
        $("#excelDownForm").submit();
    }
    function setCookie(c_name,value){
        var exdate=new Date();
        var c_value=escape(value);
        document.cookie=c_name + "=" + c_value + "; path=/";
    }
    function checkDownloadCheck(){
        if (document.cookie.indexOf("fileDownload=true") != -1) {
            var date = new Date(1000);
            document.cookie = "fileDownload=; expires=" + date.toUTCString() + "; path=/";
            //프로그래스바 OFF
            //$('.wrap-loading').addClass('display-none');
            stopspin();
            return;
        }
        setTimeout(checkDownloadCheck , 100);
    }
    function downExcelSample(){
        location.href = serverUrl("view/qa/list/downSample");
    }
    function downSampleModal(){
        $('.lyrWrap').fadeIn(300);
        $('.lyrBox').hide();
        $('#lyr_add').hide();
        $('#lyr_modify').hide();
        $('#lyr_delete').hide();
        $('#lyr_file').show();
    }
    function uploadExcel(file){
        var data = new FormData();
        //첫번째 파일태그
        data.append("file",$(file)[0].files[0]);
        var fileName = $(file).val();
        var fileExt = fileName.slice(fileName.indexOf(".") + 1).toLowerCase();
        if($.trim(fileName).length ==0){
            alert('업로드할 Excel 파일을 선택하여 주십시오.');
        }else if(fileExt !='xls' && fileExt !='xlsx'){
            alert('업로드는 엑셀파일만 가능합니다.');
        }else {
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: serverUrl("view/qa/list/insertData"),
                data: data,
                processData: false, //prevent jQuery from automatically transforming the data into a query string
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    console.log(data);
                    var result = data.result;
                    if(result.code =='200'){
                        alert('사전이 추가되었습니다.');
                        initFile();
                        $('.lyrWrap').fadeOut(300);
                        $('#lyr_file').hide();
                        $("#grid").trigger("reloadGrid");
                    }else{
                        initFile();
                        alert('사전추가가 실패하였습니다.');
                    }
                },
                error: function (e) {
                    $("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                }
            });
        }
    }
    function initFile(){
        var agent = navigator.userAgent.toLowerCase();
        if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ){
            // ie 일때 input[type=file] init.
            $("#excelFile").replaceWith( $("#excelFile").clone(true) );
        } else {
            $("#excelFile").val("");
        }
    }
    $(document).ready(function(){
        initPage();
        init();
        createGrid(0);
        customTreeResize('grid');
    })
    function init(){
        $("#tree").treeview({
            collapsed: false,
            animated: "medium",
            control:"#sidetreecontrol",
            persist: "location"
        });
        $('#searchBtn').on('click', function (e) {
            searchQaList();
        });
        $("#searchText").on('keyup', function(e){
            if(e.keyCode == 13){
                searchQaList();
            }
        });
    }
    $(window).on('resize', function () {
        customTreeResize('grid');
    }).trigger('resize');
    function initPage(){
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    }
</script>
<form style="display: hidden" action="qa/list/excelDown" method="POST" id="excelDownForm">
    <input type="hidden" id="excelDownSearchType" name="excelDownSearchType" value=""/>
    <input type="hidden" id="excelDownSearchText" name="excelDownSearchText" value=""/>
    <input type="hidden" id="excelDomainId" name="excelDomainId" value=""/>
</form>

<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- .lyrWrap -->
<div class="lyrWrap">
    <form id="qaAnswerForm" method="post">
        <input type="hidden" id="questionId" name="questionId" value=""/>
        <div class="lyr_bg"></div>
        <div id="lyr_add" class="lyrBox03">
            <div class="lyr_top">
                <h3>추가하기</h3>
                <button type="button" class="btn_lyr_close">닫기</button>
            </div>
            <div class="lyr_mid">
                <div class="txtBox">
                    <dl class="dlType02">
                        <dt>Domain</dt>
                        <dd>
                            <div class="selectbox">
                                <label for="add_domain" id="add_domain_lable">질문</label>
                                <select id="add_domain"></select>
                            </div>
                        </dd>
                    </dl>
                    <dl class="dlType02">
                        <dt>Question</dt>
                        <dd>
                            <input type="text" class="ipt_txt" id="add_question" name="add_question">
                        </dd>
                    </dl>
                    <dl class="dlType02">
                        <dt>
                            <span class="tit">Answer</span>
                            <ul class="btn_lst">
                                <li><button type="button" onclick="addAnswer('add');">답변추가</button></li>
                            </ul>
                        </dt>
                        <dd>
                            <ul class="ans_lst" id="add_answer">
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="lyr_btm">
                <ul class="btn_lst">
                    <li><button type="button" class="btn_clr" onclick="addQAandAnswer();">저장</button></li>
                    <li><button type="button" class="btn_lyr_close">취소</button></li>
                </ul>
            </div>
        </div>
        <div id="lyr_modify" class="lyrBox03">
            <div class="lyr_top">
                <h3>수정하기</h3>
                <button type="button" class="btn_lyr_close">닫기</button>
            </div>
            <div class="lyr_mid">
                <input type="hidden" class="ipt_txt" id="modify_question_type" name="modify_question_type" value="">
                <div class="txtBox">
                    <dl class="dlType02">
                        <dt>Domain</dt>
                        <dd>
                            <div class="selectbox">
                                <label for="modify_domain" id="modify_domain_lable">질문</label>
                                <select id="modify_domain" name="modify_domain"></select>
                            </div>
                        </dd>
                    </dl>
                    <dl class="dlType02">
                        <dt>Question</dt>
                        <dd>
                            <input type="text" class="ipt_txt"  id="modify_question" name="modify_question" value="">
                        </dd>
                    </dl>
                    <dl class="dlType02">
                        <dt>
                            <span class="tit">Answer</span>
                            <ul class="btn_lst">
                                <li><button type="button" onclick="addAnswer('modify');">답변추가</button></li>
                            </ul>
                        </dt>
                        <dd>
                            <ul class="ans_lst" id="modify_answer">
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="lyr_btm">
                <ul class="btn_lst">
                    <li><button type="button" class="btn_clr" onclick="updateQAandAnswer();">저장</button></li>
                    <li><button type="button" class="btn_lyr_close">취소</button></li>
                </ul>
            </div>
        </div>
        <div id="lyr_delete" class="lyrBox02">
            <div class="lyr_top">
                <h3>삭제하기</h3>
                <button type="button" class="btn_lyr_close">닫기</button>
            </div>
            <div class="lyr_mid">
                <div class="txtBox">
                    <div class="imgBox"><img src="${path}/resources/images/ico_warning_c.png" alt="주의"></div>
                    <p class="txt">삭제 하시겠습니까?</p>
                </div>
            </div>
            <div class="lyr_btm">
                <ul class="btn_lst">
                    <li><button type="button" class="btn_clr" onclick="deleteRowData();">확인</button></li>
                    <li><button type="button" class="btn_lyr_cancel">취소</button></li>
                </ul>
            </div>
        </div>
        <div id="lyr_file" class="lyrBox02">
            <div class="lyr_top">
                <h3>엑셀 업로드</h3>
                <button type="button" class="btn_lyr_close">닫기</button>
            </div>
            <div class="lyr_mid">
                <div class="srchArea">
                    <div class="fc">
                        <ul class="btn_lst">
                            <li>
                                <label class="btn_file" for="excelFile"><img src="${path}/resources/images/ico_upload_bk.png" alt="업로드">엑셀 업로드</label>
                                <input type="file" name="file" id="excelFile" class="ipt_file" onchange="uploadExcel(this)">
                            </li>
                            <li><button type="button" id="downSample" onclick="downExcelSample();"><img src="${path}/resources/images/ico_download_bk.png" alt="다운로드">샘플 다운로드</button></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="lyr_btm">
                <ul class="btn_lst">
                    <li><button type="button" class="btn_lyr_cancel">확인</button></li>
                </ul>
            </div>
        </div>
    </form>
</div>

<div class="srchArea">
    <!-- .fl -->
    <div class="fl">
        <div class="selectbox">
            <label for="searchType">질문</label>
            <select id="searchType">
                <option value="question" selected>질문</option>
                <option value="answer">응답</option>
            </select>
        </div>
        <div class="srchbox">
            <input type="text" class="ipt_txt" id="searchText" placeholder="검색어를 입력해 주세요.">
            <button type="button" id="searchBtn" class="btn_srch"><img src="${path}/resources/images/ico_srch_bk.png" alt="검색">검색</button>
        </div>
    </div>
    <!-- //.fl -->
    <!-- .fr -->
    <div class="fr">
        <ul class="btn_lst">
            <li>
                <a href="#" onclick="downSampleModal();"><img src="${path}/resources/images/ico_upload_bk.png" alt="업로드" />엑셀 업로드</a>
            </li>
            <li><button type="button" onclick="excelDownLoad();"><img src="${path}/resources/images/ico_download_bk.png" alt="다운로드" />엑셀 다운로드</button></li>
        </ul>
    </div>
    <!-- //.fr -->
</div>
<!-- //.srchArea -->
<!-- .content -->
<div class="content">
    <div class="treeWrap">
        <div class="tree_fl">
            <div class="treeBox">
                <div id="sidetree">
                    <div id="sidetreecontrol">
                        <span><a href="#none">전체 닫기</a></span>
                        <span><a href="#none">전체 열기</a></span>
                    </div>
                    <ul id="tree">
                        <c:forEach var="item" items="${domainList}">
                            <li>
                                <c:if test="${item.childCount == 0}">
                                    <a href="#none" onclick="selectedDomain('${item.domainId}')">${item.domainName}1</a>
                                </c:if>
                                <c:if test="${item.childCount > 0}">
                                    ${item.domainName}
                                    <ul>
                                        <c:forEach var="subItem" items="${item.subList}">
                                            <li>
                                                <c:if test="${subItem.childCount == 0}">
                                                    <a href="#none" onclick="selectedDomain('${subItem.domainId}')">${subItem.domainName}</a>
                                                </c:if>
                                                <c:if test="${subItem.childCount > 0}">
                                                    ${subItem.domainName}
                                                    <ul>
                                                        <c:forEach var="thriditem" items="${subItem.subList}">
                                                            <li>
                                                                <c:if test="${thriditem.childCount == 0}">
                                                                    <a href="#none" onclick="selectedDomain('${thriditem.domainId}')">${thriditem.domainName}</a>
                                                                </c:if>
                                                                <c:if test="${thriditem.childCount > 0}">
                                                                    ${thriditem.domainName}
                                                                    <ul>
                                                                        <c:forEach var="fourItem" items="${thriditem.subList}">
                                                                            <li>
                                                                                <c:if test="${fourItem.childCount == 0}">
                                                                                    <a href="#none" onclick="selectedDomain('${fourItem.domainId}')">${fourItem.domainName}</a>
                                                                                </c:if>
                                                                                <c:if test="${fourItem.childCount > 0}">
                                                                                    ${fourItem.domainName}
                                                                                    <ul>
                                                                                        <c:forEach var="fiveItem" items="${fourItem.subList}">
                                                                                            <li><a href="#none" onclick="selectedDomain('${fiveItem.domainId}')">${fiveItem.domainName}</a></li>
                                                                                        </c:forEach>
                                                                                    </ul>
                                                                                </c:if>
                                                                            </li>
                                                                        </c:forEach>
                                                                    </ul>
                                                                </c:if>
                                                            </li>
                                                        </c:forEach>
                                                    </ul>
                                                </c:if>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </c:if>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tree_fr">
            <input type="hidden" id="domainId" name="domainId" value="" />
            <table id="grid"  class="tbl_lst"></table>
            <div id="pager"></div>
            <!-- //페이징 -->
        </div>
    </div>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />

    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />

    <script  src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script  src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>
	<script type="text/javascript" src="${path}/resources/js/common.js"></script>
</head>
<body>
<script>
    var opts = ${opts};

    function inintEvent(){

        $("#searchBtn").on('click', function(e){
            searchDick();
        });


        $("#searchText").on('keyup', function(e){
            if(e.keyCode == 13){
                searchDick();
            }
        })

        $("#btnSubmit").on('click', function (event) {
            //stop submit the form, we will post it manually.
            event.preventDefault();
            uploadExcel();

        });

        $('#excelDown').on('click', function(e){
			excelDownLoad();
        });
    }


    function createSelectBox(){
       // $('#searchType').empty();
        $('#ex_select').empty();
        var optionAll = $('<option></option>');

        optionAll.attr({'value' : 'all'});
        optionAll.append('전체');

        $('#ex_select').append(optionAll);

        opts.forEach(function(obj, index){
            var option = $('<option></option>');
            option.attr({'value' : obj['serviceType']});
            option.append(obj['detailName']);
            $('#ex_select').append(option);
        });
    }


    function createGrid(){
        $("#grid").jqGrid({
            url : serverUrl('view/find/regex/diclist'),
            mtype : "POST",
            datatype : "json",
            postData : {},
            colModel : [
                {label : '구분', name : 'detailName', width : '10%', align : 'center'},
                {label : '진입발화', name : 'regexWord', width : '70%', align : 'center', sortable: false},
                {label : 'Action', name : '', width : '20%', align : 'center', sortable: false,  formatter : updateFormatter}
            ],
            rowNum : 50,
            rowList : [50, 100, 150],
            width : '1250',
            height : '350',
            pager : '#pager',
            gridview : true,
            autoencode:true,
            shrinkToFit : true,
            sortname:'detailName',
            sortorder: 'asc',
            loadComplete : function(data) {
                customResize('grid');
			}
        });
    }


    function updateFormatter(cellvalue, options, rowObject){

        var updateImage = '<%=request.getContextPath()%>/resources/images/ico_edit_bk.png';
        var delImage = '<%=request.getContextPath()%>/resources/images/ico_delete_bk.png';
        var html  ='';

        html +=  "<ul class=\"btn_lst\">" +
			       "<li><button type=\"button\" onclick=\"showUpdateView('"+rowObject.serviceType+"');\" class=\"btn_type_w lyr_mfy\"><img src="+updateImage+" alt=\"수정\">수정</button></li>"
			   +"<li><button type=\"button\" onclick=\"deleteView('"+rowObject.serviceType+"');\" class=\"btn_type_w lyr_mfy\"><img src="+delImage+" alt=\"삭제\">삭제</button></li></ul>"

		return  html;
    }


    function searchDick(){
        var param ={
            searchType : $("#ex_select option:selected").val(),
            searchText:  $("#searchText").val()
        };
        reloadGrid(param);
    }


    function reloadGrid(param){
        console.log(param);
        $('#grid').clearGridData();
        $('#grid').setGridParam({
            postData: param
        }).trigger("reloadGrid");
    }


    function showUpdateView(serviceType){

        $('.lyrWrap').fadeIn(300);
        $('#lyr_modify').show();
        $('#lyr_delete').hide();
		$('#lyr_file').hide();

        var param ={
            searchKey :"serviceType",
            searchValue: serviceType
        }

        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/find/regex/selectDetail"),
            data : param,
            success: function(data){

                $( '#data_tbody').empty();

                var result = data['data'];
                var $table = $("#data_tbody");

                var $tr = null;
                var $td = null;
                var $th = null;

                $.each(result, function(key, value){
                    $tr = $('<tr/>');
                    $td = $('<td/>');
                    $th = $('<th/>');

                    if(key =='detailName'){
                        $th.text('구분');
                    }else if(key =='regexWord'){
                        $th.text('진입발화');
                    }

                    if(value == null || value == 'undefined'){
                        value ='';
                    }

                    if(key =='detailName'){
                        $td.html('<span id="'+key +'">'+ value +'</span>');
                    }else if(key =='regexWord'){
                        $td.html('<input type="text" id="'+ key +'" class="ipt_txt" value="'+value+'">');
                    }

                    console.log(key);

                    if(key != 'serviceType') {
                        $tr.append($th);
                        $tr.append($td);
                        $table.append($tr);
                    }else{
                        var div = $("#updateDiv");
                        console.log(value);
                        var $input =  $('<input  type="hidden"  id="'+ key +'" class="ipt_txt" value="'+value+'">');
                        div.append($input);
                    }

                });
            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }



    function updateFindServiceAdmin(){

        var visibled =  $("#lyr_modify").css('display');

        if(visibled =='block'){
            var serviceType = $("#serviceType").val();
            var regexWord = $("#regexWord").val();

            console.log(serviceType);


            if($.trim(regexWord).length == 0){
                alert('진입발화를 입력하여 주십시오');
                return false;
            }else{

                var param = {
                    serviceType : serviceType,
                    regexWord : regexWord
                };

                $.ajax({
                    type:"post",
                    dataType: "json",
                    url: serverUrl("view/find/regex/updateFindServiceAdmin"),
                    data : param,
                    success: function(data){
                        var result = data.result;
                        if(result.code =='200'){

                            alert('업데이트 되었습니다.');

                            $('#serviceType').remove();

                            $('.lyrWrap').fadeOut(300);
							$('#lyr_modify').hide();
                            $( '#data_tbody').empty();

                            $("#grid").trigger("reloadGrid");


                            createSelectBox(); // 셀렉트박스는 리스트와 관계가 있기때문에...

                        }else{
                            alert('업데이트가 실패하였습니다.');
                        }
                    },
                    fail:function(data){
                        alert('update fail');
                        console.log(data)
                    },
                    error: function(data, status, error){
                        alert('update error');
                        console.log(error);
                    }
                });

            }
        }
    }


    function cancelUpdateDic(){

       $('.lyrWrap').fadeOut(300);
       $('#lyr_modify').hide();
       $( '#data_tbody').empty();
    }


    function deleteView(korId){

        $('.lyrWrap').fadeIn(300);
	    $('#lyr_delete').show();
	    $('#lyr_modify').hide();
	    $('#lyr_file').hide();

	    $('#del_target').val(korId);
	}

    function deleteRowData(){

        var serviceType = $('#del_target').val();

        console.log(serviceType);


        var param={
            serviceType :serviceType
        }

        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/find/regex/deleteFindServiceAdmin"),
            data : param,
            success: function(data){
                var result = data.result;
                if(result.code =='200'){
                    alert('삭제되었습니다.');

                    $('.lyrWrap').fadeOut(300);
                    $('#lyr_delete').hide();
                    $("#grid").trigger("reloadGrid");

                    createSelectBox(); // 셀렉트박스는 리스트와 관계가 있기때문에...
                }else{
                    alert('삭제에 실패하였습니다.');
                }
            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });

    }


    function excelDownLoad(){
        setCookie("fileDownload","false"); //호출
        checkDownloadCheck();
        //$('.wrap-loading').removeClass('display-none');
        loadSpinner();
        $("#excelDownSearchType").val($("#ex_select option:selected").val());
        $("#excelDownSearchText").val($("#searchText").val());
        $("#excelDownForm").submit();
    }


    function setCookie(c_name,value){
        var exdate=new Date();
        var c_value=escape(value);
        document.cookie=c_name + "=" + c_value + "; path=/";
    }

    function checkDownloadCheck(){
        if (document.cookie.indexOf("fileDownload=true") != -1) {
            var date = new Date(1000);
            document.cookie = "fileDownload=; expires=" + date.toUTCString() + "; path=/";
            //프로그래스바 OFF
            //$('.wrap-loading').addClass('display-none');
            stopspin();
			return;
        }
        setTimeout(checkDownloadCheck , 100);
    }


	function downSample(){
	    location.href = serverUrl("view/find/regex/downSample");
	}


    function downSampleModal(){

	    $('.lyrWrap').fadeIn(300);
	    $('#lyr_delete').hide();
	    $('#lyr_modify').hide();
		$('#lyr_file').show();

	}



    function uploadExcel(file){

        var form = $('#fileUploadForm')[0];
        var data = new FormData(form);

        var fileName = $(file).val();
        var fileExt = fileName.slice(fileName.indexOf(".") + 1).toLowerCase();


        console.log(fileExt);

        if($.trim(fileName).length ==0){
            alert('업로드할 Excel 파일을 선택하여 주십시오.');
        }else if(fileExt !='xls' && fileExt !='xlsx'){
            alert('업로드는 엑셀파일만 가능합니다.');
        }else {

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: serverUrl("view/find/regex/insertData"),
                data: data,
                processData: false, //prevent jQuery from automatically transforming the data into a query string
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    console.log(data);
                    var result = data.result;
                    if(result.code =='200'){
                        alert('게임멘트가 추가되었습니다.');
                        initFile();

						$('.lyrWrap').fadeOut(300);
                    	$('#lyr_file').hide();
                        $("#grid").trigger("reloadGrid");
                        createSelectBox(); // 셀렉트박스는 리스트와 관계가 있기때문에...
                    }else{
                        alert('게임멘트 추가를 실패하였습니다.');
                    }
                },
                error: function (e) {

                    $("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                    $("#btnSubmit").prop("disabled", false);

                }
            });
        }
    }

    function initFile(){
        var agent = navigator.userAgent.toLowerCase();
        if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ){
            // ie 일때 input[type=file] init.
            $("#excelFile").replaceWith( $("#excelFile").clone(true) );
        } else {
            $("#excelFile").val("");
        }
    }

    $(document).ready(function(){
        initPage();
        createSelectBox();
        createGrid();
        customResize('grid');
        inintEvent();
    });

    $(window).on('resize', function(){
        customResize('grid');
	}).trigger('resize');



    function initPage(){
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    }

</script>
<form style="display: hidden" action="find/regex/excelDown" method="POST" id="excelDownForm">
	<input type="hidden" id="excelDownSearchType" name="excelDownSearchType" value=""/>
	<input type="hidden" id="excelDownSearchText" name="excelDownSearchText" value=""/>
</form>
<div id="page_ldWrap" class="page_loading">
      <div class="loading_itemBox">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
      </div>
</div>
<!-- .lyrWrap -->
<div class="lyrWrap">
	<div class="lyr_bg"></div>
	<div id="lyr_modify" class="lyrBox" >
    	<div class="lyr_top">
        	<h3>수정하기</h3>
            <button type="button" onclick="cancelUpdateDic();" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid" id="updateDiv">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody id="data_tbody"></tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" onclick="updateFindServiceAdmin();" class="btn_clr">저장</button></li>
                <li><button type="button" onclick="cancelUpdateDic();" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_delete" class="lyrBox02">
		<input type="hidden" id="del_target" value=""/>
    	<div class="lyr_top">
        	<h3>삭제하기</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <div class="txtBox">
            	<div class="imgBox"><img src="${path}/resources/images/ico_warning_c.png" alt="주의"></div>
            	<p class="txt">삭제 하시겠습니까?</p>
            </div>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" onclick="deleteRowData();" class="btn_clr">확인</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_file" class="lyrBox02">
    	<div class="lyr_top">
        	<h3>엑셀 업로드</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
			<div class="srchArea">
           <div class="fc">
				<ul class="btn_lst">
					<li>
						<form method="post" enctype="multipart/form-data"  name="fileUploadForm"  id="fileUploadForm">
							<label class="btn_file" for="ipt_file"><img src="${path}/resources/images/ico_upload_bk.png" alt="업로드">엑셀 업로드</label>
							<input type="file" name="file" id="ipt_file" onchange="uploadExcel(this)" class="ipt_file">
						</form>
					</li>
					<li><button type="button" id="downSample" onclick="downSample();"><img src="${path}/resources/images/ico_download_bk.png" alt="다운로드">샘플 다운로드</button></li>
				</ul>
			</div>
			</div>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
                <li><button type="button" class="btn_lyr_cancel">확인</button></li>
            </ul>
        </div>
    </div>
</div>
<!-- //.lyrWrap -->
        	<!-- .titArea -->
<div class="titArea">
 <h3>진입발화</h3>
 <div class="path">
    <span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
	 <span>DB 관리</span>
	 <span>진입발화</span>
 </div>
</div>
<!-- //.titArea -->
<!-- .srchArea -->
<div class="srchArea">
	<!-- .fl -->
	<div class="fl">
		<div class="selectbox">
			<label for="ex_select">전체</label>
			<select id="ex_select">
			</select>
		</div>
		<div class="srchbox">
			<input type="text" id="searchText" class="ipt_txt" placeholder="검색어를 입력해 주세요.">
			<button type="button" id="searchBtn" class="btn_srch"><img src="${path}/resources/images/ico_srch_bk.png" alt="조건검색">조건검색</button>
		</div>
	</div>
	<!-- //.fl -->
	<!-- .fr -->
	<div class="fr">
		<ul class="btn_lst">
			<li>
				<a href="#" onclick="downSampleModal();"><img src="${path}/resources/images/ico_upload_bk.png" alt="업로드" />엑셀 업로드</a>
			</li>
			<li><button type="button" id="excelDown"><img src="${path}/resources/images/ico_download_bk.png" alt="다운로드" />엑셀 다운로드</button></li>
		</ul>
	</div>
	<!-- //.fr -->
</div>
<!-- //.srchArea -->
<!-- .content -->
<!-- .stn -->
<div class="content">
  <div class="stn">
	<table id="grid"  class="tbl_lst"></table>
	<div id="pager"></div>
  </div> 	<!-- //.stn -->
</div>
</body>
</html>

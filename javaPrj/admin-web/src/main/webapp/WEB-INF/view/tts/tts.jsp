<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
	<script type="text/javascript" src="${path}/resources/js/common.js"></script>

</head>
<body>
<script type="text/javascript">

    function convertText(){

         var param ={
		        lang : $("#ex_select option:selected").val(),
                userText : $('#searchText').val()
            };

		    $.ajax({

                url:serverUrl('view/tts/convert/text'),
                type : "post",
                dataType:"json",
                data : param,
                success:function(data){
                    var jsonObj = JSON.parse(data['data']);
                    var audio = $('.audioBox audio')[0];

                    audio.src = jsonObj.ttsUrl;
                    $('#resultText').val(jsonObj.ttsUrl);
                    $('#ttsfile').val(jsonObj.fileUrl);
            }
        });

    }
    
    function fileDown() {
        var fileName = $('#resultText').val();
        location.href =serverUrl('view/tts/downfile?fileName='+fileName);
    }

    function initEvent(){

        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
		    $(this).remove();

        });

        // 파일 듣기
        $('.lyr_play').on('click',function(){
            $('.lyrWrap').fadeIn(300);
            $('#lyr_play').show();
            $('.audioBox').show();
        });

        // 결과
		$('.btn_change').on('click',function(){

		    $('.resultArea').fadeIn(300);
		    convertText();
		});


    }

    $(document).ready(function (){
         initEvent();
	});
</script>
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<div class="lyrWrap">
    <div class="lyr_bg"></div>
    <div id="lyr_play" class="lyrBox">
        <div class="lyr_top">
            <h3>파일 듣기</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
        <div class="lyr_mid">
            <div class="audioBox">
                <audio id="myAudio" controls>
                    <source type="audio/mpeg" src="https://ai.ideepstudy.com/record/tts/simple/2018/09/17/test_20180917022402_373.mp3">
                </audio>
            </div>
        </div>
    </div>
</div>
<div class="titArea">
    <h3>TTS 관리</h3>
    <div class="path">
        <span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
        <span>TTS 관리</span>
        <span>TTS 테스트</span>
    </div>
</div>
<!-- //.titArea -->
<!-- .content -->
<div class="content">
    <input type="hidden" id="ttsfile" name="ttsfile" value=""/>
    <!-- .stn -->
    <div class="stn">
        <div class="stepBox lot03">
            <div class="step_item">
                <dl class="dlType01">
                    <dt>STEP 01. <span>언어선택</span></dt>
                    <dd>
                        <div class="selectbox">
                            <label for="ex_select">영어</label>
                            <select id="ex_select">
                                <c:forEach var="langSelect" items="${langSelectList}">
                                    <option value="${langSelect.langValue}" ${langSelect.langValue == 'eng' ? 'selected' : '' }>${langSelect.langText}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </dd>
                </dl>
            </div>
            <div class="step_item">
                <dl class="dlType01">
                    <dt>STEP 02. <span>텍스트 입력</span></dt>
                    <dd>
                        <div class="txtareaBox">
                            <textarea id="searchText" class="textArea" maxlength="100" placeholder="TTS로 변환할 텍스트를 입력하세요."></textarea>
                            <span class="txt_count">###</span>
                        </div>
                        <div class="btnBox">
                            <ul class="fr">
                                <li>
                                    <button type="button" id="transferBtn" class="btn_change disabled" disabled><img src="${path}/resources/images/ico_change_bk.png" alt="변환">변환</button>
                                </li>
                            </ul>
                        </div>
                    </dd>
                </dl>
            </div>
            <div class="step_item">
                <dl class="dlType01 resultArea">
                    <dt>STEP 03. <span>결과</span></dt>
                    <dd>
                        <div class="txtBox">
                            <textarea id="resultText" rows="5" cols="50"  style="border-style: hidden;background-color:#f3f6f8" disabled></textarea>
                        </div>
                        <div class="btnBox">
                            <ul class="fr">
                                <li><a class="btnType_w lyr_play" href="#none"><img src="${path}/resources/images/ico_play_bk.png" alt="듣기">듣기</a></li>
                                <li><a class="btnType_w" href="#" onclick="fileDown();"><img src="${path}/resources/images/ico_download_bk.png" alt="변환">다운로드</a></li>
                            </ul>
                        </div>
                    </dd>
                </dl>
            </div>
        </div>
    </div>
    <!-- //.stn -->
</div>
</body>
</html>

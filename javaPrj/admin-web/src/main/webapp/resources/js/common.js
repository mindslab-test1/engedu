// MINDsLab. UX/UI Team. YMJ. 20180825
var minMenuBarWidth  = 230;

$(document).ready(function() {

	 $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
        prevText: '이전 달',
        nextText: '다음 달',
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        showMonthAfterYear: true,
        yearSuffix: '년'
    });


// layer popup(오디오만 있는때 재생)
	$('.lyr_play').on('click',function(){
		$('.audioBox').fadeIn(300);

		$('.audioBox audio').each(function(){
			var audio = document.getElementById('myAudio');
			audio.play();
		});
	});

	// layer popup(테이블 정보에 오디오 있을때 재생)
	$('.btn_audio_play').on('click',function(){
		$('.audioBox').fadeIn(300);

		$('.audioBox audio').each(function(){
			var url = sttUrl  + $('#view_record').text();
			var audio = document.getElementById('myAudio');
			 audio.src = url;
			 audio.play();
		});
	});

	// layer popup(닫기)
	$('.btn_lyr_close, .btn_lyr_cancel, .lyr_bg').on('click',function(){
		$('.lyrWrap').fadeOut(300);
		$('.audioBox').fadeOut();
		$('.audioBox audio').each(function(){
			var audio = document.getElementById('myAudio');
			audio.pause();
			audio.currentTime = 0;
		});
	});
	
	// select design 
	var selectTarget = $('.selectbox select'); 
	
	selectTarget.change(function(){ 
	
	var select_name = $(this).children('option:selected').text(); 
	$(this).siblings('label').text(select_name); 
	}); 
	
	// header user
	$('#header .etcmenu .userBox dl dd > a').on('click',function(){
		$(this).parent().parent().addClass('active');
	});	
	$('#header .etcmenu .userBox').on('mouseleave',function(){
		$('#header .etcmenu .userBox dl').removeClass('active');
	});

	// snb
	$('.snb ul.nav li a').on('click',function(){
		$('.snb ul.nav li').removeClass('active');
		
		$(this).parents().addClass('active');
	});
	$('.snb ul.sub_nav > li > a').on('click',function(){
		$('.snb ul.sub_nav li').removeClass('active');
		
		$(this).parent().addClass('active');
		$(this).parents().parents().parents().addClass('active');	
	});	
	$('.snb ul.third_nav > li > a').on('click',function(){
		$('.snb ul.third_nav > li').removeClass('active');
		
		$(this).parent().addClass('active');
		$(this).parents().parents().parents().addClass('active');	
	});

	$('.snb.transition ul.nav li.active a').on(' mouseup',function(){
		$(this).parents().children('ul.sub_nav').css({
			display:'block',
		});
	});
	
	// select	
	$('.selectbox select').on('focus',function(){
		$(this).parent().addClass('active');
	});
	$('.selectbox select').on('focusout',function(){
		$(this).parent().removeClass('active');
	});
	
	// text count
	$('.txtareaBox .textArea').on('input keyup paste', function() {
		var content = $(this).val();
		$(this).height(((content.split('\n').length + 1) * 1.5) + 'px');
		$('.txt_count').html(content.length + '/100');
		
		var txtValLth = $(this).val().length;
		
		if ( txtValLth > 0) {
			$('.btn_change').removeClass('disabled');	
			$('.btn_change').removeAttr('disabled');
		} else {
			$('.btn_change').addClass('disabled');	
			$('.btn_change').attr('disabled');
			$('.resultArea').fadeOut(300);
		}
	});
	$('.txtareaBox .textArea').keyup();

	// tree
	$('#tree a').on('click',function(){
		$('#tree a').removeClass('active');

		$(this).addClass('active');
	});

	// tab
	$('.subTab_box').each(function(){
		$('.subTab_contents').hide(); //Hide all content
		$('.subTab_nav li a:first').addClass('active').show(); //Activate first tab
		$('.subTab_contents:first').show(); //Show first tab content

		//TAB On Click Event
		$('.subTab_nav li a').click(function() {

			$('.subTab_nav li a').removeClass('active'); //Remove any 'active' class
			$(this).addClass('active'); //Add 'active' class to selected tab
			$('.subTab_contents').hide(); //Hide all tab content

			var activeTab = $(this).attr('href'); //Find the href attribute value to identify the active tab + content
			$(activeTab).show(); //Fade in the active ID content
			return false;
		});
	});


	//QA 탭 이동.
	$('.tab_nav li a').click(function() {
		$('.tab_nav li a').removeClass('active');
		$(this).addClass('active');

    });


	//table depth
	$('.btn_depth').click(function(){
		$('.btn_depth').removeClass('active');
		$('.btn_depth').text('+');
		$('.tbl_depth02').hide();

		$(this).addClass('active');
		$(this).text('-');
		$(this).parent().parent().next().children('td.tbl_depth02').show();
	});

	$('.btn_subDepth').click(function(){
		$('.btn_subDepth').removeClass('active');
		$('.btn_subDepth').text('+');
		$('.tbl_depth03').hide();

		$(this).addClass('active');
		$(this).text('-');
		$(this).parent().parent().next().children('td.tbl_depth03').show();
	});

	// main 공지 팝업
	$('#header .hamBox .btn_ham').on('click',function(){
		$(this).toggleClass('active');
		$('.snb').toggleClass('transition');
		return false;
	});

});


	function getRSAKey(){

		var keyData = null;

		$.ajax({
			type: "post",
			url: serverUrl("view/user/auth/getRSAKeyValue"),
			async: false,
			success: function (data) {
				keyData = data;
			},
			fail: function (data) {
				console.log(data)
			},
			error: function (data, status, error) {
				console.log(error);
			}
		});

		return keyData;
	}


	function encryptedId(id, rsaPublicKeyModulus, rsaPpublicKeyExponent) {

		var securedLoginForm = document.getElementById('loginFrm');
		var rsa = new RSAKey();
		var securedUsername ='';
		rsa.setPublic(rsaPublicKeyModulus, rsaPpublicKeyExponent);

		securedUsername = rsa.encrypt(id);  // id 암호화

		return securedUsername;
	}



	function encryptedUserKey(key, rsaPublicKeyModulus, rsaPpublicKeyExponent) {
		 var rsa = new RSAKey();
		 var securedPassword = '';

		 rsa.setPublic(rsaPublicKeyModulus, rsaPpublicKeyExponent);
		 securedPassword = rsa.encrypt(key);  // passwd 암호화

		 return securedPassword;
	 }


   function customResize(gridName){
	     var width = $('#wrap').outerWidth(true)-260;
        //  console.log(width);
	   width = width  < 980 ? 980 : width;
	   var menuArea = $('#menuArea').outerWidth(true);

	   //console.log('menuArea : ' + menuArea);
	   $('#'+gridName).setGridWidth(width);
    }



     function customTreeResize(gridName){
		var treeWidth = $('.tree_fl').outerWidth(true);
		var width = $('#wrap').outerWidth(true)- treeWidth - 310;
		var remainWidth  = treeWidth < 248 ? 248 - treeWidth : 0 ;
		width = width - remainWidth;

		$('#'+gridName).setGridWidth(width);
    }




	var defaultExpandNode =
	function (rc) {
		console.log(this);
		return this.each(function(){
			if(!this.grid || !this.p.treeGrid) {return;}
			var expanded = this.p.treeReader.expanded_field,
			parent = this.p.treeReader.parent_id_field,
			loaded = this.p.treeReader.loaded,
			level = this.p.treeReader.level_field,
			lft = this.p.treeReader.left_field,
			rgt = this.p.treeReader.right_field;

			if(!rc[expanded]) {
				var id = $.jgrid.getAccessor(rc,this.p.localReader.id);
				var rc1 = $("#" + this.p.idPrefix + $.jgrid.jqID(id),this.grid.bDiv)[0];
				var position = this.p._index[id];

				if(typeof rc1 != 'undefined'){
					console.log($(this).jqGrid("isNodeLoaded",this.p.data[position]));
					if( $(this).jqGrid("isNodeLoaded",this.p.data[position]) ) {
						rc[expanded] = true;
						$("div.treeclick",rc1).removeClass(this.p.treeIcons.plus+" tree-plus").addClass(this.p.treeIcons.minus+" tree-minus");
					} else if (!this.grid.hDiv.loading) {
						rc[expanded] = true;
						$("div.treeclick",rc1).removeClass(this.p.treeIcons.plus+" tree-plus").addClass(this.p.treeIcons.minus+" tree-minus");
						this.p.treeANode = rc1.rowIndex;
						this.p.datatype = this.p.treedatatype;
						if(this.p.treeGridModel === 'nested') {
							$(this).jqGrid("setGridParam",{postData:{nodeid:id,n_left:rc[lft],n_right:rc[rgt],n_level:rc[level]}});
						} else {
							$(this).jqGrid("setGridParam",{postData:{nodeid:id,parentid:rc[parent],n_level:rc[level]}} );
						}
						$(this).trigger("reloadGrid");
						rc[loaded] = true;
						if(this.p.treeGridModel === 'nested') {
							$(this).jqGrid("setGridParam",{postData:{nodeid:'',n_left:'',n_right:'',n_level:''}});
						} else {
							$(this).jqGrid("setGridParam",{postData:{nodeid:'',parentid:'',n_level:''}});
						}
					}

				}
			}

		});
	};

	var defaultCollapseNode =
	function (rc) {
		return this.each(function(){

			if(!this.grid || !this.p.treeGrid) {return;}
			var expanded = this.p.treeReader.expanded_field;
			if(rc[expanded]) {
				rc[expanded] = false;
				var id = $.jgrid.getAccessor(rc,this.p.localReader.id);
				var rc1 = $("#" + this.p.idPrefix + $.jgrid.jqID(id),this.grid.bDiv)[0];
				$("div.treeclick",rc1).removeClass(this.p.treeIcons.minus+" tree-minus").addClass(this.p.treeIcons.plus+" tree-plus");

			}
		});
	};


	var tempid =null;
	function loadSpinner(){
      	var target = $("#container");

		var maskHeight = $(target || window).outerHeight(true);
		var maskWidth = $(target || window).outerWidth(true);
		var obj = target.offset();

		// #div의 현재 위치
		//console.log("left: " + obj.left + "px, top: " + obj.top + "px");
		tempid = 'target_div';
		/*트리그리드는...자식노드 그릴려고..자꾸 반복한다...그래서 생성된 spin 그리드가 존재하면 우선 지운다...*/
		if($("#"+tempid).length > 0){
			$("#"+tempid).remove();
		}

		var bgDiv = $('<div id="'+tempid+'"/>');
		bgDiv.css({
			zIndex:99999999,
			position:'absolute',
			top: obj.top,
			left:obj.left,
			width:'100%',
			height:'100%',
			opacity:0.4
		});

		bgDiv.css("background-color","white");
		bgDiv.css("display","none");

		bgDiv.appendTo(target);
		bgDiv.show();
		bgDiv.spin();
	}


	function stopspin(){
	   $("#target_div").spin(false);
	   $("#target_div").hide();
    }
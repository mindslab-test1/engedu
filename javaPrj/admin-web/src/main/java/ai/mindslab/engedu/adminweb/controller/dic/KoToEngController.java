package ai.mindslab.engedu.adminweb.controller.dic;

import ai.mindslab.engedu.admin.korengdic.dao.data.KorToEngDicAdminVO;
import ai.mindslab.engedu.admin.korengdic.service.KorToEngDicAdminService;
import ai.mindslab.engedu.admin.korengdic.service.KorToEngDicExcel;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.adminweb.common.view.DownloadView;
import ai.mindslab.engedu.adminweb.common.view.ExcelView;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/view/koen")
public class KoToEngController extends CommonController {
	private static Logger logger  = LoggerFactory.getLogger(KoToEngController.class);
	@Autowired
	KorToEngDicAdminService kortoEngDicService;
	@Autowired
	KorToEngDicExcel korToEngDicExcel;

	@Autowired
	DownloadView downloadView;

	@RequestMapping("/list")
	public ModelAndView korToEndView(HttpServletRequest req, HttpServletResponse res) {
		ModelAndView view = new ModelAndView();
		view.setViewName("ko_en/ko_en_list");
		return view;

	}


	@RequestMapping("/diclist")
	public ModelAndView getKoenDicList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
		ModelAndView view = new ModelAndView();
		Map<String, Object> param = reqToHash(req);

		int rows = 0;
		int page = 0;
		int total_pages = 0;
		int startNum = 0;

		if (param.containsKey("rows") && param.get("rows") != null) {
			rows = Integer.parseInt(param.get("rows").toString());
		} else {
			rows = 30;
		}

		if (param.containsKey("page") && param.get("page") != null) {
			page = Integer.parseInt(param.get("page").toString());
		} else {
			page = 1;
		}

		if(param.containsKey("sidx") && param.get("sidx").equals("korId")){
			param.put("sidx", "kor_id");
		}

		startNum = rows * (page - 1);

		int totalCount = kortoEngDicService.getDicAdminCount(param);

		param.put("start", startNum);
		param.put("rows", rows);

		//게시판 리스트 가져오기
		List<KorToEngDicAdminVO> list = kortoEngDicService.getDicAdminList(param);


		if (list.size() > 0) {
			total_pages = (int) Math.ceil((double) totalCount / rows);
		} else {
			total_pages = 0;
		}

		view.addObject("total", total_pages);    // the total pages of the query
		view.addObject("records", totalCount);     // the total records from the query
		view.addObject("rows", list);

		view.setViewName("jsonView");

		return view;
	}

	@RequestMapping("/selectDetail")
	public ModelAndView selectDetail(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
		ModelAndView view = new ModelAndView();

		Map<String, Object> param = reqToHash(req);

		KorToEngDicAdminVO vo = kortoEngDicService.selectDicById(param);

		view.addObject("data", vo);
		view.setViewName("jsonView");
		return view;
	}

	@RequestMapping(value = "/updateKorengDetail")
	public ModelAndView updateKorengDetail(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
		ModelAndView view = new ModelAndView();

		Map<String, Object> param = reqToHash(req);
		BaseResponse<Object> resp = kortoEngDicService.updateKorengDetail(param);

		view.addObject("result", resp);
		view.setViewName("jsonView");

		return view;
	}

	@RequestMapping(value = "/deleteKorEngDic")
	public ModelAndView deleteKorEngDic(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
		ModelAndView view = new ModelAndView();
		Map<String, Object> param = reqToHash(req);

		BaseResponse<Object> resp = kortoEngDicService.deleteKorEngDic(param);

		view.addObject("result", resp);
		view.setViewName("jsonView");

		return view;
	}


	@RequestMapping(value = "/downSample")
	public ModelAndView downSample(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
		ModelAndView view = new ModelAndView();
		Map<String, Object> param = reqToHash(req);

		String fileUrl  = req.getSession().getServletContext().getRealPath("resources/sample/koreng_sample.xlsx");

		logger.info("down sample url : " + fileUrl);

		 File file = new File(fileUrl);

        view.addObject("downloadFile", file);
        view.setView(downloadView);

		return view;
	}



	@RequestMapping(value = "/excelDown")
	public ModelAndView excelDown(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
		ModelAndView view = new ModelAndView();
		Map<String, Object> param = reqToHash(req);
		param.put("searchType",param.get("excelDownSearchType"));
		param.put("searchText",param.get("excelDownSearchText"));

		String date = new SimpleDateFormat("yyyymmddHHmmss", Locale.KOREA).format(new Date());
		String fileName = "KorToEngDictionary_"+ date +".xlsx";
		String title ="한영사전";

		korToEngDicExcel.setTitle(title);
		Workbook book = korToEngDicExcel.createExcel(param);


		view.setView(new ExcelView());
		view.addObject("workBook", book);
		view.addObject("fileName", fileName);
		view.addObject("title", title);

		return view;
	}

	@PostMapping(value="/insertData")
	public ModelAndView uploadFile(HttpServletRequest req,
								   @RequestParam("file") MultipartFile uploadfile)throws Exception {
		ModelAndView view = new ModelAndView();
		UserVO vo = getUserInfo(req);
		BaseResponse<Object> resp = kortoEngDicService.uploadExcelData(vo, uploadfile);

		view.addObject("result", resp);
		view.setViewName("jsonView");

		return view;

	}

}

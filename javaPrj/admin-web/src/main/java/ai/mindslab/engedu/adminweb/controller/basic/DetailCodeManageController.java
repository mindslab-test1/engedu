package ai.mindslab.engedu.adminweb.controller.basic;

import ai.mindslab.engedu.admin.code.detail.dao.data.DetailCodeAdminVO;
import ai.mindslab.engedu.admin.code.detail.dao.data.GroupCodeTypeVO;
import ai.mindslab.engedu.admin.code.detail.service.CodeDetailAdminService;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("view/code/detail")
public class DetailCodeManageController  extends CommonController {


    @Autowired
    CodeDetailAdminService service;

    @RequestMapping("/list")
    public ModelAndView userAuthList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        view.setViewName("manage_code/detail_code");
        return view;

    }


    @RequestMapping("/getGroupCode")
    public ModelAndView getUserRole(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        List<GroupCodeTypeVO> codeList = service.getGroupCode();

        view.addObject("codeList", codeList);
        view.setViewName("jsonView");

        return view;

    }

    @RequestMapping("/codeList")
    public ModelAndView codeList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }

        if(param.containsKey("sidx") && param.get("sidx").equals("endId")){
            param.put("sidx", "end_id");
        }

        startNum = rows * (page - 1);

        int totalCount = service.getDetailCodeAdminCount(param);

        param.put("start", startNum);
        param.put("rows", rows);

        //게시판 리스트 가져오기
        List<DetailCodeAdminVO> list = service.getCodeDetailAdminList(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping("/getCodeDetailAdmin")
    public ModelAndView getCodeDetailAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {

        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        DetailCodeAdminVO userInfo = service.getCodeDetailAdmin(param);

        view.addObject("data", userInfo);
        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping("/updateCodeDetail")
    public ModelAndView updateCodeDetail(HttpServletRequest req, HttpServletResponse res) throws Exception {
        Map<String, Object> param = reqToHash(req);
        ModelAndView view = new ModelAndView();

        UserVO vo = this.getUserInfo(req);


        param.put("updatorId", vo.getUserId());

        BaseResponse<Object> resp = service.updateCodeDetail(param);

        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }


    @RequestMapping("/addCodeDetail")
    public ModelAndView addCodeDetail(HttpServletRequest req, HttpServletResponse res) throws Exception {
        Map<String, Object> param = reqToHash(req);
        ModelAndView view = new ModelAndView();

        UserVO vo = this.getUserInfo(req);

        param.put("creatorId", vo.getUserId());
        BaseResponse<Object> resp = service.addCodeDetail(param);

        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;
    }



    @RequestMapping("/deleteCodeDetail")
    public ModelAndView deleteCodeDetail(HttpServletRequest req, HttpServletResponse res) throws EngEduException {

        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        UserVO vo = this.getUserInfo(req);

        BaseResponse<Object> resp = service.deleteCodeDetail(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;
    }
}

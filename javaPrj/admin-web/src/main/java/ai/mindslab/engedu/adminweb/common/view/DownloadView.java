package ai.mindslab.engedu.adminweb.common.view;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Map;

/**
 * 파일 다운로드를 위한 가상의 view
 */

@Slf4j
@Component
public class DownloadView extends AbstractView {

    public DownloadView() {
        setContentType("applicaiton/download;charset=utf-8");
    }

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model,
                                           HttpServletRequest request, HttpServletResponse response) throws Exception {

        // controller 로 부터 넘어온 파일객체를 가져온다..
        File file = (File) model.get("downloadFile");
        //view 타입을 지정한다.
        response.setContentType(getContentType());
        //다운받을 파일의 길이를 지정한다.
        response.setContentLength((int) file.length());
        //파일이름 한글깨짐을 방지하기 위해 UTF-8로 인코딩한다.
        String fileName = java.net.URLEncoder.encode(file.getName(), "UTF-8");
        //파일 다운로드를 위한 헤더 설정.
        log.info("filePath === " + file.getPath());
        log.info("fileName === "+ fileName);
        response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\";");
        response.setHeader("Content-Transfer-Encoding", "binary");
        //파일 다운로드~
        OutputStream out = response.getOutputStream();
        FileInputStream fis = null;

        try {
            fis = new FileInputStream(file);
            FileCopyUtils.copy(fis, out);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e2) {e2.printStackTrace();}
            }
        }
        out.flush();
    }
}

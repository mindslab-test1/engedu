package ai.mindslab.engedu.adminweb.controller.game;


import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.speedgame.dao.data.SpeedGameAdminVO;
import ai.mindslab.engedu.admin.speedgame.service.SpeedGameAdminService;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("view/game/speed")
public class SpeedGameAdminController extends CommonController {

    @Autowired
    SpeedGameAdminService service;


    @RequestMapping("/list")
    public ModelAndView speedGameView(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        JSONArray array =  new JSONArray();
        view.setViewName("game/game_speed");
        return view;

    }

    @RequestMapping("/diclist")
    public ModelAndView getSpeedGameList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }


        startNum = rows * (page - 1);

        int totalCount = service.getSpeedGameAdminCount(param);

        param.put("start", startNum);
        param.put("rows", rows);

        //게시판 리스트 가져오기
        List<SpeedGameAdminVO> list = service.getSpeedGameAdminList(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping("/selectDetail")
    public ModelAndView selectDetail(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();

        Map<String, Object> param = reqToHash(req);
        SpeedGameAdminVO vo = service.getSpeedGameAdmin(param);
        view.addObject("data", vo);
        view.setViewName("jsonView");
        return view;
    }

    @RequestMapping("/updateSpeedGameAdmin")
    public ModelAndView updateSpeedGameAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        UserVO vo = this.getUserInfo(req);

        param.put("user_id", vo.getUserId());

        BaseResponse<Object> resp = service.updateSpeedGameAdmin(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }

    @RequestMapping("/deleteSpeedGameAdmin")
    public ModelAndView deleteSpeedGameAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        BaseResponse<Object> resp = service.deleteSpeedGameAdmin(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;
    }

    @RequestMapping("/insertSpeedGameAdmin")
    public ModelAndView insertSpeedGameAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        UserVO vo = this.getUserInfo(req);

        param.put("user_id", vo.getUserId());

        BaseResponse<Object> resp = service.insertSpeedGameAdmin(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;
    }

}

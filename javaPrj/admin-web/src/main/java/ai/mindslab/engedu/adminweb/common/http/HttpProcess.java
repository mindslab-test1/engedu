package ai.mindslab.engedu.adminweb.common.http;

import ai.mindslab.engedu.freetalk.solr.FreeTalkSolrClient;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

@Component
public class HttpProcess {
	
	private Logger logger = LoggerFactory.getLogger(HttpProcess.class);

	/**
	 * Https url 통신...
	 * 인증없이 접근.
	 * @param inputurl
	 * @return
	 * @throws Exception
	 */
	public JSONObject sendReceiveHttps(String inputurl) throws Exception {

		logger.debug("connect Ews Url : " + inputurl);		
		// Get HTTPS URL connection  
		URL url = new URL(inputurl);

		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager(){
			@Override
			public X509Certificate[] getAcceptedIssuers(){ return null; }
			@Override
			public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException{}
			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException{}
		}};
		  
		SSLContext sc = SSLContext.getInstance("SSL"); 
		sc.init( null, trustAllCerts, new java.security.SecureRandom() );

		HttpsURLConnection.setDefaultSSLSocketFactory( sc.getSocketFactory() );

		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
			@Override
			public boolean verify(String hostname, SSLSession session){ return true; }
		});
		  
		// Connect to host 
		HttpsURLConnection conn = ( HttpsURLConnection ) url.openConnection();
		 
		conn.setDoInput( true );
		conn.setDoOutput( true );
		conn.setConnectTimeout( 60000 );
		conn.setRequestMethod( "POST" );
		conn.setInstanceFollowRedirects(true);  


		InputStream s = conn.getInputStream();
		BufferedReader br = new BufferedReader( new InputStreamReader(conn.getInputStream(), "utf-8" ) );
		String input = "";
		StringBuilder resultBuffer = null;

		resultBuffer = new StringBuilder();
		
		while( ( input = br.readLine() ) != null ){
			  resultBuffer.append(input+"\n");
		}
		
		if(resultBuffer != null && resultBuffer.toString().length() > 0){
			logger.debug("read Result of searchInfo " + resultBuffer.toString());
		}else{
			logger.debug("No Response Data Form  PbxEws");
		}

		br.close();
		
		JSONObject jObj  = new JSONObject(resultBuffer.toString());
		
		return jObj;		
	}


	/**
	 * 인증서 무시하고 접근....
	 * @param fileUrl
	 * @return
	 */
	public String createDownFileByHttps(String ttsFilePath, String fileUrl) throws  Exception {

		TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
		}};

		String midiFile = null;

		// Activate the new trust manager

		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		String fileName = fileUrl.substring(fileUrl.lastIndexOf("/"), fileUrl.length());

        midiFile =ttsFilePath + fileName;

        logger.info("tts mp3 path : " +  midiFile);

		File file =new File(midiFile);

		if(!file.exists()){
            file.getParentFile().mkdirs();
              logger.info("tts 저장폴더 생성 : ");
        }else{
			logger.info("tts 폴더 존재....");
		}

		URL url = new URL(fileUrl);
		URLConnection connection = url.openConnection();
		InputStream is = connection.getInputStream();

		ReadableByteChannel rbc = Channels.newChannel(is);
		FileOutputStream outputStream = new FileOutputStream(midiFile);
		outputStream.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);


		return fileName;
	}



	public String createDownFileByHttp(String ttsFilePath,String fileUrl) throws  Exception {

		String midiFile = null;
        String fileName = fileUrl.substring(fileUrl.lastIndexOf("/"), fileUrl.length());

		midiFile =ttsFilePath + fileName;
        File file =new File(midiFile);

        logger.info("tts mp3 path : " +  midiFile);

        if(!file.exists()){
            file.getParentFile().mkdirs();
            logger.info("tts 저장폴더 생성 : ");
        }

		URL url = new URL(fileUrl);
		URLConnection connection = url.openConnection();
		InputStream is = connection.getInputStream();

		ReadableByteChannel rbc = Channels.newChannel(is);
		java.io.FileOutputStream outputStream = new java.io.FileOutputStream(midiFile);
		outputStream.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

		return fileName;
	}

	/**
	 * http 통신용...
	 * @param Url
	 * @return
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public JSONObject sendReceiveHttp(String Url) throws Exception {

		URL url = new URL(Url);

		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		StringBuffer sb =  new StringBuffer();
      
		try{
			conn.setDoInput(true);            // 입력스트림 사용여부
			conn.setDoOutput(true);            // 출력스트림 사용여부
			conn.setUseCaches(false);        // 캐시사용 여부
			conn.setReadTimeout(2000000);        // 타임아웃 설정 ms단위
			conn.setRequestMethod("GET");   // Post로 Request하기

			BufferedReader br = new BufferedReader( new InputStreamReader(conn.getInputStream()));

			for(;;){
				String line =  br.readLine();
				if(line == null) break;
				sb.append(line+"\n");
			}

			br.close();
			conn.disconnect();
		}catch(Exception e){
			e.printStackTrace();
		}
		// System.out.println(sb.toString());               
		return new JSONObject(sb.toString());
	}


	public String getOperator(){
		String OS = System.getProperty("os.name").toLowerCase();
		String operator ="/";
		 if (OS.indexOf("win") >= 0) {
           operator ="\\";

        } else {
		 	operator ="/";
		 }
		 return operator;
	}
}
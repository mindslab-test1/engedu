package ai.mindslab.engedu.adminweb.controller.paraphrase;

import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.paraphrase.data.ParaphraseAdminVO;
import ai.mindslab.engedu.admin.paraphrase.service.ParaphraseAdminService;
import ai.mindslab.engedu.admin.paraphrase.service.ParaphraseExcel;
import ai.mindslab.engedu.adminweb.common.view.DownloadView;
import ai.mindslab.engedu.adminweb.common.view.ExcelView;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping("/view/paraphrase")
public class ParaphraseAdminController extends CommonController {

    private static Logger logger  = LoggerFactory.getLogger(ParaphraseAdminController.class);

    @Autowired
    private ParaphraseAdminService paraphraseAdminService;

    @Autowired
    DownloadView downloadView;
    @Autowired
    ParaphraseExcel paraphraseExcel;

    @RequestMapping("/list")
    public ModelAndView korToEndView(HttpServletRequest req, HttpServletResponse res) {
        ModelAndView view = new ModelAndView();
        view.setViewName("paraphrase/paraphrase_list");
        return view;

    }

    @RequestMapping("/paraphraseList")
    public ModelAndView getParaphraseList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }

        if(param.containsKey("sidx") && param.get("sidx").equals("wordId")){
            param.put("sidx", "word_Id");
        }

        startNum = rows * (page - 1);

        int totalCount = paraphraseAdminService.getParaphraseAdminCount(param);

        param.put("start", startNum);
        param.put("rows", rows);

        //게시판 리스트 가져오기
        List<ParaphraseAdminVO> list = paraphraseAdminService.getParaphraseAdminList(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }

    @RequestMapping(value = "/downSample")
    public ModelAndView downSample(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        String fileUrl  = req.getSession().getServletContext().getRealPath("resources/sample/paraphrase_sample.xlsx");

        logger.info("down sample url : " + fileUrl);

        File file = new File(fileUrl);

        view.addObject("downloadFile", file);
        view.setView(downloadView);

        return view;
    }

    @PostMapping(value="/insertData")
    public ModelAndView uploadFile(HttpServletRequest req,
                                   @RequestParam("file") MultipartFile uploadfile)throws Exception {
        ModelAndView view = new ModelAndView();
        UserVO vo = getUserInfo(req);
        BaseResponse<Object> resp = paraphraseAdminService.uploadExcelData(vo, uploadfile);

        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }

    @RequestMapping("/selectDetail")
    public ModelAndView selectDetail(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();

        Map<String, Object> param = reqToHash(req);

        ParaphraseAdminVO vo = paraphraseAdminService.selectParaphraseById(param);

        view.addObject("data", vo);
        view.setViewName("jsonView");
        return view;
    }


    @RequestMapping(value = "/updateParaphraseDetail")
    public ModelAndView updateKorengDetail(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();

        Map<String, Object> param = reqToHash(req);
        BaseResponse<Object> resp = paraphraseAdminService.updateParaphraseDetail(param);

        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;
    }

    @RequestMapping(value = "/deleteParaphrase")
    public ModelAndView deleteParaphrase(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        BaseResponse<Object> resp = paraphraseAdminService.deleteParaphrase(param);

        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;
    }

    @RequestMapping(value = "/excelDown")
    public ModelAndView excelDown(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        param.put("searchType",param.get("excelDownSearchType"));
        param.put("searchText",param.get("excelDownSearchText"));

        String date = new SimpleDateFormat("yyyymmddHHmmss", Locale.KOREA).format(new Date());
        String fileName = "Paraphrase_"+ date +".xlsx";
        String title ="동의어";

        paraphraseExcel.setTitle(title);
        Workbook book = paraphraseExcel.createExcel(param);


        view.setView(new ExcelView());
        view.addObject("workBook", book);
        view.addObject("fileName", fileName);
        view.addObject("title", title);

        return view;
    }


}

package ai.mindslab.engedu.adminweb.controller.eval;

import ai.mindslab.engedu.adminweb.common.http.HttpProcess;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Controller
@RequestMapping("view/eval/eng/test")
public class EvaluationTestAdminController extends CommonController {

    @Value("${client.websocket.domain}")
	private String domain;

    @Value("${client.server.domain}")
    private String serverUrl;

    @Autowired
    HttpProcess http;

    @RequestMapping("/eng/list")
    public ModelAndView engEvalView(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();

        view.addObject("domain", domain);
        view.addObject("userId", getUserInfo(req).getUserId());
        view.setViewName("eval/eng_eval");
        return view;

    }



    @RequestMapping("/kor/list")
    public ModelAndView korEvalView(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();

        view.addObject("domain", domain);
        view.addObject("userId", getUserInfo(req).getUserId());
        view.setViewName("eval/kor_eval");
        return view;

    }



    @RequestMapping("/evaltext")
    public ModelAndView evalText(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();

        Map<String, Object> param = reqToHash(req);
        StringBuilder sb = new StringBuilder();
        JSONObject obj = null;

        try {
            sb.append(serverUrl);
            sb.append("uapi/evaluation/getTestResult?");
            sb.append("inputStr=");
            sb.append(URLEncoder.encode(param.get("userText").toString(), "UTF-8"));
            sb.append("&answerStr=");
            sb.append(URLEncoder.encode(param.get("answerText").toString(), "UTF-8"));
            sb.append("&language=" + URLEncoder.encode(param.get("lang").toString(), "UTF-8"));

            String pattern = "^(https?)";

            // https 인지 아닌지 판별
            boolean secure= req.isSecure();
            Pattern p = Pattern.compile(pattern);
            Matcher mc = p.matcher(sb.toString());

            if(mc.find()){
                if(mc.group(1).equals("https")){
                      obj = http.sendReceiveHttps(sb.toString());
                }else{
                      obj = http.sendReceiveHttp(sb.toString());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
        }

        JSONObject resultObj = (JSONObject)obj.get("result");

        view.addObject("data", obj.toString());
        view.setViewName("jsonView");

        return view;

    }

}

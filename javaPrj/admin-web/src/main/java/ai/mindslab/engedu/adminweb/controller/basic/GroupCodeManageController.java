package ai.mindslab.engedu.adminweb.controller.basic;

import ai.mindslab.engedu.admin.code.group.dao.data.GroupCodeAdminVO;
import ai.mindslab.engedu.admin.code.group.service.GroupCodeAdminService;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("view/code/group")
public class GroupCodeManageController extends CommonController {


    @Autowired
    GroupCodeAdminService service;

    @RequestMapping("/list")
    public ModelAndView userAuthList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        view.setViewName("manage_code/group_code");
        return view;

    }


    @RequestMapping("/codeList")
    public ModelAndView codeList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }

        if(param.containsKey("sidx") && param.get("sidx").equals("endId")){
            param.put("sidx", "end_id");
        }

        startNum = rows * (page - 1);

        int totalCount = service.getGroupCodeAdminCount(param);

        param.put("start", startNum);
        param.put("rows", rows);

        //게시판 리스트 가져오기
        List<GroupCodeAdminVO> list = service.getGroupCodeAdminList(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping("/getGroupCodeAdmin")
    public ModelAndView getGroupCodeAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {

        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        GroupCodeAdminVO userInfo = service.getGroupCodeAdmin(param);

        view.addObject("data", userInfo);
        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping("/updateGroupCode")
    public ModelAndView updateGroupCode(HttpServletRequest req, HttpServletResponse res) throws Exception {
        Map<String, Object> param = reqToHash(req);
        ModelAndView view = new ModelAndView();

        UserVO vo = this.getUserInfo(req);


        param.put("updatorId", vo.getUserId());

        BaseResponse<Object> resp = service.updateGroupCode(param);

        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }



     @RequestMapping("/addGroupCode")
    public ModelAndView addGroupCode(HttpServletRequest req, HttpServletResponse res) throws Exception {
        Map<String, Object> param = reqToHash(req);
        ModelAndView view = new ModelAndView();

        UserVO vo = this.getUserInfo(req);

        param.put("creatorId", vo.getUserId());
        BaseResponse<Object> resp = service.addGroupCode(param);

        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;
    }


     @RequestMapping("/deleteGroupCode")
    public ModelAndView deleteGroupCode(HttpServletRequest req, HttpServletResponse res) throws EngEduException {

        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        UserVO vo = this.getUserInfo(req);

        BaseResponse<Object> resp = service.deleteGroupCode(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;
    }


}

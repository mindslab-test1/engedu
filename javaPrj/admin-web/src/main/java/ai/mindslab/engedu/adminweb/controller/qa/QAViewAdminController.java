package ai.mindslab.engedu.adminweb.controller.qa;

import ai.mindslab.engedu.admin.qa.list.dao.data.QADomainVO;
import ai.mindslab.engedu.admin.qa.list.service.QAListAdminService;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("view/qa/tab")
public class QAViewAdminController extends CommonController {

    @Autowired
    QAListAdminService service;

     @RequestMapping("/main")
    public ModelAndView qaMainView(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        view.setViewName("qa/qa_main");
        return view;

    }

    @RequestMapping("/list")
    public ModelAndView qaListView(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
         Map<String, Object> param = reqToHash(req);
        List<QADomainVO> list = service.getDomainList(param);
        view.addObject("domainList", list);
        view.setViewName("qa/qa_list");
        return view;

    }


      @RequestMapping("/index")
    public ModelAndView qaIndexView(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        view.setViewName("qa/qa_index");
        return view;

    }

     @RequestMapping("/domain")
    public ModelAndView qaDomainView(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        view.setViewName("qa/qa_domain");
        return view;

    }
}

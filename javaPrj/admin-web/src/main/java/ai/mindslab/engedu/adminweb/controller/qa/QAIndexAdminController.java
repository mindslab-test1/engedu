package ai.mindslab.engedu.adminweb.controller.qa;

import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.qa.index.dao.data.QAIndexAdminVO;
import ai.mindslab.engedu.admin.qa.index.service.QAIndexAdminService;
import ai.mindslab.engedu.admin.qa.list.dao.data.QAListAdminVO;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.bqa.dao.data.QaIndexingHistoryVo;
import ai.mindslab.engedu.bqa.service.BqaIndexingService;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("view/qa/index")
public class QAIndexAdminController extends CommonController {

    @Autowired
    QAIndexAdminService service;

    @Autowired
    BqaIndexingService indexingService;

     @RequestMapping("getlist")
     public ModelAndView getQaList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }

        if(param.containsKey("sidx") && param.get("sidx").equals("endId")){
            param.put("sidx", "end_id");
        }

        startNum = rows * (page - 1) ;

        int totalCount = service.getIndexHisgoryAdminListCount(param);

        param.put("offset", startNum);
        param.put("limit", rows);

        //게시판 리스트 가져오기
        List<QAIndexAdminVO> list = service.getIndexHisgoryAdminList(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }


     @RequestMapping("fullindex")
     public ModelAndView fullindex(HttpServletRequest req, HttpServletResponse res) {
         ModelAndView view = new ModelAndView();
         BaseResponse<Object> resp = new BaseResponse<>();

         view.setViewName("jsonView");

         try {

             Map<String, Object> param = reqToHash(req);
             UserVO vo = this.getUserInfo(req);

             QaIndexingHistoryVo indexVO = indexingService.fullIndexing(vo.getUserId());

             if(indexVO !=null){
                 resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                 resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
             }

         }catch(EngEduException e){
             e.printStackTrace();
             resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());

         }

         view.addObject("result", resp);
         return view;
     }


      @RequestMapping("addIndex")
     public ModelAndView addIndexing(HttpServletRequest req, HttpServletResponse res) {
         ModelAndView view = new ModelAndView();
         BaseResponse<Object> resp = new BaseResponse<>();

         view.setViewName("jsonView");

         try {

             Map<String, Object> param = reqToHash(req);
             UserVO vo = this.getUserInfo(req);

             QaIndexingHistoryVo indexVO = indexingService.addIndexing(vo.getUserId());

             if(indexVO !=null){
                 resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                 resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
             }

         }catch(EngEduException e){
             e.printStackTrace();
             resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());

         }

         view.addObject("result", resp);
         return view;
     }

}

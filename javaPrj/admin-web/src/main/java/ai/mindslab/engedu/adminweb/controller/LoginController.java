package ai.mindslab.engedu.adminweb.controller;

import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.login.service.LoginService;
import ai.mindslab.engedu.admin.user.dao.data.UserInfoSVO;
import ai.mindslab.engedu.adminweb.common.rsa.ZRsaSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.PrivateKey;

@Controller
@RequestMapping("/login")
public class LoginController {

    private static Logger logger  = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    LoginService service;

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @RequestMapping("/main")
    public ModelAndView mainView(HttpServletRequest req, HttpServletResponse res){
    	 ModelAndView view = new ModelAndView();


    	 logger.info("move login page");

    	try{

    	    ZRsaSecurity zSecurity = new ZRsaSecurity();
			PrivateKey privateKey = zSecurity.getPrivateKey();

			HttpSession session = req.getSession();
			// 세션에 공개키의 문자열을 키로하여 개인키를 저장한다.
			session.setAttribute("_rsaPrivateKey_", privateKey);
			// 공개키를 문자열로 변환하여 JavaScript RSA 라이브러리 넘겨준다.
			String publicKeyModulus = zSecurity.getRsaPublicKeyModulus();
			String publicKeyExponent = zSecurity.getRsaPublicKeyExponent();

			view.addObject("publicKeyModulus", publicKeyModulus);
			view.addObject("publicKeyExponent", publicKeyExponent);

    	}catch(Exception e){
			logger.error("login error: ", e);
			e.printStackTrace();
		}

    	 view.setViewName("login/login");
        return  view;
    }


    @RequestMapping("/userlogin")
    public ModelAndView loginUser(UserInfoSVO svo,
                                  HttpServletRequest req,
                                  HttpServletResponse res) throws Exception{
        ModelAndView view = new ModelAndView();
        ZRsaSecurity rsa  = new ZRsaSecurity();

        String id = svo.getUserId();
        String key = svo.getUserKey();


        HttpSession session = req.getSession();

        PrivateKey privateKey = (PrivateKey) session.getAttribute("_rsaPrivateKey_");


        String userId =  rsa.decryptRSA(privateKey, id);
        String userKey = rsa.decryptRSA(privateKey, key);


        UserVO userInfo =  service.loginUser(userId);
        try {
            if(userInfo !=null){  // 사용자가 존재한다면

            	boolean math = passwordEncoder.matches(userKey, userInfo.getUserPass());

            	if(math) {

            	    session.removeAttribute("_rsaPrivateKey_"); // 키의 재사용을 막는다. 다만 로그인되기전까지만 유지...
                    session.setAttribute("userInfo", userInfo); // 세션에다가 사용자 정보 유지
                    session.setMaxInactiveInterval(60*30); // 30분 타임제한...

                    view.addObject("resultCode", 200);
                    view.addObject("resultMsg", "OK");
                    view.addObject("userVO", userInfo);
            	}else {
            	    view.addObject("resultCode", 400);
            	    view.addObject("resultMsg", "비밀번호를 확인해주십시오.");
            	}


            }else{
                view.addObject("resultCode", 400);
                view.addObject("resultMsg", "아이디가 존재하지 않습니다.");
            }

        }catch (Exception e){
            logger.error("loginError :" + e.getMessage());
        }
        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping("/checkSession")
    public ModelAndView checkSession(HttpServletRequest req, HttpServletResponse res){
        ModelAndView view = new ModelAndView();
        try{
            HttpSession session = req.getSession();

            if(session.getAttribute("userInfo") !=null){
                view.addObject("resultCode", 200);
                view.addObject("resultMsg", "OK");
            }else{
                view.addObject("resultCode", 300);
                view.addObject("resultMsg", "No Session");
            }

        }catch(Exception e){
            view.addObject("resultCode", 400);
            view.addObject("resultMsg", "fail");
            e.printStackTrace();
        }

        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping("/logout")
    public ModelAndView loginOutUser(HttpServletRequest req, HttpServletResponse res){
    	ModelAndView view = new ModelAndView();
    	try{
			  HttpSession session = req.getSession();
			  if(session.getAttribute("userInfo") !=null){
				  session.removeAttribute("userInfo");
				  view.addObject("resultCode", 200);
	        	  view.addObject("resultMsg", "OK");
			  }

		  }catch(Exception e){
			 view.addObject("resultCode", 400);
        	  view.addObject("resultMsg", "fail");
			  e.printStackTrace();
		  }
    	  view.setViewName("jsonView");

    	return view;
    }

}

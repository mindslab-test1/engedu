package ai.mindslab.engedu.adminweb.common.base;


import ai.mindslab.engedu.admin.tts.dao.data.TtsLanguageVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class BaseAdminInfo {

    @Value("${client.brand.code}")
    private String brandId;


    /**
     * ADMIN 첫 로딩시 뿌려줄 PAGE SETTING
     * @return
     */
    public String getFirstPage(){


        String firstPage = "view/eval/eng/test/kor/list";
        switch (brandId){
            case "BP0002" :
                firstPage = "view/eval/eng/test/eng/list";
                break;
            case "mAIEnglish" :
                firstPage = "view/eval/eng/test/eng/list";
                break;
        }

        return firstPage;
    }

    /**
     * TTS 관리 > TTS 테스트 > 언어 선택 COMBO BOX GET LIST
     * @return
     */
    public ArrayList<TtsLanguageVO> getTtsLangSelectList() {

        ArrayList<TtsLanguageVO> result = new ArrayList<>();

        switch (brandId){
            case "BP0002" :
                result.add(new TtsLanguageVO("영어","eng"));
                break;
            default:
                result.add(new TtsLanguageVO("영어","eng"));
                result.add(new TtsLanguageVO("한국어","kor"));
        }

        return result;

    }

}

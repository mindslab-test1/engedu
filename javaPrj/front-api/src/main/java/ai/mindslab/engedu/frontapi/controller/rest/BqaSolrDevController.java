package ai.mindslab.engedu.frontapi.controller.rest;


import ai.mindslab.engedu.bqa.solr.BqaSolrClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
@RequestMapping("/bqaSolrDev")
public class BqaSolrDevController {

    private Logger logger = LoggerFactory.getLogger(BqaSolrDevController.class);

    @Autowired
    private BqaSolrClient bqaSolrClient;

    @RequestMapping("/deleteSolrDomain")
    public void deleteSolrDomain(@RequestParam(name = "domainId") int domainId){

        try{
            bqaSolrClient.removeOneDomain(domainId);
        }catch(Exception e){
            logger.warn("IOException search Error/{}", e);
        }
    }

}

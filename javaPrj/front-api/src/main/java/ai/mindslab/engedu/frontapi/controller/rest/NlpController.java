package ai.mindslab.engedu.frontapi.controller.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.engedu.bqa.dao.data.NlpAnalysisVo;
import ai.mindslab.engedu.bqa.dao.data.NlpMorpVo;
import ai.mindslab.engedu.bqa.nlp.NlpAnalyzeClient;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/nlp")
public class NlpController {
	
	@Autowired
	private NlpAnalyzeClient nlpAnalyzeClient;

    @RequestMapping("/analyze")
    public List<NlpMorpVo> analyze( 
    		@RequestParam(name = "question", defaultValue = "", required=true) String question, 
    		@RequestParam(name = "lang", defaultValue = "kor", required=true) String lang ){
    	
    	/*String questionMorph = nlpAnalyzeClient.analyze(question);
    	log.debug("questionMorph:{}", questionMorph);*/
    	
    	List<NlpAnalysisVo> list = nlpAnalyzeClient.nlpTestAnalyze(question,lang);
    	
    	List<NlpMorpVo> morpList = null;
    	
//    	log.debug(list.size()+"");

    	if (list != null) {
    		morpList =list.get(0).getMorpList();
			
    		for (NlpMorpVo nlpMorpVo : morpList) {
    			log.debug("nlpMorpVo.toString():{}", nlpMorpVo.toString());
				
			}
		}
    	
        return morpList;

    }


}

package ai.mindslab.engedu.frontapi.controller.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/bqaImagineClient")
public class BqaImagineClientController {

    @RequestMapping("/getClientUserInfo")
    public Map<String,Object> getClientUserInfo(Map<String,Object> object){

        Map<String,Object> result = new HashMap();
        result.put("code","0");
        result.put("_name_","뽀로로");
        result.put("_khh_","3시 8분");
        result.put("_mhh_","3시 50분");
        return result;

    }
}
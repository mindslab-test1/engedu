package ai.mindslab.engedu.frontapi.common;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.service.ApiLogService;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ResponseLogResolver {
	
	@Autowired
	private ApiLogService apiLogService;
	
	private static ApiLogService staticApiLogService;
	
	
	@PostConstruct
	private void initStatic() {
		staticApiLogService = apiLogService;
	}
	

	public static void insertResponseLog(Response response, String userId, String errText)  {
		
		try {

			String resultId = staticApiLogService.insertApiLog(
					response.getResultType()
					, Integer.parseInt(response.getResCode())
					, response.getResMsg()
					, new ObjectMapper().writeValueAsString(response)
					, userId
			);
			response.setResultId(resultId);

			log.info("resultId:::::"+ response.getResultId());

			if(errText != null) {
				staticApiLogService.insertErrorLog(resultId, errText, userId);
			}

		} catch (NumberFormatException | JsonProcessingException e) {
			e.printStackTrace();
		}
	}
}

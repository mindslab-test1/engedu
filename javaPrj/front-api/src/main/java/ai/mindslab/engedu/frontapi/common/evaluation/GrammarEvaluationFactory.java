package ai.mindslab.engedu.frontapi.common.evaluation;


import ai.mindslab.engedu.common.codes.IGrammarEvaluationCodes;
import ai.mindslab.engedu.frontapi.client.GrammarEvaluationDiffClient;
import ai.mindslab.engedu.frontapi.client.GrammarEvaluationEqualClient;
import ai.mindslab.engedu.frontapi.client.IGrammarEvaluationClient;

public class GrammarEvaluationFactory {

    /**
     * 문법 평가 Type에 따라 분리
     * @param paramGrammarEvaluationType 문법 평가 type ex) DIFF || EQUAL
     * @return IGrammarEvaluationClient
     */
    public IGrammarEvaluationClient getClient(String paramGrammarEvaluationType,String paramLanguage) {

        IGrammarEvaluationClient clientResult;

        // TODO diff / enum 변경 요망
        switch (paramGrammarEvaluationType){

            // DiffMatchPatchUtil 클래스를 이용한 문법 평가
            case IGrammarEvaluationCodes.DIFF :

                clientResult = new GrammarEvaluationDiffClient(paramLanguage);
                break;

            // 각각의 문자를 이용한 문법 평가
            case IGrammarEvaluationCodes.EQUAL:

                clientResult = new GrammarEvaluationEqualClient();
                break;

            default:

                clientResult = new GrammarEvaluationDiffClient(paramLanguage);

        }

        return clientResult;

    }

}

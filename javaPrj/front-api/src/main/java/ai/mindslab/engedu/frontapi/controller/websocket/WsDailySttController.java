package ai.mindslab.engedu.frontapi.controller.websocket;

import ai.mindslab.engedu.frontapi.service.DailySttService;
import ai.mindslab.engedu.frontapi.service.DialogSttService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import java.util.Map;

@Slf4j
@Component
public class WsDailySttController extends BinaryWebSocketHandler {

	@Autowired
	private DailySttService dailySttService;
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		try {
			//super.afterConnectionEstablished(session);

			Map<String, Object> requestMap = session.getAttributes();
			log.info(requestMap.toString());
			dailySttService.openSession(session, requestMap);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
		dailySttService.onMessage(session, message.getPayload(), false);
		super.handleBinaryMessage(session, message);
	}
	
	
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
    	try {
    	log.debug("afterConnectionClosed {}", status.toString());
//    	dailySttService.onClose(session);
		dailySttService.closeSttClient(session);
    	super.afterConnectionClosed(session, status);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
}

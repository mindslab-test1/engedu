package ai.mindslab.engedu.frontapi.controller.data;

import ai.mindslab.engedu.evaluation.dao.data.GrammarEvaluationDetailVO;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import lombok.Data;

import java.util.ArrayList;

@Data
public class Result {

	/** 문법 점수 */
	private String grammarScore = "0";
	/** 문법 상세 점수 */
	private ArrayList<GrammarEvaluationDetailVO> grammarScoreDetail;
	/** 발음 점수 (client 용)*/
	private String pronounceScore = "0";
	/** 발음 단어일 경우 점수 */
	private String evalScore;
	/** 발음 평균 점수(speed,rhythm,intonation..등등 평균) */
	private String pronounceHolisticScore;
	/** 발음 속도 점수 */
	private String pronounceSpeedScore;
	/** 발음 리듬감 점수 */
	private String pronounceRhythmScore;
	/** 발음 억양 점수 */
	private String pronounceIntonationScore;
	/** 발음 발성 점수 */
	private String pronounceSegmentalScore;
	/** 단어 발성에 대한 segment 점수 */
	private String pronounceSegmentalFeat19Score;

	/** 평균 점수 = 문법 점수 + 발음 점수 */
	private String averageScore = "0";
	/** 정답 문장 */
	private String answerText = "";
	/** 학습자 문장( STT에서 변환된 문장) */
	private String userText = "";

	private String userAbbreviationConvertText;
	/** 학습자 음성 녹음 url*/
	private String recordUrl = "";
	/** 응답 TTS */
	private String ttsUrl = "";
	/** 응답 텍스트 */
	private String ttsText = "";
	/** 추가 데이터 타입 */
//	private String extType = "";
	/** 추가 데이터 값 */
//	private String extData = "";

	/** 영어 대화 결과 */
	private String freeTalkResult;
	
	/** 파닉스 STP 결과 */
	private String phonicsStpResult;
	
	/** 파닉스 DIC 결과 */
	private String phonicsDicResult;
	
	/** 파닉스 waveImageUrl 결과 */
	private String wavDiagramUrl;
	
	/* 알파벳 STL 결과 */

	/** 파닉스 정답여부 결과 */
	private int phonicsResult;    //0, 1
	/** 파닉스 사용자 발음 결과 */
	private  String phonicsUserPron;

	private String letterStlResult;

	private ExtInfo extInfo;
	

}

package ai.mindslab.engedu.frontapi.client;
import ai.mindslab.engedu.common.codes.IExtensionCodes;
import ai.mindslab.engedu.common.codes.ILanguageCodes;
import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.utils.DateUtil;
import ai.mindslab.engedu.common.utils.FileCreateUtil;
import lombok.NoArgsConstructor;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import ai.mindslab.engedu.common.data.TtsPreVO;
import ai.mindslab.engedu.common.utils.TtsPreUtil;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

@Slf4j
@NoArgsConstructor
public class TtsMindsLabClient implements ITtsClient{

    @Autowired
    Environment env;

    // tts 서버 ip
    private String serverIp;

    // tts 서버 port
    private int serverPort;

    // tts local,dev 테스트시 저장 폴더
    private String ttsDevDir;

    // tts 언어
    private String language;

    // 사용자 발화
    private String inputStr;

    // 서비스 타입
    private String serviceType;

    // tts 저장 경로
    private String filePath;

    // 파일명
    private String fileName;

    private String activeProfile;

    private String domain;

    private final static String TTS_URL = "https://125.132.250.244:13246/tts-rest/tts/block/speak";

    public TtsMindsLabClient(
            String paramTtsDevDir,
            String paramLanguage,
            String paramInputStr,
            String paramServiceType,
            String paramFilePath,
            String paramFileName,
            String paramActiveProfile,
            String paramDomain)
    {

        this.ttsDevDir = paramTtsDevDir;
        this.language = paramLanguage;
        this.inputStr = paramInputStr;
        this.serviceType = paramServiceType;
        this.filePath = paramFilePath;
        this.fileName = paramFileName;
        this.activeProfile = paramActiveProfile;
        this.domain = paramDomain;
    }


    public void setServerInfo(String paramServerIp,int paramServerPort){
        this.serverIp = paramServerIp;
        this.serverPort = paramServerPort;
    }

    public Response getTtsResponse(){

        Response responseResult = new Response();

        Result result = new Result();

        ArrayList<String> fileList = new ArrayList<>();


        try {

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));

            ArrayList<TtsPreVO> messageList =  new TtsPreUtil().getWordLanguageDivision(this.inputStr,this.serviceType);
            String ttsInputStr = this.inputStr;
            FileCreateUtil fileCreateUtil = new FileCreateUtil();
            String currentTime = new DateUtil().getCurrentTime(DateUtil.TO_SECOND);
            String fileFullPath = fileCreateUtil.makeFolder(this.filePath,currentTime);

            String ttsFileName = getTtsFileName(fileFullPath);

            for(int i=0; i<messageList.size(); i++){

                String messageLang = messageList.get(i).getLanguage();

                String lang = getMessageLanguage(messageLang);

                JSONObject param = new JSONObject();
                param.put("lang", lang);
                param.put("text", messageList.get(i).getUtter());
                HttpEntity<JSONObject> entity = new HttpEntity<>(param, headers);
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<byte[]> response = restTemplate.postForEntity(this.TTS_URL, entity, byte[].class, "1");

                String tempFileNm;

                if(messageList.size() == 1){

                    tempFileNm = ttsFileName;

                }else{

                    String[] ttsArr = ttsFileName.split(IExtensionCodes.EXT_WAV);
                    tempFileNm = ttsArr[0]+i + IExtensionCodes.EXT_WAV;

                }

                if (response.getStatusCode() == HttpStatus.OK) {

                    Files.write(Paths.get(tempFileNm), response.getBody());

                }

            }

            soxMergeFile(fileList,ttsFileName);

            fileCreateUtil.writeHandWavMp3(ttsFileName,ttsFileName.replaceAll(IExtensionCodes.EXT_WAV,IExtensionCodes.EXT_MP3) );

            tempFileDelete(fileList);

            result.setTtsText(ttsInputStr);
            result.setTtsUrl(domain +ttsFileName.replaceFirst("/","").replaceAll(IExtensionCodes.EXT_WAV,IExtensionCodes.EXT_MP3));
            responseResult.setResult(result);
            responseResult.setResCode(String.valueOf(IRestCodes.ERR_CODE_SUCCESS));
            responseResult.setResMsg(IRestCodes.ERR_MSG_SUCCESS);

        }catch (Exception e){

            responseResult.setResult(result);
            responseResult.setResCode(String.valueOf(IRestCodes.ERR_CODE_TTS_EVALUATION_ERROR));
            responseResult.setResMsg(IRestCodes.ERR_MSG_TTS_EVALUATION_ERROR);
            log.error(e.getMessage());

        }

        return  responseResult;

    }

    /**
     * 문장 언어 ko_KR or en_US
     * @param paramMessageLang 언어
     * @return 문장 언어 ko_KR or en_US
     */
    private String getMessageLanguage(String paramMessageLang) {

        String result;

        if(ILanguageCodes.KOR.equals(paramMessageLang)){

            result = ILanguageCodes.KO_KR;

        }else{

            result = ILanguageCodes.EN_US;

        }

        return result;

    }

    /**
     * 파일 2개 이상일 경우 File Merge SOX
     * @param fileList 파일 List
     * @param paramTtsFileName TTS File Name
     * @throws Exception
     */
    private void soxMergeFile(ArrayList<String> fileList,String paramTtsFileName) throws Exception{

        if(fileList.size() > 1){

            Runtime rt = Runtime.getRuntime();
            Process pc = null;
            String command = "sox ";

            for(int j=0; j<fileList.size(); j++){

                command += fileList.get(j) + " ";

            }

            command += paramTtsFileName;
            log.debug("sox command ======= "+ command);
            pc = rt.exec(command);

            int processResult = pc.waitFor();

            if(processResult != 0){

                throw new Exception();
            }

            pc.destroy();

        }

    }

    /**
     * 임시 파일 삭제 ( SOX )
     * @param paramFileList 임시 파일 리스트
     */
    private void tempFileDelete(ArrayList<String> paramFileList) throws Exception{

        if(paramFileList.size() > 1){

            for(int i=0; i< paramFileList.size(); i++){

                File file = new File(paramFileList.get(i));

                if(file.exists()){

                    file.delete();

                }

            }

        }

    }

    private String getTtsFileName(String paramFileFullPath) {

        String ttsFileName;

        ttsFileName = paramFileFullPath.replaceAll("\\\\","/")+ IMarkCodes.FORWARD_SLASH+fileName;

        return ttsFileName;
    }

    public void shutdown() throws InterruptedException {}

}

package ai.mindslab.engedu.frontapi.controller.rest;


import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.engedu.bqa.service.BqaSearchService;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;

@Controller
@RestController
@RequestMapping("/bqa")
public class BqaController implements IRestCodes {

	private Logger logger = LoggerFactory.getLogger(BqaController.class);

	@Autowired
	private BqaSearchService bqaSearchService;
	
	@RequestMapping(value = "/search", method = { RequestMethod.POST })
	public @ResponseBody BaseResponse<?> search(@RequestParam(name = "question", required = true) String question,
			@RequestParam(name = "searchFlowType", defaultValue = "0") int searchFlowType,
			@RequestParam(name = "domainIds", required = true) int[] domainIds) throws EngEduException {
		logger.info("RestController search/{}/{}", question, domainIds);

		BaseResponse<Object> resp = null;
		try {
			if(domainIds == null || domainIds.length == 0) {
				resp = new BaseResponse<>(ERR_CODE_SEARCH_DOMAIN_PARAMETER_ERROR, ERR_MSG_SEARCH_DOMAIN_PARAMETER_ERROR);
				throw new EngEduException(ERR_CODE_SEARCH_DOMAIN_PARAMETER_ERROR, ERR_MSG_SEARCH_DOMAIN_PARAMETER_ERROR);
			}
			resp = bqaSearchService.search(question,searchFlowType,domainIds);

			logger.info("search Object/{}/{}", question, resp.getData());
		} catch (EngEduException e) {
			resp = new BaseResponse<>(e.getErrCode(), e.getErrMsg());
			throw new EngEduException(e.getErrCode(), e.getErrMsg());
		}
		finally {
			return resp;
		}
	}
}
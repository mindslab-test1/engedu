package ai.mindslab.engedu.frontapi.common;

import java.util.Map;

import javax.annotation.PostConstruct;

import ai.mindslab.engedu.intent.IntentServiceType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.socket.WebSocketSession;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.service.SttConfigService;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SttModelResolver {
	
	@Autowired
	private SttConfigService sttConfigService;
	
	private static SttConfigService staticSttConfigService;
	
	@Value("${brain.stt.model}")
	private String sttModel;
	
	private static String BASEMODEL="";
	
	@PostConstruct
	private void initStaticConfigService() {
		staticSttConfigService = sttConfigService;
		BASEMODEL= sttModel;
	}
	
	

	public static String getModel(WebSocketSession session, Map<String, Object> params)  {

		String userId = (String) params.get("userId");
		String model="";
		String serviceType = "";
		try {
			serviceType = staticSttConfigService.getServiceType(userId);

			if(StringUtils.equals(IntentServiceType.SPEEDGAME, serviceType)) {

				model = staticSttConfigService.getSpeedGameModel(userId);
//				model = BASEMODEL;
			} else {

				model = staticSttConfigService.getModel(userId);
			}

			if (ObjectUtils.isEmpty(model)) {
				model = BASEMODEL;
			}
		
		} catch (Exception e) {
			model=BASEMODEL;
			e.printStackTrace();
		}
		
		log.debug("SttModelResolver getModel::::"+model);
		//TODO: 현재 context에 맞게 model 선택, 입력, 값은 DB table 정보에 맞게 수정 필요
		return model;
	}
}

package ai.mindslab.engedu.frontapi.client;

import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.common.codes.IPronounceCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.utils.EngStringUtil;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;

import engedu.eng_evaluation.EnglishEvaluation;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@NoArgsConstructor
@Slf4j
public class PhonicsEvaluationClient {

    private ManagedChannel channel;
    private engedu.eng_evaluation.EvaluationServiceGrpc.EvaluationServiceBlockingStub blockingStub;
    private String activeProfile;

    public PhonicsEvaluationClient(String paramServerIP, int paramServerPort, String paramActiveProfile){

        this.channel = ManagedChannelBuilder.forAddress(paramServerIP, paramServerPort).usePlaintext().build();
        this.blockingStub = engedu.eng_evaluation.EvaluationServiceGrpc.newBlockingStub(channel);
        this.activeProfile = paramActiveProfile;

    }


    public Response getPhonicsResponseInfo(
            String paramAnswerText,
            String paramCheckSymbol,
            String paramFileWriteWavName)
    {


        EnglishEvaluation.PhonicsResponse phonicsResponse = getPhonicsAnalyzeResult(paramAnswerText,
                paramCheckSymbol,
                paramFileWriteWavName);
        Response response = new Response();
        Result result = new Result();

        log.debug("phonicsResponse!!!!!!!!!!!!!!!" + phonicsResponse );

        result.setPhonicsResult(phonicsResponse.getStatus());
        result.setPhonicsUserPron(phonicsResponse.getUserPron());
        response.setResult(result);
        response.setResCode("" + phonicsResponse.getResultCode());

        return response;

    }

    private EnglishEvaluation.PhonicsResponse getPhonicsAnalyzeResult(
            String paramAnswerText,
            String paramCheckSymbol,
            String paramFileWriteWavName)
    {


        try{

            log.debug("====================================== >>> paramAnswerText {}", paramAnswerText);

            EnglishEvaluation.PhonicsRequest evaluationRequest = EnglishEvaluation.PhonicsRequest.newBuilder()
                    .setAnswerText(paramAnswerText)
                    .setChkSym(paramCheckSymbol)
                    .setFilename(paramFileWriteWavName)
                    .build();

            return this.blockingStub.withDeadlineAfter(30, TimeUnit.SECONDS).phonicsAnalyze(evaluationRequest);

        }catch (Exception e){

            log.error("====================================== >>>  getPhonicsAnalyzeResult Error : {} " + e.toString());
            e.printStackTrace();
            return null;

        }

    }

    public void shutdown() throws InterruptedException {

        if (this.channel != null) {

            this.channel.shutdownNow();

        }
    }

}
package ai.mindslab.engedu.frontapi.controller.sample.dialog;


import ai.mindslab.engedu.intent.CalculateIntentExecute;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 계산기 sample rest api
 */
@RestController
@RequestMapping("/calculate")
public class CalculateSampleController {

    @Autowired
    private CalculateIntentExecute calculateIntentExecute;


    /***
     * @param paramInputStr 계산  문장 ex) 2 더하기 3 || 3 빼기 2 || 4 나누기 2
     * @return
     * @throws Exception
     */
    @RequestMapping("/sample")
    public IntentExecuteMessageVO sample(
            @RequestParam(name="inputStr") String paramInputStr) throws Exception
    {
        IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
        intentExecuteVO.setInputStr(paramInputStr);
        IntentExecuteMessageVO intentExecuteMessageVO = calculateIntentExecute.execute(intentExecuteVO);
        return intentExecuteMessageVO;

    }

}

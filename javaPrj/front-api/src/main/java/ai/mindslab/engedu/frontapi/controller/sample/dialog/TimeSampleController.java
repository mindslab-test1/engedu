package ai.mindslab.engedu.frontapi.controller.sample.dialog;


import ai.mindslab.engedu.intent.TimeIntentExecute;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/time")
public class TimeSampleController {


    @Autowired
    private TimeIntentExecute timeIntentExecute;
    /***
     * @param paramInputStr 사용자 발화 문장  ex) 지금 몇시야?
     * @return
     * @throws Exception
     */
    @RequestMapping("/sample")
    public IntentExecuteMessageVO sample(
            @RequestParam(name="inputStr") String paramInputStr) throws Exception
    {
        IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
        intentExecuteVO.setInputStr(paramInputStr);
        IntentExecuteMessageVO intentExecuteMessageVO = timeIntentExecute.execute(intentExecuteVO);
        return intentExecuteMessageVO;

    }
}

package ai.mindslab.engedu.frontapi.controller.sample.dialog;


import ai.mindslab.engedu.intent.KorDicAntonymIntentExecute;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 국어사전 반의어 sample rest api
 */
@RestController
@RequestMapping("/korDicAntonym")
public class KorDicAntonymSampleController {

    @Autowired
    private KorDicAntonymIntentExecute korDicAntonymIntentExecute;

    /***
     * @param paramInputStr 국어사전 반의어 단어 ex) 가구
     * @return
     * @throws Exception
     */
    @RequestMapping("/sample")
    public IntentExecuteMessageVO sample(
            @RequestParam(name="inputStr") String paramInputStr) throws Exception
    {
        IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
        intentExecuteVO.setInputStr(paramInputStr);
        IntentExecuteMessageVO intentExecuteMessageVO = korDicAntonymIntentExecute.execute(intentExecuteVO);
        return intentExecuteMessageVO;
    }

}

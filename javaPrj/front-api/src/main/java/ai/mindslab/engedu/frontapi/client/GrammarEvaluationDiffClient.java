package ai.mindslab.engedu.frontapi.client;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.utils.DiffMatchPatchUtil;
import ai.mindslab.engedu.evaluation.dao.data.GrammarEvaluationDetailVO;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
@Slf4j
public class GrammarEvaluationDiffClient implements  IGrammarEvaluationClient{

    private String language;

    public GrammarEvaluationDiffClient(String language) {
        this.language = language;
    }

    @Override
    public Response getGrammarEvaluationInfo(String paramInputStr, String paramAnswerStr) {

        Response response = new Response();
        Result grammarResult = new Result();

        try{

            if(IRestCodes.ERR_MSG_GRAMMAR_EVALUATION_ERROR.equals(paramInputStr)){
                throw  new Exception();
            }
            DiffMatchPatchUtil dmp = new DiffMatchPatchUtil();
            LinkedList<DiffMatchPatchUtil.Diff> diffs = dmp.diffWordMode(paramAnswerStr, paramInputStr);

            int score = dmp.getScore(diffs, paramAnswerStr);
            List<DiffMatchPatchUtil.WordScore> scoreDetail = dmp.getScoreDetail(diffs, paramAnswerStr);

            grammarResult = setGrammarResult(paramInputStr,paramAnswerStr,score,scoreDetail);
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_SUCCESS));
            response.setResMsg(IRestCodes.ERR_MSG_SUCCESS);
            response.setResult(grammarResult);

        }catch(Exception e){
            e.printStackTrace();
            log.error("getGrammarEvaluationInfo Error:{}" + e.getMessage());
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_GRAMMAR_EVALUATION_ERROR));
            response.setResMsg(IRestCodes.ERR_MSG_GRAMMAR_EVALUATION_ERROR);
            response.setResult(grammarResult);
        }

        return response;

    }

    @Override
    public void shutdown() {

    }

    private Result setGrammarResult(
            String paramInputStr,
            String paramAnswerStr,
            int paramScore,
            List<DiffMatchPatchUtil.WordScore> paramScoreDetail)
    {

        Result result = new Result();

        if(language != null && language.toUpperCase().equals("KOR")){

            result.setAverageScore(String.valueOf(paramScore));

        }else{

            ArrayList<GrammarEvaluationDetailVO> scoreDetail = new ArrayList<>();
            // 점수가 0점 이하면 0점으로 처리 즉 최저 점수는 0점
            paramScore = paramScore >= 0 ? paramScore : 0;
            result.setGrammarScore(String.valueOf(paramScore));
            for(DiffMatchPatchUtil.WordScore detail : paramScoreDetail) {
                scoreDetail.add(new GrammarEvaluationDetailVO(detail.getWord(),detail.getScore()));
            }

            result.setGrammarScoreDetail(scoreDetail);

        }

        result.setUserText(paramInputStr);
        result.setAnswerText(paramAnswerStr);
        return result;
    }
}

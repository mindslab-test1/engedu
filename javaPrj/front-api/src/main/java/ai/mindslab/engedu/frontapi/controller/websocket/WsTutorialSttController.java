package ai.mindslab.engedu.frontapi.controller.websocket;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import ai.mindslab.engedu.frontapi.service.TutorialSttService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class WsTutorialSttController extends BinaryWebSocketHandler {

	@Autowired
	private TutorialSttService tutorialSttService ;
	
	@Override
	protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
		tutorialSttService.onMessage(session, message.getPayload(), false);
		super.handleBinaryMessage(session, message);
	}
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		try {

			Map<String, Object> requestMap = session.getAttributes();
			log.info(requestMap.toString());
			tutorialSttService.openSession(session, requestMap);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
    	try {
    	log.debug("afterConnectionClosed {}", status.toString());
    	tutorialSttService.onClose(session);
    	super.afterConnectionClosed(session, status);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
}

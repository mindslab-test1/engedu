package ai.mindslab.engedu.frontapi.controller.sample.dialog;


import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkQuestionVO;
import ai.mindslab.engedu.intent.EngFreeTalkIntentExecute;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 영어대화 sample rest api
 */
@RestController
@RequestMapping("/engFreeTalk")
public class EngFreeTalkSampleController {

    @Autowired
    private EngFreeTalkIntentExecute engFreeTalkIntentExecute;

    /**
     * @param paramInputStr 사용자 발화 문장 ex) yes what is it?
     * @param paramBrandId brandId ex) BP0001
     * @param paramBookId bookdId ex) B05
     * @param paramChapterId chapterId 1
     * @param paramQuestionId questionId 1
     * @return
     * @throws Exception
     */
    @RequestMapping("/sample")
    public IntentExecuteMessageVO sample(
            @RequestParam(name="inputStr") String paramInputStr,
            @RequestParam(name="brandId") String paramBrandId,
            @RequestParam(name="bookId") String paramBookId,
            @RequestParam(name="chapterId") String paramChapterId,
            @RequestParam(name="questionId") String paramQuestionId) throws Exception
    {
        IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
        intentExecuteVO.setInputStr(paramInputStr);

        EngFreeTalkQuestionVO engFreeTalkQuestionVO = new EngFreeTalkQuestionVO();
        engFreeTalkQuestionVO.setBrandId(paramBrandId);
        engFreeTalkQuestionVO.setBookId(paramBookId);
        engFreeTalkQuestionVO.setChapterId(paramChapterId);
        engFreeTalkQuestionVO.setQuestionId(paramQuestionId);

        intentExecuteVO.setEngFreeTalkQuestionVO(engFreeTalkQuestionVO);
        IntentExecuteMessageVO intentExecuteMessageVO = engFreeTalkIntentExecute.execute(intentExecuteVO);
        return intentExecuteMessageVO;
    }
}

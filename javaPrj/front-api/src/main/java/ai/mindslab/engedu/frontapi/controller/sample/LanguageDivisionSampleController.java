package ai.mindslab.engedu.frontapi.controller.sample;


import ai.mindslab.engedu.common.data.TtsPreVO;
import ai.mindslab.engedu.common.utils.TtsPreUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/languageDivision")
public class LanguageDivisionSampleController {

    @RequestMapping("/sample")
    public ArrayList<TtsPreVO> aaa(
            @RequestParam(name="inputStr",defaultValue = "i love you") String paramInputStr){
        ArrayList<TtsPreVO> messageList =  new TtsPreUtil().getWordLanguageDivision(paramInputStr,"");
        return messageList;
    }
}

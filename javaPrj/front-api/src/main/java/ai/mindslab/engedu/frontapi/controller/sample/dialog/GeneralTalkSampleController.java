package ai.mindslab.engedu.frontapi.controller.sample.dialog;


import ai.mindslab.engedu.intent.GeneralTalkIntentExecute;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * bqa sample rest api
 */
@RestController
@RequestMapping("/generalTalk")
public class GeneralTalkSampleController {


    @Autowired
    private GeneralTalkIntentExecute generalTalkIntentExecute;

    /***
     * @param paramInputStr 사용자 발화 문장  ex) 디봇 하루 종일 뭐했어?
     * @return
     * @throws Exception
     */
    @RequestMapping("/sample")
    public IntentExecuteMessageVO sample(
            @RequestParam(name="inputStr") String paramInputStr,
            @RequestParam(name="userId") String userId) throws Exception
    {
        IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
        intentExecuteVO.setInputStr(paramInputStr);
        intentExecuteVO.setUserId(userId);
        IntentExecuteMessageVO intentExecuteMessageVO = generalTalkIntentExecute.execute(intentExecuteVO);
        return intentExecuteMessageVO;

    }
}

package ai.mindslab.engedu.frontapi.controller.sample.dialog;

import ai.mindslab.engedu.common.base.EnvironmentBase;
import ai.mindslab.engedu.common.codes.IExtensionCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.data.DialogVO;
import ai.mindslab.engedu.common.service.DialogSttPostService;
import ai.mindslab.engedu.common.service.UserService;
import ai.mindslab.engedu.common.utils.DateUtil;
import ai.mindslab.engedu.common.utils.FileCreateUtil;
import ai.mindslab.engedu.frontapi.client.ITtsClient;
import ai.mindslab.engedu.frontapi.client.TtsMindsLabClient;
import ai.mindslab.engedu.frontapi.client.TtsSelvasClient;
import ai.mindslab.engedu.frontapi.common.ResponseLogResolver;
import ai.mindslab.engedu.frontapi.common.SttWriteFile;
import ai.mindslab.engedu.frontapi.common.server.ServerManageFactory;
import ai.mindslab.engedu.frontapi.common.server.data.ServerVO;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.ServiceMessageVO;
import ai.mindslab.engedu.frontapi.controller.data.SttFileResponseVO;
import ai.mindslab.engedu.frontapi.service.data.Parameters;
import ai.mindslab.engedu.intent.IntentExecute;
import ai.mindslab.engedu.intent.IntentManager;
import ai.mindslab.engedu.intent.IntentServiceMsg;
import ai.mindslab.engedu.intent.IntentServiceType;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import ai.mindslab.engedu.intent.vo.IntentFinderVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.binding.ObjectExpression;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.TextMessage;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 백과사전 sample rest api
 */
@Slf4j
@RestController
@RequestMapping("/encyclopDic")
public class EncyclopDicController {

    @Autowired
    @Qualifier("mindsLabChatBotExecute")
    private IntentExecute mindsLabChatBotExecute;

    @Value("${client.record.domain}")
    private String domain;

    @Autowired
    private EnvironmentBase environmentBase;

    @Autowired
    private IntentManager intentManager;

    @Value("${brain.tts.dev.rec.dir}")
    private String ttsDevDir;

    @Value("${brain.dialog.stt.rec.dir}")
    private String sttRecDir;

    @Value("${brain.dialog.tts.rec.dir}")
    private String ttsRecDir;

    @Autowired
    private ServerManageFactory serverManageFactory;

    @Autowired
    private UserService userService;

    /***
     * @param paramInputStr 사용자 발화 문장  ex) 이순신이 누구야?
     * @return
     * @throws Exception
     */

    @RequestMapping("/text")
    @SuppressWarnings("unchecked")
    public String sample(
            @RequestParam(name = "inputStr") String paramInputStr,
            @RequestParam(name = "userId") String paramUserId) throws Exception {
//        HashMap<String, Object> paramInputStrMap = (HashMap) req.get("paramInputStr");
//        String paramInputStr = (String) paramInputStrMap.get("paramInputStr");
//
//        HashMap<String, Object> paramUserIdMap = (HashMap) req.get("paramUserId");
//        String paramUserId = (String) paramUserIdMap.get("paramInputStr");

        IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
        intentExecuteVO.setInputStr(paramInputStr);
        intentExecuteVO.setUserId(paramUserId);
        intentExecuteVO.setChatBotType("Noah");
        IntentExecuteMessageVO intentExecuteMessageVO = mindsLabChatBotExecute.execute(intentExecuteVO);

        log.debug("userId Value is !!!!!!!!!!!!!" + paramUserId);


        String utter = paramInputStr;

        Response response;
        Result result = new Result();
        String resultId = "";
        String errText = null;

        //현재 사용자가 접속한 서비스를 찾는다.
        IntentFinderVO intentFinderVO = new IntentFinderVO();
        intentFinderVO.setUserId(paramUserId);
        intentFinderVO.setInputStr(paramInputStr);
        intentFinderVO.setServiceType("ST0020");
        intentFinderVO.setFirstServiceEntry(false);


        intentManager.init(intentFinderVO);
        intentManager.getCurrentIntent();

        intentExecuteVO.setServiceType(intentFinderVO.getServiceType());

        DialogSttPostService dialogSttPostService = new DialogSttPostService();
        String message = dialogSttPostService.getSttPostMessage(
                intentExecuteVO.isFirstServiceEntry(),
                intentExecuteVO.getServiceType(),
                intentExecuteVO.getInputStr(),
                environmentBase.getActiveProfile()
        );

        log.info("sttPost before =============================================>" + utter);
        log.info("sttPost after =============================================>" + message);
        intentExecuteVO.setInputStr(message);
        utter = message;
//        sttFileResponseVO.setUtter(message);

//        IntentExecuteMessageVO intentExecuteMessageVO = intentManager.getIntent(intentExecuteVO);
//        if (intentExecuteMessageVO.getCode() != IRestCodes.ERR_CODE_SUCCESS) {
//            response = new Response(Integer.toString(intentExecuteMessageVO.getCode()), intentExecuteMessageVO.getMsg(), IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);
//            errText = intentExecuteMessageVO.getErrText();

//        } else {
        SttFileResponseVO sttFileResponseVO = new SttFileResponseVO();
        sttFileResponseVO.setUserId(paramUserId);
        sttFileResponseVO.setUtter(paramInputStr);
        sttFileResponseVO.setFilePath(sttRecDir);

        String waveFileName = writeTtsFile(paramUserId);

        ITtsClient ttsClient = new TtsSelvasClient(
                this.ttsDevDir,
                "ko_KR",
                intentExecuteMessageVO.getResultMsg(),
                intentExecuteVO.getServiceType(),
                ttsRecDir,
                waveFileName,
                environmentBase.getActiveProfile(),
                domain
        );

        String language = "ko_KR";

        ttsClient = setTtsServerInfo(ttsClient, language);

        Response ttsResponse = ttsClient.getTtsResponse();
        Result ttsResult = ttsResponse.getResult();

        this.closeChannel(ttsClient);


//            if (ttsResult == null || ttsResponse.getResCode().equals(String.valueOf(IRestCodes.ERR_CODE_TTS_EVALUATION_ERROR))) {
//                ttsResult.setTtsText(intentExecuteMessageVO.getResultMsg());
//                insertDialog(sttFileResponseVO, ttsResult);
//                response = new Response(Integer.toString(IRestCodes.ERR_CODE_TTS_EVALUATION_ERROR), IRestCodes.ERR_MSG_TTS_EVALUATION_ERROR, IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);
//                result.setUserText(utter);
//                result.setRecordUrl(sttFileResponseVO.getRecordUrl());
//                result.setTtsText(intentExecuteMessageVO.getResultMsg());
//                result.setExtInfo(intentExecuteMessageVO.getExtInfo());
//                //intentManager.forceQuit(intentExecuteVO); // ROLLBACK 처리해주는 함수
//
//            } else {
        response = new Response(Integer.toString(IRestCodes.ERR_CODE_SUCCESS), IRestCodes.ERR_MSG_SUCCESS, IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);

        // DB 저장
        insertDialog(sttFileResponseVO, ttsResult);

        result.setUserText(utter);
        result.setRecordUrl(sttFileResponseVO.getRecordUrl());
        result.setTtsUrl(ttsResult.getTtsUrl());
        result.setTtsText(intentExecuteMessageVO.getResultMsg());

        result.setExtInfo(intentExecuteMessageVO.getExtInfo());

        log.debug("ttsResult.getTtsUrl():" + ttsResult.getTtsUrl());

//            }

//        }
        log.info("intentExecuteMessageVO.toString()================>" + intentExecuteMessageVO.toString());

        ResponseLogResolver.insertResponseLog(response, paramUserId, errText);

        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(response);
        log.info("jsonString: {} ", jsonString);

//        session.sendMessage(new TextMessage(jsonString));
        // 도메인 종료 조건 END

//        return intentExecuteMessageVO;
        return jsonString;
    }

    private ITtsClient setTtsServerInfo(ITtsClient paramTtsClient, String paramLanguage) {

        ITtsClient result = paramTtsClient;
        ServerVO serverVO = serverManageFactory.getTtsServerInfo();

        if (serverVO != null) {

            log.info("setTtsServerInfo serverVO : " + serverVO);
            result.setServerInfo(serverVO.getServerIp(), serverVO.getServerPort());

        } else {

            log.info("setTtsServerInfo serverVO null");
            result = new TtsSelvasClient();

        }

        return result;

    }

    private void insertDialog(SttFileResponseVO sttFileResponseVO, Result ttsResult) {
        log.info("sttFileResponseVO.toString():::::" + sttFileResponseVO.toString());
        log.info("ttsResult.toString():::::" + ttsResult.toString());

        DialogVO dialogVO = new DialogVO();
        dialogVO.setUserId(sttFileResponseVO.getUserId());
        dialogVO.setFilePath(sttFileResponseVO.getFilePath());
        dialogVO.setSttFileTextName(sttFileResponseVO.getFileTextName());
        dialogVO.setSttFileWaveName(sttFileResponseVO.getFileWavName());
        dialogVO.setCreatorId(sttFileResponseVO.getUserId());


        dialogVO.setTtsFileMp3Name(ttsResult.getTtsUrl().substring(ttsResult.getTtsUrl().lastIndexOf("/") + 1).trim());

        dialogVO.setUserText(sttFileResponseVO.getUtter());

        dialogVO.setAnswerText(ttsResult.getTtsText());

        // dialogVO.setServiceType(serviceType);

        userService.insertUserDialog(dialogVO);
    }

    private String writeTtsFile(String paramUserid) {
        FileCreateUtil fileCreateUtil = new FileCreateUtil();

        String currentTime = new DateUtil().getCurrentTime(DateUtil.TO_SECOND);
        String userId;
        if( paramUserid != null) {
            userId = paramUserid;
        } else {
            userId = "notUse";
        }

        String randomNum = fileCreateUtil.getRandomNum();

        String fileWavName;
        fileWavName = fileCreateUtil.getFileName(userId, currentTime, randomNum, IExtensionCodes.EXT_WAV);
        log.debug(fileWavName);

        return  fileWavName;
    }

    private void closeChannel(ITtsClient paramTtsClient) {

        try{
            paramTtsClient.shutdown();
        }catch (Exception e){
            e.printStackTrace();
            log.error("closeChannel :{}" + e.getMessage());
        }

    }

}

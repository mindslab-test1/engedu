package ai.mindslab.engedu.frontapi.client;

import ai.mindslab.engedu.frontapi.controller.data.Response;

public interface ITtsClient {

    void setServerInfo(String paramServerIp,int paramServerPort);

    Response getTtsResponse();

    void shutdown() throws InterruptedException;

}

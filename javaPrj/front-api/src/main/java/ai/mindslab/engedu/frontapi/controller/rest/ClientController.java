package ai.mindslab.engedu.frontapi.controller.rest;

import ai.mindslab.engedu.common.base.EnvironmentBase;
import ai.mindslab.engedu.common.codes.IExtensionCodes;
import ai.mindslab.engedu.common.codes.ILanguageCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.utils.DateUtil;
import ai.mindslab.engedu.common.utils.FileCreateUtil;
import ai.mindslab.engedu.frontapi.client.ITtsClient;
import ai.mindslab.engedu.frontapi.client.TtsMindsLabClient;
import ai.mindslab.engedu.frontapi.client.TtsSelvasClient;
import ai.mindslab.engedu.frontapi.common.ResponseLogResolver;
import ai.mindslab.engedu.frontapi.controller.data.Response;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.ServiceMessageVO;
import ai.mindslab.engedu.intent.IntentManager;
import ai.mindslab.engedu.intent.dao.IntentFinderMapper;
import ai.mindslab.engedu.intent.dao.data.UserServiceVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import ai.mindslab.engedu.intent.vo.IntentFinderVO;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/client")
public class ClientController {

	@Value("${brain.dialog.tts.rec.dir}")
	private String filePath;

	@Value("${brain.tts.dev.rec.dir}")
	private String ttsDevDir;

	@Value("${brain.dialog.tts.rec.dir}")
	private String ttsRecDir;

	@Value("${brain.eng.tts.ip}")
	private String engTtsIp;

	@Value("${brain.eng.tts.port}")
	private String engTtsPort;

	@Value("${brain.kor.tts.ip}")
	private String korTtsIp;

	@Value("${brain.kor.tts.port}")
	private String korTtsPort;

	@Value("${client.record.domain}")
	private String domain;
	
	@Value("${engedu.dics.timeout}")
	private String dicsTimeout;

	@Autowired
	private IntentManager intentManager;


	@Autowired
	private EnvironmentBase environmentBase;
	
	@Autowired
	private IntentFinderMapper intentFinderMapper ;
	
	
	@RequestMapping(value="/currentServiceType", method= {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	public UserServiceVO currentServiceType(@RequestParam(name = "userId", required = true) String userId) {
		
		Map<String, String> serviceMap = new HashMap<>();
		serviceMap.put("userId", userId);
		serviceMap.put("timeOut", dicsTimeout);
		
		UserServiceVO userServiceVO = intentFinderMapper.getCurrentIntent(serviceMap);
		
		if (userServiceVO != null) {
			SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			userServiceVO.setCurrentTimeStr(dayTime.format(userServiceVO.getCurrentTime()));
			userServiceVO.setLimitTimeStr(dayTime.format(userServiceVO.getServiceLimitTime()));
			
			if (userServiceVO.getServiceType() == null) {
				userServiceVO.setServiceType("HOME");
			}
			
			log.debug("userServiceVO.toString():"+userServiceVO.toString());
			
		} else {
			userServiceVO = new UserServiceVO();
		}
		
		
		return userServiceVO;
	}


	@RequestMapping(value="/forceQuit", method= {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	public Response forceQuit(ServiceMessageVO serviceMessageVO) {

		log.info(new Object(){}.getClass().getEnclosingMethod().getName()+" serviceMessageVO.toString() >>>>"+serviceMessageVO.toString());

		Response response = null;
		Result result = new Result();
		String resultId = "";
		String errText = null;

		try {
			if (ObjectUtils.isEmpty(serviceMessageVO.getUserId())) {
				response = new Response(Integer.toString(IRestCodes.ERR_CODE_NOT_FOUND), "USER " + IRestCodes.ERR_MSG_NOT_FOUND, IRestCodes.RESULT_TYPE_CLIENT_API, resultId, result);

			} else if (ObjectUtils.isEmpty(serviceMessageVO.getExitType())) {
				response = new Response(Integer.toString(IRestCodes.ERR_CODE_PARAMS_INVALID), IRestCodes.ERR_MSG_PARAMS_INVALID, IRestCodes.RESULT_TYPE_CLIENT_API, resultId, result);

			} else {

				//현재 사용자가 접속한 서비스를 찾는다.
				IntentFinderVO intentFinderVO = new IntentFinderVO();
				intentFinderVO.setUserId(serviceMessageVO.getUserId());
				intentFinderVO.setInputStr(serviceMessageVO.getInputStr());
				intentFinderVO.setServiceType(serviceMessageVO.getServiceType());
				intentFinderVO.setFirstServiceEntry(false);


				intentManager.init(intentFinderVO);
				intentManager.getCurrentIntent();

				log.info("intentFinderVO.toString()  =============================================>" + intentFinderVO.toString());
				log.info("currentServiceType =============================================>" + intentFinderVO.getServiceType() + "<=============================================");


				IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
				intentExecuteVO.setUserId(serviceMessageVO.getUserId());
				intentExecuteVO.setInputStr(serviceMessageVO.getInputStr());
				intentExecuteVO.setServiceType(intentFinderVO.getServiceType());
				intentExecuteVO.setFirstServiceEntry(intentFinderVO.isFirstServiceEntry());
				intentExecuteVO.setExitType(serviceMessageVO.getExitType());

				IntentExecuteMessageVO intentExecuteMessageVO = intentManager.forceQuit(intentExecuteVO);
				if (intentExecuteMessageVO.getCode() != IRestCodes.ERR_CODE_SUCCESS) {
					response = new Response(Integer.toString(intentExecuteMessageVO.getCode()), intentExecuteMessageVO.getMsg(), IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);
					errText = intentExecuteMessageVO.getErrText();

				} else if (!StringUtils.isEmpty(intentExecuteMessageVO.getResultMsg())) {

					FileCreateUtil fileCreateUtil = new FileCreateUtil();
					String currentTime = new DateUtil().getCurrentTime(DateUtil.TO_SECOND);
					String randomNum = fileCreateUtil.getRandomNum();
					String fileMp3Name = fileCreateUtil.getFileName(intentExecuteVO.getUserId(), currentTime, randomNum, IExtensionCodes.EXT_MP3);
					String fileWavName = fileCreateUtil.getFileName(intentExecuteVO.getUserId(), currentTime, randomNum, IExtensionCodes.EXT_WAV);

					ITtsClient ttsClient = new TtsSelvasClient(
							this.ttsDevDir,
							ILanguageCodes.KOR,
							intentExecuteMessageVO.getResultMsg(),
							intentExecuteVO.getServiceType(),
							filePath,
							fileWavName,
							environmentBase.getActiveProfile(),
							domain
					);

					ttsClient = setTtsInitInfo(ttsClient, ILanguageCodes.KOR);

					Response ttsResponse = ttsClient.getTtsResponse();
					result = ttsResponse.getResult();

					this.closeChannel(ttsClient);

					log.info("intentExecuteMessageVO.toString()================>" + intentExecuteMessageVO.toString());
				}
			}

			if (response == null) {
				response = new Response(Integer.toString(IRestCodes.ERR_CODE_SUCCESS), IRestCodes.ERR_MSG_SUCCESS, IRestCodes.RESULT_TYPE_CLIENT_API, resultId, result);
			}
		} catch(Exception e){

			try{
				response = new Response(
						Integer.toString(IRestCodes.ERR_CODE_FAILURE)
						, IRestCodes.ERR_MSG_FAILURE
						, IRestCodes.RESULT_TYPE_CLIENT_API
						, null
						, null
				);
				errText = ExceptionUtils.getStackTrace(e);
			}catch (Exception e1){
				log.error("forceQuit.json Error : {} ", e1);
				e.printStackTrace();
			}

		} finally {
			ResponseLogResolver.insertResponseLog(response, serviceMessageVO.getUserId(), errText);
		}

		return response;
	}

	private void closeChannel(ITtsClient paramTtsClient) {

		try{
			paramTtsClient.shutdown();
		}catch (Exception e){
			e.printStackTrace();
			log.error("closeChannel :{}" + e.getMessage());
		}

	}

	private ITtsClient setTtsInitInfo(ITtsClient paramTtsClient, String paramLanguage)

	{

		switch (paramLanguage.toUpperCase()){
			case ILanguageCodes.KOR :
				paramTtsClient.setServerInfo(korTtsIp,Integer.valueOf(korTtsPort));
				break;
			case ILanguageCodes.ENG :
				paramTtsClient.setServerInfo(engTtsIp,Integer.valueOf(engTtsPort));
				break;
		}



		return paramTtsClient;

	}
}

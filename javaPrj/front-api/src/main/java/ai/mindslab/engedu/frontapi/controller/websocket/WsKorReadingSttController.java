package ai.mindslab.engedu.frontapi.controller.websocket;

import ai.mindslab.engedu.frontapi.service.KorReadingSttService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import java.util.Map;

@Slf4j
@Component
public class WsKorReadingSttController extends BinaryWebSocketHandler {

	@Autowired
	private KorReadingSttService korReadingSttService;

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		try {
			//super.afterConnectionEstablished(session);

			Map<String, Object> requestMap = session.getAttributes();
			log.info(requestMap.toString());
			korReadingSttService.openSession(session, requestMap);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
		korReadingSttService.onMessage(session, message.getPayload(), false);
		super.handleBinaryMessage(session, message);
	}


	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		try {
			log.debug("afterConnectionClosed {}", status.toString());
//			korReadingSttService.onClose(session);
			korReadingSttService.closeSttClient(session);
			super.afterConnectionClosed(session, status);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}

package ai.mindslab.engedu.frontapi.common;

import ai.mindslab.engedu.common.codes.IExtensionCodes;
import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.common.utils.DateUtil;
import ai.mindslab.engedu.common.utils.FileCreateUtil;
import ai.mindslab.engedu.frontapi.controller.data.SttFileResponseVO;
import ai.mindslab.engedu.frontapi.service.data.Parameters;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.File;

@Slf4j
public class SttWriteFile {

    public SttFileResponseVO write(
            ByteArrayOutputStream paramByteArrayOutputStream,
            String paramUtter,
            String paramRecDir,
            String paramDomain,
            Parameters paramParameters) throws  Exception
    {

        FileCreateUtil fileCreateUtil = new FileCreateUtil();

        // 현재 시각 년월일시분초
        String currentTime = new DateUtil().getCurrentTime(DateUtil.TO_SECOND);

        String fileFullPath = fileCreateUtil.makeFolder(paramRecDir,currentTime);

        String userId = paramParameters.getUserId();
        String answer = paramParameters.getAnswerText();

        log.debug("paramRecDir : " + paramRecDir);
        log.debug("userId : " + paramParameters.getUserId());

        String randomNum = fileCreateUtil.getRandomNum();

        //** 파일명 risingsuntae_20180808135901.txt / risingsuntae_20180808135901.wav *//*
        String fileTextName = fileCreateUtil.getFileName(userId,currentTime,randomNum,IExtensionCodes.EXT_TXT);
        String fileWavName = fileCreateUtil.getFileName(userId,currentTime,randomNum,IExtensionCodes.EXT_WAV);
        String filePcmName = fileCreateUtil.getFileName(userId,currentTime,randomNum,IExtensionCodes.EXT_PCM);
        String fileMp3Name = fileCreateUtil.getFileName(userId,currentTime,randomNum,IExtensionCodes.EXT_MP3);

        log.debug("fileTextName : " + fileTextName);
        log.debug("fileWavName : " + fileWavName);
        log.debug("filePcmName : " + filePcmName);
        log.debug("fileMp3Name : " + fileMp3Name);
        
        // for kaldi test save dir
        //fileFullPath="/record/kaldi";

        //** 폴더명 + 파일명 /record/stt/evaluation/2018/08/17/risingsuntae_20180808135901.txt *//*
        String fileFolderFileTextName = fileCreateUtil.getFolderFileName(fileTextName,fileFullPath);
        String fileFolderFileWavName = fileCreateUtil.getFolderFileName(fileWavName,fileFullPath);
        String fileFolderFilePcmName = fileCreateUtil.getFolderFileName(filePcmName,fileFullPath);
        String fileFolderFileMp3Name = fileCreateUtil.getFolderFileName(fileMp3Name,fileFullPath);

        log.debug("fileFolderFileTextName : " + fileFolderFileTextName);
        log.debug("fileFolderFileWavName : " + fileFolderFileWavName);
        log.debug("fileFolderFilePcmName : " + fileFolderFilePcmName);
        log.debug("fileFolderFileMp3Name : " + fileFolderFileMp3Name);

        createFile(fileCreateUtil,paramUtter,fileFolderFileTextName,paramByteArrayOutputStream,fileFolderFileWavName,fileFolderFileMp3Name,fileFolderFilePcmName);

        //** 파일 경로(도메인제외) 2018/08/08/ *//*
        String fileSubPath = fileCreateUtil.getFileSubPath(IMarkCodes.FORWARD_SLASH,currentTime);
        log.debug("fileSubPath : " + fileSubPath);

        String recordUrl =paramDomain  + paramRecDir.replaceFirst("/","")+fileSubPath + fileMp3Name;

        SttFileResponseVO sttFileResponseVO = getSttWriteResult(
                userId,
                paramUtter,
                answer,
                recordUrl,
                fileSubPath,
                fileTextName,
                fileWavName,
                fileMp3Name,
                fileFolderFileWavName,
                fileFolderFilePcmName
        );

        return sttFileResponseVO;

    }

    /**
     * 파일 생성 메서드
     * ex) {filename}.mp3 / {filename}.wav / {filename}.txt
     * @param paramFileCreateUtil
     * @param paramUtter
     * @param paramFileFolderFileTextName
     * @param paramByteArrayOutputStream
     * @param paramFileFolderFileWavName
     * @param paramFileFolderFileMp3Name
     */
    private void createFile(
            FileCreateUtil paramFileCreateUtil,
            String paramUtter,
            String paramFileFolderFileTextName,
            ByteArrayOutputStream paramByteArrayOutputStream,
            String paramFileFolderFileWavName,
            String paramFileFolderFileMp3Name,
            String paramFileFolderFilePcmName
            )
    {

        // txt 파일 생성
        paramFileCreateUtil.writeHandFile(paramUtter,paramFileFolderFileTextName);
        // wav 파일 생성
        paramFileCreateUtil.writeHandWav(paramByteArrayOutputStream,paramFileFolderFileWavName,16000);
        // pcm 파일 생성
        paramFileCreateUtil.writeHandPcm(paramByteArrayOutputStream,paramFileFolderFilePcmName,16000);
        /**
         * mp3 파일 생성
         * wav 파일 존재 할 경우에만 lame 으로 mp3 변환
         */
        File pcmFile = new File(paramFileFolderFilePcmName);
        if (pcmFile.exists()) {
            paramFileCreateUtil.writeHandPcmMp3(paramFileFolderFilePcmName,paramFileFolderFileMp3Name );
        }

    }

    /**
     * 파일 생성 결과 값 메서드
     * @param paramUserId 학습자 ID
     * @param paramUtter 발화문장
     * @param paramAnswer 정답문장
     * @param paramRecordUrl 저장 URL
     * @param paramFileSubPath 파일 경로 ( 도메인 제외)
     * @param paramFileTextName text 파일명
     * @param paramFileWavName wav 파일명
     * @param paramFileMp3Name mp3 파일명
     * @param paramFileFolderFileWavName 폴더+파일 wav 명
     * @param paramFileFolderFilePcmName 폴더 + 파일 pcm 명
     * @return
     */
    private SttFileResponseVO getSttWriteResult(
            String paramUserId,
            String paramUtter,
            String paramAnswer,
            String paramRecordUrl,
            String paramFileSubPath,
            String paramFileTextName,
            String paramFileWavName,
            String paramFileMp3Name,
            String paramFileFolderFileWavName,
            String paramFileFolderFilePcmName)
    {

        SttFileResponseVO sttFileResponseVO = new SttFileResponseVO();
        sttFileResponseVO.setUserId(paramUserId);
        sttFileResponseVO.setUtter(paramUtter);
        sttFileResponseVO.setAnswer(paramAnswer);
        sttFileResponseVO.setRecordUrl(paramRecordUrl);
        sttFileResponseVO.setFilePath(paramFileSubPath);
        sttFileResponseVO.setFileTextName(paramFileTextName);
        sttFileResponseVO.setFileWavName(paramFileWavName);
        sttFileResponseVO.setFileMp3Name(paramFileMp3Name);
        sttFileResponseVO.setFileFolderFileWavName(paramFileFolderFileWavName);
        sttFileResponseVO.setFileFolderFilePcmName(paramFileFolderFilePcmName);

        return sttFileResponseVO;
    }

}

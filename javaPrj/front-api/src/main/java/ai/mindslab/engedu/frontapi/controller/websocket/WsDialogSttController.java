package ai.mindslab.engedu.frontapi.controller.websocket;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import ai.mindslab.engedu.frontapi.service.DialogSttService;
import ai.mindslab.engedu.frontapi.service.SimpleSttService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class WsDialogSttController extends BinaryWebSocketHandler {

	@Autowired
	private DialogSttService dialogSttService;
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		try {
			//super.afterConnectionEstablished(session);

			Map<String, Object> requestMap = session.getAttributes();
			log.info(requestMap.toString());
			dialogSttService.openSession(session, requestMap);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
		dialogSttService.onMessage(session, message.getPayload(), false);
		super.handleBinaryMessage(session, message);
	}
	
	
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
    	try {
    	log.debug("afterConnectionClosed {}", status.toString());
//    	dialogSttService.onClose(session);
 		dialogSttService.closeSttClient(session);
    	super.afterConnectionClosed(session, status);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
}

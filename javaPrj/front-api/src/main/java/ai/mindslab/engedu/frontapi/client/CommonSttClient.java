package ai.mindslab.engedu.frontapi.client;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.SttPostUtil;
import ai.mindslab.engedu.frontapi.service.EvaluationSttService;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import com.google.protobuf.ByteString;

import ai.mindslab.engedu.frontapi.common.SttTimeoutJob;
import ai.mindslab.engedu.frontapi.service.WebSocketCallbackInterface;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import maum.brain.stt.SpeechToTextServiceGrpc;
import maum.brain.stt.SpeechToTextServiceGrpc.SpeechToTextServiceBlockingStub;
import maum.brain.stt.SpeechToTextServiceGrpc.SpeechToTextServiceStub;
import maum.brain.stt.Stt.Segment;
import maum.brain.stt.Stt.Speech;

@Slf4j
public class CommonSttClient {

	private ManagedChannel channel;
	private SpeechToTextServiceStub asyncStub;
	private SpeechToTextServiceBlockingStub blockingStub;
	private StreamObserver<Speech> requestObserver;
	private StreamObserver<Segment> responseObserver;

	private String resp = "";
	private WebSocketSession session;

	private ByteArrayOutputStream byteArryOutputStream ;

	private Map<String, String> clientParam;

	private WebSocketCallbackInterface webSocket;

	private SttTimeoutJob sttTimeoutJob;

	private int timeOut;

	private String userId;

	public CommonSttClient(String sttIp, int sttPort, String lang, String model, String sampleRate, WebSocketCallbackInterface webSocket, int timeOut) {
		this.channel = ManagedChannelBuilder.forAddress(sttIp, sttPort).usePlaintext().build();
		this.asyncStub = SpeechToTextServiceGrpc.newStub(channel);
		this.blockingStub = SpeechToTextServiceGrpc.newBlockingStub(channel);

		Metadata meta = new Metadata();
		Metadata.Key<String> key1 = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
		Metadata.Key<String> key2 = Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
		Metadata.Key<String> key3 = Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER);
		meta.put(key1, lang.toLowerCase());
		meta.put(key2, sampleRate);
		meta.put(key3, model);
		asyncStub = MetadataUtils.attachHeaders(asyncStub, meta);

		byteArryOutputStream = new ByteArrayOutputStream( );


		this.clientParam = new HashMap<>();
		this.clientParam.putAll(clientParam);

		this.webSocket = webSocket;

		this.timeOut = timeOut;

	}

	public void shutdown() throws InterruptedException {
//		channel.shutdown().awaitTermination(5,  TimeUnit.SECONDS);
		channel.shutdownNow();
	}
	public void init(WebSocketSession session, String userId) {
		init(session);
		this.userId = userId;

	}
	public void init(WebSocketSession session) {
		log.debug("init CommonSttClient Session: {}" , session.getId());

		sttTimeoutJob = new SttTimeoutJob(webSocket,session);
		Timer timer = new Timer(true);
//		timer.schedule(sttTimeoutJob, 10000);  // 주석을 막으면 작동 안함

		this.session = session;

		Map<String, String> resultMap = new HashMap<>();


		responseObserver = new StreamObserver<Segment>() {
			String resultUtter="";
			Boolean do_complete = false;

			@Override
			public void onCompleted() {
				timer.cancel();
				timer.purge();
				log.debug("CommonSttClient EVENT: onCompleted");
				resultMap.put("utter",resultUtter);
				log.debug("utter value!!!!!!!!!!!!!!!!!!!!!!!!!!!" +resultUtter);

				try {

					webSocket.onSttResult(session,resultMap, byteArryOutputStream);
					channel.shutdownNow();

				} catch (EngEduException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onError(Throwable arg0) {
				timer.cancel();
				timer.purge();
				log.debug("CommonSttClient EVENT: onError:{}", arg0.getMessage());
				//arg0.printStackTrace();e
				try {
					session.close(CloseStatus.SERVER_ERROR);
					channel.shutdownNow();

				} catch (IOException e) {
					e.printStackTrace();
					log.error("CommonSttClient EVENT: onError session.close {} : " + e.getMessage());
				}
			}

			@Override
			public void onNext(Segment segment) {
				timer.cancel();
				timer.purge();

				log.debug("CommonSttClient EVENT: onNext:{} {}", segment.getTxt(), segment.getEnd());

				try {
					if(!resultUtter.isEmpty()) {
						resultUtter += " ";

					}
					resultUtter += new SttPostUtil().getSttPostStr(segment.getTxt()).trim();
					log.debug("do_complete value is " + do_complete);
					log.debug("utterValue is " + resultUtter);
					if (!do_complete) {
						requestObserver.onCompleted();
						log.debug("onCompleted Request!!!!!!!");
						do_complete = true;
					}

				} catch (Exception e ) {
					e.printStackTrace();
				}
			}

		};
		requestObserver = asyncStub.streamRecognize(responseObserver);
	}



	public void sendData(byte[] buffer) {
		Speech speech = Speech.newBuilder().setBin(ByteString.copyFrom(buffer)).build();

		try {
			byteArryOutputStream.write(buffer);
		} catch (IOException e) {
			byteArryOutputStream.reset();
		}

		requestObserver.onNext(speech);
		String sessionInfo = this.session.getId();

		log.debug("CommonSttClient send , sessionId =" + sessionInfo + ",userId =" + userId);
//		doCutTest();
	}

	public void doCutTest() {
		try{
			String filePath = "/home/minds/pcmData/cutFilePcm/lee942_20190219124356_121.pcm";
			FileInputStream fileInputStream = null;
			fileInputStream = new FileInputStream(filePath);

			byte[] buf = new byte[1024];
			while (fileInputStream.read(buf, 0, buf.length) != -1) {
				maum.brain.stt.Stt.Speech speech = maum.brain.stt.Stt.Speech.newBuilder().setBin(ByteString.copyFrom(buf)).build();
				requestObserver.onNext(speech);
				log.debug("speech" + speech);
			}

		} catch (RuntimeException e) {
			System.out.println("on error");
			requestObserver.onError(e);
			throw e;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		requestObserver.onCompleted();


	}
}

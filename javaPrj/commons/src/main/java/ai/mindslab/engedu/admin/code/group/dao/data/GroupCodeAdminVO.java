package ai.mindslab.engedu.admin.code.group.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class GroupCodeAdminVO implements Serializable {

    private String groupCode;
    private String codeName;
    private String descript;
    private String useYn;
    private String createdTime;

}

package ai.mindslab.engedu.admin.qa.list.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class QaAnswerAdminVO implements Serializable {

    private int  questionId;
    private String answerId;
    private int answerIdNum;
    private String clientMenuCd;
    private String answer;
    private String UserId;

}

package ai.mindslab.engedu.dic.service;


import ai.mindslab.engedu.dic.dao.MathDicMapper;
import ai.mindslab.engedu.dic.dao.data.MathDicVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class MathDicService {
	
	@Autowired
	private MathDicMapper mathDicMapper;
	
	public int getCount() {
		//log.info(new Object(){}.getClass().getEnclosingMethod().getName()+">>>>"+mathDicMapper.getCount());
		return mathDicMapper.getCount();
	}

	public List<MathDicVO> getSearch(String searchKey) {

		Map<String, String> hashMap = new HashMap<>();
		hashMap.put("searchKey", searchKey);
		
		return mathDicMapper.getSearch(hashMap);
	}

}

package ai.mindslab.engedu.common.service;


import ai.mindslab.engedu.common.dao.SttConfigMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class SttConfigService {
	
	@Autowired
	private SttConfigMapper sttConfigMapper  ;

	public String getServiceType(String userId) {
		Map<String, String> hashMap = new HashMap<>();
		hashMap.put("userId", userId);

		return sttConfigMapper.getServiceType(hashMap);
	}

	public String getSpeedGameModel(String userId) {
		Map<String, String> hashMap = new HashMap<>();
		hashMap.put("userId", userId);

		return sttConfigMapper.getSpeedGameModel(hashMap);
	}

	public String getModel(String userId) {

		Map<String, String> hashMap = new HashMap<>();
		hashMap.put("userId", userId);
		
		return sttConfigMapper.getModel(hashMap);
	}

}

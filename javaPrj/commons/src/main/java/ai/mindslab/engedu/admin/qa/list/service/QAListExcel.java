package ai.mindslab.engedu.admin.qa.list.service;

import ai.mindslab.engedu.admin.qa.list.dao.data.QAListAdminVO;
import ai.mindslab.engedu.admin.servicement.dao.data.ServiceMentAdminVO;
import ai.mindslab.engedu.admin.servicement.service.GameMentService;
import ai.mindslab.engedu.common.component.CommonExcelComponent;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class QAListExcel {

    @Autowired
    CommonExcelComponent excelComponent;

    @Autowired
    QAListAdminService qaListAdminService;

    private Workbook workbook;
    private Map<String, Object> model;
    private HttpServletResponse response;
    private String title;


    public Workbook createExcel(Map<String, Object> paramMap) throws EngEduException {

        int[] columnWiths ={300, 6000, 12000,3000, 5000};
        excelComponent.init();
        excelComponent.setSheetPrintSetting();

        //컬럼 width 세팅
        excelComponent.setColumnWidth(columnWiths);
        //title.
        excelComponent.createTitle(this.title, 1,3);

        List<String> column = new ArrayList<>();

        String[] columnMap ={"질문", "응답","질문타입", "수정일"};

       List<QAListAdminVO> list = qaListAdminService.getQAQuestionAdminExcelList(paramMap);

        int rowCount = excelComponent.getRowCount();

        rowCount++;

        excelComponent.createColumnRow(rowCount, columnMap);

        long start = System.currentTimeMillis();


        for(QAListAdminVO vo : list){
            String answer = vo.getAnswer();

            if(answer !=null) {
                answer = answer.replaceAll("▤", "\n");
            }

            String[] data = {
                    vo.getQuestion(),
                    answer,
                    vo.getQuestionType(),
                    vo.getUpdateTime()
            };

            excelComponent.createDataRow(data);
        }

        this.workbook = excelComponent.getWorkBook();

        long end = System.currentTimeMillis();
        System.out.println("writeHSSFWorkbook : "+(end-start));

        return this.workbook;
    }


    public void setTitle(String title){
        this.title = title;
    }
}

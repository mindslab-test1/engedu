package ai.mindslab.engedu.admin.engtalk.dao.data.answer;

import lombok.Data;

import java.io.Serializable;

@Data
public class EngTalkAdminAnswerVO implements Serializable {

    private String brandId;
    private String bookId;
    private String chapterId;
    private String questionId;
    private String answerId;
    private String answerText;
    private String useYn;
}

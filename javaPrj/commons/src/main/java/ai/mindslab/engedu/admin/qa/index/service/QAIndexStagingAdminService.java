package ai.mindslab.engedu.admin.qa.index.service;


import ai.mindslab.engedu.admin.qa.list.dao.data.QAListAdminVO;
import ai.mindslab.engedu.admin.qa.list.service.QAListAdminService;
import ai.mindslab.engedu.bqa.dao.QaIndexMapper;
import ai.mindslab.engedu.bqa.dao.QaQnaParaphraseMapper;
import ai.mindslab.engedu.bqa.dao.data.QaIndexVo;
import ai.mindslab.engedu.bqa.dao.data.QaQnaParaphraseVo;
import ai.mindslab.engedu.bqa.dao.data.QaQuestionVo;
import ai.mindslab.engedu.bqa.nlp.NlpAnalyzeClient;
import ai.mindslab.engedu.bqa.service.BqaManagementService;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import maum.brain.nlp.Nlp;
import maum.common.LangOuterClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class QAIndexStagingAdminService {

    @Autowired
    private QAListAdminService qaListAdminService;

    @Autowired
    private NlpAnalyzeClient nlpAnalyzeClient;

    @Autowired
    private QaQnaParaphraseMapper qnaParaphrase;

    @Autowired
    private BqaManagementService bqaManagementService;

    @Autowired
    private QaIndexMapper indexMapper;

    /**
     * qa_index_tb 초기화 -> 모두 삭제 후, qa_question_tb 기준으로 다시 돌린다.
     * @param param 파라미터
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = { Exception.class })
    public String AllReset(Map<String, Object> param) throws Exception {

        String result = "success";

        indexMapper.deleteAll();

        List<QAListAdminVO> qaList = qaListAdminService.getQAQuestionList(param);

        this.qaIndexProcess(qaList);
        return result;

    }

    /**
     * qa_index_tb 특정 question_id로 초기화 -> 해당 id 삭제 후, 다시 돌린다.
     * @param paramQuestionId QUESTION ID
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = { Exception.class })
    public String OneReset(int paramQuestionId) throws Exception
    {

        String result = "success";

        indexMapper.deleteOneQuestionIndex(paramQuestionId);
        Map<String, Object> param = new HashMap<>();
        param.put("questionId",paramQuestionId);

        List<QAListAdminVO> qaList = qaListAdminService.getQAQuestionList(param);
        this.qaIndexProcess(qaList);
        return result;

    }

    private void qaIndexProcess(List<QAListAdminVO> qaList) {

        /*
        Map<String, Object> updateParamMap = new HashMap<>();
        indexMapper.updateAutoIncrement(updateParamMap);
        */

        List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
        List<QaIndexVo> qaQnaIndexList = new ArrayList<>();

        for(int i=0;i<qaList.size();i++){

            QaQuestionVo q = new QaQuestionVo();
            q.setQuestionId(qaList.get(i).getQuestionId());
            q.setDomainId(qaList.get(i).getDomainId());
            q.setQuestion(qaList.get(i).getQuestion());
            q.setUseYn("Y");
            q.setMainYn("Y");
            qaQnaIndexList.addAll(bqaManagementService.addParaphrase(q, paraphraseList));

        }

        StreamObserver<Nlp.Document> responseObserver = new StreamObserver<Nlp.Document>() {

            int index = 0;

            @Override
            public void onNext(Nlp.Document document) {

                Map<String,Object> map = nlpAnalyzeClient.modify(document.getSentencesList());

                String questionMorph = map.get("questionMorph").toString();
                int morphCount = (Integer)map.get("morphCount");
                qaQnaIndexList.get(index).setMorphCount(morphCount);
                qaQnaIndexList.get(index).setQuestionMorph(questionMorph); // 분석 결과 삽입
                log.info("question {}, questionMorph , {}", qaQnaIndexList.get(index).getQuestion(),
                        questionMorph);
                index++;
            }

            @Override
            public void onError(Throwable t) {

                log.error("analyzeMultiple Failed: {}", Status.fromThrowable(t));
                nlpAnalyzeClient.countDownLatch();
            }

            @Override
            public void onCompleted() {

                nlpAnalyzeClient.countDownLatch();
                int result = insertIndexList(qaQnaIndexList);
                log.info("insertIndexList result count: {}", result);
            }
        };

        requestAnalyze(responseObserver, qaQnaIndexList);

    }


    private void requestAnalyze(
            StreamObserver<Nlp.Document> responseObserver,
            List<QaIndexVo> qaIndexList)
    {

        StreamObserver<Nlp.InputText> requestObserver = nlpAnalyzeClient.analyzeMultiple(responseObserver);

        for (QaIndexVo qaIndexVo : qaIndexList) {

            String question = qaIndexVo.getQuestion();
            Nlp.InputText inputText = Nlp.InputText.newBuilder().setText(question).setLang(LangOuterClass.LangCode.kor).setSplitSentence(true)
                    .setUseTokenizer(true).setLevel(Nlp.NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
                    .setKeywordFrequencyLevel(Nlp.KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();

            requestObserver.onNext(inputText);
        }
        requestObserver.onCompleted();
    }

    // index db insert 요청
    private int insertIndexList(List<QaIndexVo> qaIndexList) {

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("list", qaIndexList);

        return indexMapper.insertList(paramMap);
    }



}

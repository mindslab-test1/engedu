package ai.mindslab.engedu.game.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class SpeedGameVO implements Serializable {
	private Integer speedGameId;
	private String word;
}

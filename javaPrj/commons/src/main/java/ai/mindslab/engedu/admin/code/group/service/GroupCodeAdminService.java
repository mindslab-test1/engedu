package ai.mindslab.engedu.admin.code.group.service;

import ai.mindslab.engedu.admin.code.detail.dao.data.DetailCodeAdminVO;
import ai.mindslab.engedu.admin.code.group.dao.GroupCodeAdminMapper;
import ai.mindslab.engedu.admin.code.group.dao.data.GroupCodeAdminVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class GroupCodeAdminService {

    @Autowired
    GroupCodeAdminMapper mapper;

    public int getGroupCodeAdminCount(Map<String, Object> paramMap) throws EngEduException{
         return mapper.getGroupCodeAdminCount(paramMap);
    }

    public List<GroupCodeAdminVO> getGroupCodeAdminList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getGroupCodeAdminList(paramMap);
    }

     public GroupCodeAdminVO getGroupCodeAdmin(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getGroupCodeAdmin(paramMap);
    }


     public BaseResponse<Object> updateGroupCode(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.updateGroupCode(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }




     public BaseResponse<Object> addGroupCode(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.addGroupCode(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }


      public BaseResponse<Object> deleteGroupCode(Map<String, Object> paramMap) throws EngEduException {

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {

            String unique = paramMap.get("unique").toString();

            GroupCodeAdminVO[] keys = new Gson().fromJson(unique, GroupCodeAdminVO[].class);

            int result = 0;

            for(GroupCodeAdminVO svo : keys){
                paramMap.put("groupCode", svo.getGroupCode());

                result = mapper.deleteGroupCode(paramMap);

                if (result > 0) {
                    resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                    resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
                }else{
                    resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                    resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
                    throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
                }
            }

        } catch (Exception e) {
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }

}

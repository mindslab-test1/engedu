package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

@Slf4j
@Component
public class CalculateIntentExecute implements IntentExecute {
	
	@Autowired
	@Qualifier("intentMsgService")
	private IntentMsgService intentMsgService;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();
		String resultMsg ="";
		try {
			String inputStr = intentExecuteVO.getInputStr();
			String[] operandStrArr 	= {" + ", " - ", " * ", " / "};
			String[] keyStrArr 		= {"더하기", "빼기", "곱하기", "나누기"};

			// 연산자 찾기
			String operand = " ";
			for(int i=0; i < keyStrArr.length; i++) {
				if(inputStr.contains(keyStrArr[i])) {
					operand = operandStrArr[i];
					break;
				}
			}
			
			log.info("CalculateIntentExecute operand["+operand.trim()+"]");
			
			if (!"".equals(operand.trim())) {
				
				// 수에 문자붙을 경우 숫자만 추출
				String expression;
				String[] strArr = inputStr.split(" ");
				StringBuilder expressionBuilder = new StringBuilder();
				for(String s : strArr) {
					s = s.replaceAll("[^0-9]", "");
					if(!StringUtils.isEmpty(s)) {
						if(expressionBuilder.toString().contains(operand)) {
							expressionBuilder.append(s);
							break;
						} else {
							expressionBuilder.append(s).append(operand);
						}
					}
				}
				expression = expressionBuilder.toString();

				boolean isDivision = false;
				if(StringUtils.equals(operand, operandStrArr[3])) {
					isDivision = true;
				}

				String result;
				String remainder = null;
				
				try {
					ScriptEngineManager mgr = new ScriptEngineManager();
					ScriptEngine engine = mgr.getEngineByName("JavaScript");

					if(isDivision) {
						remainder = engine.eval(expression.replace("/", "%")).toString();
						expression = expression + " >> 0";
					}

					result = engine.eval(expression).toString();
					
					// 기본 계산기 정답
					resultMsg = "정답은 " + result;

					// 나눗셈 정답
					if(isDivision) {
						resultMsg = "몫은 " + result + " 나머지는 " + remainder;
					}

					// 0으로 나누는 나눗셈 결과
					if(StringUtils.equals(result, "Infinity") || StringUtils.equals(remainder, "NaN")) {
						resultMsg = "0 으로는 나눌수 없어.";
					}
					
					
				} catch (ScriptException e) {
//					e.printStackTrace();
					//throw new EngEduException(IRestCodes.ERR_CODE_SCRIPT_EXPRESSION_ERROR, IRestCodes.ERR_MSG_SCRIPT_EXPRESSION_ERROR);
					resultMsg = intentMsgService.getServiceMsg(IntentServiceType.CALCULATE, IntentServiceMsg.CALCULATE_DO_NOT_CAL);
				}
				
			} else {
				resultMsg = intentMsgService.getServiceMsg(IntentServiceType.CALCULATE, IntentServiceMsg.CALCULATE_DO_NOT_CAL);
			}

			log.info("CalculateIntentExecute ResultMsg:"+resultMsg);
			intentExecuteMessageVO.setResultMsg(resultMsg);

		//} catch (EngEduException e) {
		//	throw new EngEduException(e.getErrCode(), e.getErrMsg());
		} catch (Exception e) {
			resultMsg = intentMsgService.getServiceMsg(IntentServiceType.CALCULATE, IntentServiceMsg.CALCULATE_DO_NOT_CAL);
			intentExecuteMessageVO.setResultMsg(resultMsg);
			//throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	@Override
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {

		return null;
	}
}
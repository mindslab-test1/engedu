package ai.mindslab.engedu.admin.dialog.dao;

import ai.mindslab.engedu.admin.dialog.dao.data.DialogAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface DialogAdminMapper {

    int dialogAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<DialogAdminVO> dialogAdminList(Map<String, Object> paramMap) throws EngEduException;
    List<DialogAdminVO> dialogAdminExcelList(Map<String, Object> paramMap) throws EngEduException;
    DialogAdminVO dialogAdmin(Map<String, Object> paramMap) throws EngEduException;
}

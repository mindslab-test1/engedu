package ai.mindslab.engedu.admin.code.detail.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class DetailCodeAdminVO implements Serializable {

    private String fullCode;
    private String groupCode;
    private String detailCode;
    private String detailName;
    private String descript;
    private String useYn;
    private String createdTime;

}

package ai.mindslab.engedu.admin.eval.korean.service;

import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalAdminVO;
import ai.mindslab.engedu.admin.eval.korean.dao.KoreanEvalAdminMapper;
import ai.mindslab.engedu.admin.eval.korean.dao.data.KoreanEvalAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class KoreanEvalAdminService {

    @Autowired
    KoreanEvalAdminMapper mapper;

      public int korEvalAdminCount(Map<String, Object> paramMap) throws EngEduException {
        return mapper.korEvalAdminCount(paramMap);
    }


    public List<KoreanEvalAdminVO> korEvalAdminList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.korEvalAdminList(paramMap);
    }

    public List<KoreanEvalAdminVO> korEvalAdminExcelList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.korEvalAdminExcelList(paramMap);
    }

    public KoreanEvalAdminVO korEvalAdmin(Map<String, Object> paramMap) throws EngEduException{
        return mapper.korEvalAdmin(paramMap);
    }

}

package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.game.dao.data.SpeedGameUsedVO;
import ai.mindslab.engedu.game.dao.data.SpeedGameVO;
import ai.mindslab.engedu.game.service.SpeedGameService;
import ai.mindslab.engedu.intent.service.IntentFinderService;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

@Slf4j
@Component
public class SpeedGameIntentExecute implements IntentExecute {

	@Autowired
	private SpeedGameService speedGameService;

	@Autowired
	private IntentMsgService intentMsgService;

	@Autowired
	private IntentFinderService intentFinderService;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();

		try {
			String userId 	= intentExecuteVO.getUserId();
			String inputStr = intentExecuteVO.getInputStr();
			boolean isFirstServiceEntry = intentExecuteVO.isFirstServiceEntry();

			int level 		= this.getUserLevel(userId); // client api 호출 
			
			if (level == -1) {
				return clientError(userId);
			}
			
			
			int[] winStartNum 	= {7, 11, 16, 20, 26};
			int[] winEndNum 	= {10, 15, 20, 25, 30};

			Integer seq;
			Integer speedGameId;
			ArrayList<String> wordList;
			int wordSize;
			int index 			= 0;

			String startMsg = null;
			ExtInfo extInfo = new ExtInfo();
			if(isFirstServiceEntry) {
				seq = 0;
        		speedGameId = speedGameService.getRandomSpeedGameId();
				SpeedGameVO speedGameVO = speedGameService.getGameWord(speedGameId);
				wordList = new ArrayList<>(Arrays.asList(speedGameVO.getWord().split("")));
				wordSize = wordList.size();

				startMsg = intentMsgService.getServiceMsg(IntentServiceType.SPEEDGAME, IntentServiceMsg.SPEEDGAME_START);
				startMsg = startMsg.replace("_speedGameWord_", speedGameVO.getWord());
				if(startMsg.contains("_name_")) {
					String name = this.getUserName(userId);
					
					if ("CLIENT_ERROR".equals(name)) {
						return clientError(userId);
					}
					
					startMsg = startMsg.replace("_name_", name);
					index = wordSize - 1;
				} else {
					startMsg += " "+wordList.get(index)+"!";
				}

				extInfo.setPrintMeans( intentMsgService.getServiceMsg(IntentServiceType.SPEEDGAME, IntentServiceMsg.SPEEDGAME_START_PRINTMENT) );
				
				
			} else {
				// 이전 음절 체크
				seq = speedGameService.getMaxSeq(userId);
				SpeedGameUsedVO previousAiThumbVO = speedGameService.getUsedThumb(userId, seq);
				speedGameId = previousAiThumbVO.getSpeedGameId();
				SpeedGameVO speedGameVO = speedGameService.getGameWord(speedGameId);
				wordList = new ArrayList<>(Arrays.asList(speedGameVO.getWord().split("")));
				wordSize = wordList.size();

				int previousIndex = previousAiThumbVO.getThumbIndex();

				int currentIndex = (previousIndex + 1) % wordSize;
				if (!StringUtils.equals(wordList.get(currentIndex), inputStr)) {
					// 종료 프로세스 - 올바르지 않은 단어를 말한 경우
					return gameEndProcess(IntentServiceMsg.SPEEDGAME_WRONGWORD, wordList.get((previousIndex + 1) % wordSize), userId);
				}

				// 15회 이상시에만 체크
				if(seq >= winStartNum[level-1]) {
					// 30회 이상시에는 반드시 또는 15~30일시에는 절반으로 랜덤
					if(Math.random() < 0.5 || seq >= winEndNum[level-1]) {
						// 종료 프로세스 - 사용자 승리
						return gameEndProcess(IntentServiceMsg.SPEEDGAME_USERWIN, null, userId);
					}
				}

				// 사용자 음절 저장
				speedGameService.insertUsedThumb(userId, speedGameId, wordList.get(currentIndex), currentIndex, "U", ++seq);
				index = (currentIndex + 1) % wordSize;
			}

			extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0019.getDomainName());
			intentExecuteMessageVO.setExtInfo(extInfo);

			// AI 음절 저장
			speedGameService.insertUsedThumb(userId, speedGameId, wordList.get(index), index, "A", ++seq);

			String resultMsg = wordList.get(index)+"!";
			if(isFirstServiceEntry) {	// 게임 처음 시작시
				resultMsg = startMsg;
			}

			log.info("SpeedGameIntentExecute ResultMsg:"+resultMsg);
			intentExecuteMessageVO.setResultMsg(resultMsg);

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) throws EngEduException {

		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();
		try {
			String exitType = intentExecuteVO.getExitType();
			if(!StringUtils.isEmpty(exitType) && StringUtils.equalsIgnoreCase(exitType, "T")) {
				intentExecuteMessageVO = new IntentExecuteMessageVO();

				String resultMsg = intentMsgService.getServiceMsg(IntentServiceType.SPEEDGAME, IntentServiceMsg.SPEEDGAME_TIMELIMIT);
				intentExecuteMessageVO.setResultMsg(resultMsg);
			}

			// 사용 세션 단어들 삭제
			speedGameService.deleteUsedThumbs(intentExecuteVO.getUserId());

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	// 종료 프로세스
	private IntentExecuteMessageVO gameEndProcess(String type, String thumb, String userId) {

		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();
		String resultMsg = intentMsgService.getServiceMsg(IntentServiceType.SPEEDGAME, type);

		switch(type)
		{
			case IntentServiceMsg.SPEEDGAME_WRONGWORD:
				String name = this.getUserName(userId);
				resultMsg = resultMsg.replace("_name_", name);
				resultMsg = resultMsg.replace("_thumb_", thumb);
				break;
		}

		// 사용 세션 단어들 삭제
		speedGameService.deleteUsedThumbs(userId);

		// 게임 종료시 서비스 타입 null처리
		intentFinderService.purgeServiceType(userId);

		log.info("SpeedGameIntentExecute ResultMsg:"+resultMsg);
		ExtInfo extInfo = new ExtInfo();
		extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0019.getDomainName());
		intentExecuteMessageVO.setResultMsg(resultMsg);
		intentExecuteMessageVO.setExtInfo(extInfo);

		return intentExecuteMessageVO;
	}
	
	private IntentExecuteMessageVO clientError(String userId) {
		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();
		
		// 사용 세션 단어들 삭제
		speedGameService.deleteUsedThumbs(userId);

		// 게임 종료시 서비스 타입 null처리
		intentFinderService.purgeServiceType(userId);
		
		String resultMsg = intentMsgService.getServiceMsg(IntentServiceType.BASICTALK,IntentServiceMsg.BQA_CLIENT_ERROR);
		log.info("SpeedGameIntentExecute clientError ResultMsg:"+resultMsg);
		intentExecuteMessageVO.setResultMsg(resultMsg);

		return intentExecuteMessageVO;
	}

	private String getUserName(String userId) {
		return StringUtils.defaultString(getClientData("name", userId), "CLIENT_ERROR"); 
	}

	private int getUserLevel(String userId) {
		
		int level;
		
		try {
			int age = Integer.parseInt(  this.getClientData("age", userId));
			if(age >= 13) {
				level = 5;
			} else if(age >= 12) {
				level = 4;
			} else if(age >= 10) {
				level = 3;
			} else if(age >= 8) {
				level = 2;
			} else {
				level = 1;
			}
		} catch (Exception e) {
			level = -1;
		}
		
		return level;
	}

	// Client API 연동 함수
	private String getClientData(String type, String userId) {

		String data = "";

		String uri = "https://stu.ideepstudy.com/appv2/api/member";
		if(type.equalsIgnoreCase("name")) {
			uri += "/name.do";
		} else if(type.equalsIgnoreCase("age")) {
			uri += "/age.do";
		}

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("charset", "UTF-8");

			MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<>();
			paramMap.add("stuId", userId);

			HttpEntity<MultiValueMap<String, String>> httpEntityRequest = new HttpEntity<>(paramMap, headers);

			ResponseEntity<Map> mapResponse = restTemplate.exchange(URI.create(uri), HttpMethod.POST, httpEntityRequest, Map.class);

			log.info("getClientData:"+uri);
			log.info("getClientData:"+mapResponse.getStatusCode());
			log.info("getClientData:"+mapResponse.getBody());
			String clientResult = (String) mapResponse.getBody().get("result");
			Object obj = mapResponse.getBody().get("data");

			if (HttpStatus.OK == mapResponse.getStatusCode() && "success".equals(clientResult)) {
				data = (String) obj;
			}
		} catch (Exception e){
			log.warn("SpeedGameIntentExecute ClientApiException", e);
			data="CLIENT_ERROR";
		}
		return data;
	}
}

package ai.mindslab.engedu.admin.speedgame.dao.data;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


@Data
@EqualsAndHashCode(callSuper = true)
public class SpeedGameAdminSVO extends SpeedGameAdminVO implements Serializable {

	private String updatorId;
	private String creatorId;

	public void setData(SpeedGameAdminVO vo){

		this.setSpeedId(vo.getSpeedId());
		this.setWord(vo.getWord());
		this.setUseYn(vo.getUseYn());

	}

}

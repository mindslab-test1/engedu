package ai.mindslab.engedu.common.dao;

import ai.mindslab.engedu.common.data.DialogVO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DialogMapper {

    int insertUserDialog(DialogVO dialogVO);
    
}

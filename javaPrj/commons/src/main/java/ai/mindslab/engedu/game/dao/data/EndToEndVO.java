package ai.mindslab.engedu.game.dao.data;

import java.io.Serializable;

import lombok.Data;

@Data
public class EndToEndVO implements Serializable {
	private String endId;
	private String word;
	private String thumb1;
	private String thumb2;
	private String level;

}

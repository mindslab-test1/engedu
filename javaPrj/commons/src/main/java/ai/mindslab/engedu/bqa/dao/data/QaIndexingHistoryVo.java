package ai.mindslab.engedu.bqa.dao.data;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({ "INDEX_STATUS_SUCCESS","INDEX_STATUS_FAIL","INDEX_STATUS_ERROR","ALL_TARGET_DOMAIN","INDEX_TYPE_FULL","INDEX_TYPE_ADD","INDEX_TYPE_REMOVE","INDEX_TYPE_DOMAIN" })
@Data
public class QaIndexingHistoryVo implements Serializable {

	public final char INDEX_STATUS_SUCCESS = 'S';
	public final char INDEX_STATUS_FAIL = 'F'; // data wrong
	public final char INDEX_STATUS_ERROR = 'E'; // system error

	public final int ALL_TARGET_DOMAIN = 0;

	public final char INDEX_TYPE_FULL = 'F';
	public final char INDEX_TYPE_ADD = 'A';
	public final char INDEX_TYPE_DOMAIN = 'D';
	public final char INDEX_TYPE_REMOVE = 'R';

	private int historyId;
	private char type;
	private int domainId;
	private long total;
	private String msg;
	private char status;
	private LocalDateTime createdDtm;
	private String creatorId;
	private LocalDateTime committedDtm;
	private String takenTime;
	private LocalDateTime indexingStartedDtm;

}

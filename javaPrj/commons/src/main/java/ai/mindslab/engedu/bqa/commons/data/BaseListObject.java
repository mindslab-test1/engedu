package ai.mindslab.engedu.bqa.commons.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@SuppressWarnings("serial")
@NoArgsConstructor
@Data
public class BaseListObject<T> implements Serializable{

	protected long totalCount = 0;
	protected List<T> list;

	public BaseListObject(int tot, List<T> list) {
		this.totalCount = tot;
		this.list = list;
	}

	public List<T> getList() {
		if(list == null) {
			list = new ArrayList<>();
		}
		return list;
	}

	@Override
	public String toString() {
		return "BaseListObject [totalCount=" + totalCount + ", list=" + list + "]";
	}
}

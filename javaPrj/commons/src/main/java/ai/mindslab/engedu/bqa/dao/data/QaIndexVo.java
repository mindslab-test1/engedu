package ai.mindslab.engedu.bqa.dao.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@SuppressWarnings("serial")
@JsonIgnoreProperties({"createdDtm","updatedDtm","useYn"})
@Data
@NoArgsConstructor
public class QaIndexVo implements Serializable {

	public static final int SEARCH_CODE_SUCCESS = 1;
	public static final int SEARCH_CODE_FAIL = 0;
	
	private int indexId;
 	private int questionId;
	private int questionNo;
	private String question;
	private String questionMorph;
	private String questionType;
	private int morphCount;
	private int domainId;
	private LocalDateTime createdDtm;
	private LocalDateTime updatedDtm;
	private String useYn;
	private int res;
	private Float score;
	private String mainYn;
	private List<QaAnswerVo> answerList;

	public QaIndexVo(QaQuestionVo qna, int no, String mainYn){
		
		this.setQuestionId(qna.getQuestionId());
		this.setQuestionNo(no);
		this.setQuestion(qna.getQuestion());
		this.setQuestionType(qna.getQuestionType());
		this.setDomainId(qna.getDomainId());
		this.setUseYn(qna.getUseYn());
		this.setMainYn(mainYn);
	}
}


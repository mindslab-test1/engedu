package ai.mindslab.engedu.intent;

import java.util.List;

import ai.mindslab.engedu.common.codes.IRestCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.WordExtractUtil;
import ai.mindslab.engedu.dic.dao.data.MathDicVO;
import ai.mindslab.engedu.dic.service.MathDicService;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MathDicIntentExecute implements IntentExecute {
	
	@Autowired
	private MathDicService mathDicService;
	
	@Autowired
	private IntentMsgService intentMsgService;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO executeMessageVO = new IntentExecuteMessageVO();

		try {
			WordExtractUtil wordExtractUtil = new WordExtractUtil();
			intentExecuteVO.setInputStr(wordExtractUtil.extractWord(intentExecuteVO.getServiceType(), intentExecuteVO.getInputStr()));

			List<MathDicVO> list = mathDicService.getSearch(intentExecuteVO.getInputStr());


			StringBuilder resultMsgBuilder = new StringBuilder();
			StringBuilder urlBuilder = new StringBuilder();
			StringBuilder printMeansBuilder = new StringBuilder();
			
			for (MathDicVO mathDicVO : list) {

				log.info("mathDicVO.toString():" + mathDicVO.toString());

				resultMsgBuilder.append(mathDicVO.getMeans());
				urlBuilder.append(mathDicVO.getUrl());
				printMeansBuilder.append(mathDicVO.getPrintMeans());
			}
			
			
			String resultMsg = resultMsgBuilder.toString();
			String url=  urlBuilder.toString();
			String printMeans=  printMeansBuilder.toString();
			
			/*if (list.size()==1) {
				resultMsg = resultMsg.replaceAll("[,]*", "");
				url = url.replaceAll("[,]*", "");
				printMeans = printMeans.replaceAll("[,]*", "");
			}*/

			ExtInfo extInfo = new ExtInfo();
			extInfo.setUrl(url);
			extInfo.setPrintMeans(printMeans);


			// 결과가 없으면
			if ("".equals(resultMsg)) {
				resultMsg = intentMsgService.getServiceMsg(IntentServiceType.MATHDIC, IntentServiceMsg.MATHDIC_NOT_FOUND);
			} else {
				resultMsg = resultMsg.substring(0, resultMsg.length() - 1);

				String serviceMsg = intentMsgService.getServiceMsg(IntentServiceType.MATHDIC, IntentServiceMsg.MATHDIC_MAKE_MENT);

				resultMsg = serviceMsg.replaceAll("_question_", intentExecuteVO.getInputStr()).replaceAll("_answer_", resultMsg);
			}

			log.info("MathDicIntentExecute resultMsg:" + resultMsg);

			executeMessageVO.setData(list);
			executeMessageVO.setResultMsg(resultMsg);
			executeMessageVO.setExtInfo(extInfo);

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}
		
		return executeMessageVO;
	}

	@Override
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {
		return null;
	}

}

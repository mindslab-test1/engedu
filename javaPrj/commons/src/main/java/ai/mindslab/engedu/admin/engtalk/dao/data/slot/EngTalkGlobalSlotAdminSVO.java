package ai.mindslab.engedu.admin.engtalk.dao.data.slot;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
public class EngTalkGlobalSlotAdminSVO extends EngTalkGlobalSlotAdminVO implements Serializable {
    private String creatorId;
    private String updatorId;


    public void setData(EngTalkGlobalSlotAdminVO vo){
        this.setOldSlotKey(vo.getOldSlotKey());
        this.setSlotKey(vo.getSlotKey());
    }
}

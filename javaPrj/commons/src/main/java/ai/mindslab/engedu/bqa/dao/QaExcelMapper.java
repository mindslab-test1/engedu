package ai.mindslab.engedu.bqa.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.bqa.dao.data.QaExcelVo;

@Mapper
public interface QaExcelMapper {

	List<QaExcelVo> getsExcel(Map<String, Object> paramMap);
}

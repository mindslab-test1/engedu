package ai.mindslab.engedu.admin.engtalk.dao;

import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkGlobalSlotAdminSVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkGlobalSlotAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface EngFreeTalkGlobalSlotAdminMapper {

    int engTalkGlobalSlotAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<EngTalkGlobalSlotAdminVO> engTalkGlobalSlotAdminList(Map<String, Object> paramMap) throws EngEduException;
    List<EngTalkGlobalSlotAdminVO> engTalkGlobalSlotExcelList(Map<String, Object> paramMap) throws EngEduException;
    EngTalkGlobalSlotAdminVO engTalkGlobalSlotAdmin(Map<String, Object> paramMap) throws EngEduException;
    int deleteGlobalSlotAdmin(EngTalkGlobalSlotAdminVO vo) throws EngEduException;
    int insertGlobalSlotAdmin(EngTalkGlobalSlotAdminSVO svo) throws EngEduException;

}

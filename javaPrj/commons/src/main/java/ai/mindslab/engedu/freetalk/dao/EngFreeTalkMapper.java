package ai.mindslab.engedu.freetalk.dao;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkQuestionVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface EngFreeTalkMapper {

    EngFreeTalkQuestionVO getQuestion(Map<String, Object> hashMap);

    List<EngFreeTalkQuestionVO> getQuestionList(Map<String, Object> hashMap);

    List<String> getSlotKeyList(Map<String, Object> hashMap);

    int insertFromSelectIntoTempDB(Map<String, Object> hashMap);
}

package ai.mindslab.engedu.admin.user.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserRoleVO implements Serializable {

    String roleId;
    String roleName;
    String descript;
    String order;

}

package ai.mindslab.engedu.dic.service;


import ai.mindslab.engedu.bqa.dao.data.QaAnswerVo;
import ai.mindslab.engedu.bqa.dao.data.QaIndexVo;
import ai.mindslab.engedu.dic.dao.UnitConversionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UnitConversionService {
	
	@Autowired
	private UnitConversionMapper unitConversionMapper;
	
	public int getCount() {
		return unitConversionMapper.getCount();
	}

	public QaIndexVo searchKeyword(String question,String question_type) {

		Map<String, String> hashMap = new HashMap<>();
		hashMap.put("question", question);
		hashMap.put("question_type", question_type);
		
		return unitConversionMapper.searchKeyword(hashMap);
	}
	
	public QaAnswerVo getAnswer(Map<String, String> hashMap) {
		return unitConversionMapper.getAnswer(hashMap);
	}

}

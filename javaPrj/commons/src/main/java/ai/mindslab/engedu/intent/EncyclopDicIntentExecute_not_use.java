package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.dic.service.EncyclopDicService_not_use;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Component
/**
 * Created by risingsuntae 2018.09.04
 * 백과사전 KBQA
 * 노아봇을 MindsLabChatBotExecute에서 사용하게 변경함에 따라 사용하지 않는 class 2018-12-28
 */
public class EncyclopDicIntentExecute_not_use implements IntentExecute {


	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {

		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();

		try {

			EncyclopDicService_not_use encyclopDicService = new EncyclopDicService_not_use();
			String resultMsg = encyclopDicService.getKbqaAnswer(intentExecuteVO.getUserId(),intentExecuteVO.getInputStr());
			log.info("EncyclopDicIntentExecute ResultMsg:"+resultMsg);

			if(StringUtils.isEmpty(resultMsg)){
				throw new Exception();
			}

			intentExecuteMessageVO.setResultMsg(resultMsg);


		} catch (EngEduException e) {
			throw new EngEduException(e.getErrCode(), e.getErrMsg());
		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	@Override
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {
		return null;
	}

}

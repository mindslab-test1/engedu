package ai.mindslab.engedu.common.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TtsPreVO implements Serializable {

    private String language;
    private String utter;

}

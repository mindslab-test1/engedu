package ai.mindslab.engedu.admin.paraphrase.service;

import ai.mindslab.engedu.admin.paraphrase.data.ParaphraseAdminVO;
import ai.mindslab.engedu.common.component.CommonExcelComponent;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class ParaphraseExcel {

    @Autowired
    CommonExcelComponent excelComponent;

    @Autowired
    ParaphraseAdminService paraphraseAdminService;

    private Workbook workbook;
    private String title;

    public Workbook createExcel(Map<String, Object> paramMap) throws EngEduException {

        int[] columnWiths ={300, 3800, 3800, 15000,3800};
        excelComponent.init();
        excelComponent.setSheetPrintSetting();

        //컬럼 width 세팅
        excelComponent.setColumnWidth(columnWiths);
        //title.
        excelComponent.createTitle(this.title, 1,4);

        List<String> column = new ArrayList<>();

        String[] columnMap ={"No", "대표어미", "활용어미", "사용여부"};

        List<ParaphraseAdminVO> list = paraphraseAdminService.getExcelParaphraseAdminList(paramMap);

        int rowCount = excelComponent.getRowCount();

        rowCount++;
        excelComponent.createColumnRow(rowCount, columnMap);

        long start = System.currentTimeMillis();


        for(ParaphraseAdminVO vo : list){
            String[] data = {
                    String.valueOf(vo.getWordId()),
                    vo.getMainWord(),
                    vo.getParaphraseWord(),
                    vo.getUseYn()+""
            };

            excelComponent.createDataRow(data);
        }

        this.workbook = excelComponent.getWorkBook();

        long end = System.currentTimeMillis();
        System.out.println("writeHSSFWorkbook : "+(end-start));

        return this.workbook;
    }


    public void setTitle(String title){
        this.title = title;
    }
}

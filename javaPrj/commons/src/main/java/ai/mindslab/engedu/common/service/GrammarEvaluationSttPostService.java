package ai.mindslab.engedu.common.service;

import ai.mindslab.engedu.evaluation.dao.GrammarAbbreviationMapper;
import ai.mindslab.engedu.evaluation.dao.data.GrammarEvaluationAbbreviationVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class GrammarEvaluationSttPostService {


    @Autowired
    private GrammarAbbreviationMapper grammarAbbreviationMapper;

    /**
     * 축약어 get List
     * @param paramBrandId BP_CODE
     * @return
     */
    public List<GrammarEvaluationAbbreviationVO> getSttAbbreviationPostMessage(String paramBrandId){

        List<GrammarEvaluationAbbreviationVO> result = grammarAbbreviationMapper.getGrammarAbbreviationList(paramBrandId);

        return result;
    }


}

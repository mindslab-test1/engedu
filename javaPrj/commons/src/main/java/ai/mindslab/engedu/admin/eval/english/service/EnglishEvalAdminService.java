package ai.mindslab.engedu.admin.eval.english.service;

import ai.mindslab.engedu.admin.eval.english.dao.EnglishEvalAdminMapper;
import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalAdminVO;
import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalFileExtVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class EnglishEvalAdminService {

    @Autowired
    EnglishEvalAdminMapper mapper;

    public int engEvalAdminCount(Map<String, Object> paramMap) throws EngEduException{
        return mapper.engEvalAdminCount(paramMap);
    }


    public List<EngEvalAdminVO> engEvalAdminList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.engEvalAdminList(paramMap);
    }

    public List<EngEvalAdminVO> engEvalAudioFileList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.engEvalAudioFileList(paramMap);
    }

    public List<EngEvalAdminVO> engEvalAdminExcelList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.engEvalAdminExcelList(paramMap);
    }

    public EngEvalAdminVO engEvalAdmin(Map<String, Object> paramMap) throws EngEduException{
        return mapper.engEvalAdmin(paramMap);
    }

    public List<EngEvalFileExtVO> engEvalFileExt(Map<String, Object> paramMap) throws EngEduException{
        return mapper.engEvalFileExt(paramMap);
    }

    public List<EngEvalFileExtVO> engEvalFileExtFilter(Map<String, Object> paramMap) throws EngEduException{
        return mapper.engEvalFileExtFilter(paramMap);
    }


}

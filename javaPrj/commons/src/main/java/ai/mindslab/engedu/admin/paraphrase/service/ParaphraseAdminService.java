package ai.mindslab.engedu.admin.paraphrase.service;


import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.paraphrase.dao.ParaphraseAdminMapper;
import ai.mindslab.engedu.admin.paraphrase.data.ParaphraseAdminVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.ExcelUtils;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ParaphraseAdminService {

    @Autowired
    private ParaphraseAdminMapper paraphraseAdminMapper;

    public int getParaphraseAdminCount(Map<String, Object> paramMap) throws EngEduException {

        return paraphraseAdminMapper.getParaphraseAdminCount(paramMap);
    }

    public List<ParaphraseAdminVO> getParaphraseAdminList(Map<String, Object> paramMap) throws EngEduException {
        return paraphraseAdminMapper.getParaphraseAdminList(paramMap);
    }

    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> uploadExcelData(UserVO vo, MultipartFile file) throws  EngEduException {
        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);

        Workbook workbook = ExcelUtils.getExcelWorkBook(file);
        Sheet sheet = (Sheet) workbook.getSheetAt(0);

        int rowCount = sheet.getPhysicalNumberOfRows();
        int cellCount = 0;
        int lastCellNum = 0;

        try {

            for (int i = 0; i < rowCount; i++) {
                Row row = sheet.getRow(i);

                if (row != null) {
                    cellCount = row.getPhysicalNumberOfCells();

                    if (i == 0) {
                        lastCellNum = row.getLastCellNum();

                        if (lastCellNum != 3) {
                            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                            resp.setMsg("Excel 양식이 잘못되었습니다.");
                            break;
                        }
                    } else {

                        ParaphraseAdminVO paraphraseAdminVO = new ParaphraseAdminVO();

                        paraphraseAdminVO.setMainWord(ExcelUtils.getCellValue(row.getCell(0)));
                        paraphraseAdminVO.setParaphraseWord(ExcelUtils.getCellValue(row.getCell(1)));
                        paraphraseAdminVO.setUseYn(ExcelUtils.getCellValue(row.getCell(2)));

                        paraphraseAdminMapper.insertParaphrase(paraphraseAdminVO);

                    }
                }
            }

        }catch (Exception e){
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
        }

        return resp;
    }

    public ParaphraseAdminVO selectParaphraseById(Map<String, Object> paramMap) throws  EngEduException{
        return paraphraseAdminMapper.selectDetail(paramMap);
    }

    public BaseResponse<Object> updateParaphraseDetail(Map<String, Object> paramMap) {
        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();
        try {
            int result = paraphraseAdminMapper.updateParaphraseDetail(paramMap);

            if(result > -1){
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        } catch (Exception e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }

    public BaseResponse<Object> deleteParaphrase(Map<String, Object> paramMap) {
        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();
        try {
            int result = paraphraseAdminMapper.deleteParaphrase(paramMap);

            if(result > -1){
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        } catch (Exception e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }

    public List<ParaphraseAdminVO> getExcelParaphraseAdminList(Map<String, Object> paramMap) throws  EngEduException {
        return paraphraseAdminMapper.getExcelParaphraseAdminList(paramMap);
    }
}

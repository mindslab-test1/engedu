package ai.mindslab.engedu.freetalk.service;

import ai.mindslab.engedu.freetalk.dao.EngFreeTalkExMapper;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkAnswerVO;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkGlobalSlotVO;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkLocalSlotVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class EngFreeTalkCache {

    private final int DenominatorTime =  60 * 60 * 1000;    // 1시간
//    private final int DenominatorTime =  60 * 1000;       // 1분

    @Autowired
    private EngFreeTalkExMapper engFreeTalkExMapper;

    private Map<String, EngFreeTalkAnswer> engFreeTalkAnswerMap;

    private Map<String, EngFreeTalkLocalSlot> engFreeTalkLocalSlotMap;

    private Map<String, EngFreeTalkGlobalSlot> engFreeTalkGlobalSlotMap;

    public EngFreeTalkCache() {
        engFreeTalkAnswerMap = new HashMap<>();
        engFreeTalkLocalSlotMap = new HashMap<>();
        engFreeTalkGlobalSlotMap = new HashMap<>();
    }

    private class EngFreeTalk{
        Date cacheTime;
    }

    private class EngFreeTalkAnswer extends EngFreeTalk {
        List<EngFreeTalkAnswerVO> list;
    }

    private class EngFreeTalkLocalSlot extends EngFreeTalk {
        List<EngFreeTalkLocalSlotVO> list;
        List<String> slotValueList;
    }

    private class EngFreeTalkGlobalSlot extends EngFreeTalk {
        List<EngFreeTalkGlobalSlotVO> list;
        List<String> slotValueList;
    }

    List<EngFreeTalkAnswerVO> getAnswerList(String brandId, String bookId, String chapterId, String questionId) {

        String key = brandId+"_"+bookId+"_"+chapterId+"_"+questionId;
        EngFreeTalkAnswer answer = engFreeTalkAnswerMap.get(key);

        if(answer != null) {    // 캐시 존재하는 경우

            long diff = new Date().getTime() - answer.cacheTime.getTime();
            long diffTime = diff / DenominatorTime;

            // 캐시 만료시
            if(diffTime >= 1) {
                answer.cacheTime = new Date();
                answer.list = this.getNewAnswerList(brandId, bookId, chapterId, questionId);
            }
        } else {
            answer = new EngFreeTalkAnswer();

            answer.cacheTime = new Date();
            answer.list = this.getNewAnswerList(brandId, bookId, chapterId, questionId);

            engFreeTalkAnswerMap.put(key, answer);
        }

        return answer.list;
    }

    private List<EngFreeTalkAnswerVO> getNewAnswerList(String brandId, String bookId, String chapterId, String questionId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("brandId", brandId);
        paramMap.put("bookId", bookId);
        paramMap.put("chapterId", chapterId);
        paramMap.put("questionId", questionId);

        return engFreeTalkExMapper.getAnswerList(paramMap);
    }

    List<String> getLocalSlotList(String brandId, String bookId, String chapterId, String questionId, String slotKey) {

        String key = brandId+"_"+bookId+"_"+chapterId+"_"+questionId+"_"+slotKey;
        EngFreeTalkLocalSlot localSlot = engFreeTalkLocalSlotMap.get(key);

        if(localSlot != null) {    // 캐시 존재하는 경우

            long diff = new Date().getTime() - localSlot.cacheTime.getTime();
            long diffTime = diff / DenominatorTime;

            // 캐시 만료시
            if(diffTime >= 1) {
                localSlot.cacheTime = new Date();
                localSlot.list = this.getNewLocalSlotList(brandId, bookId, chapterId, questionId, slotKey);
                localSlot.slotValueList = this.getNewLocalSlotValueList(brandId, bookId, chapterId, questionId, slotKey);
            }
        } else {
            localSlot = new EngFreeTalkLocalSlot();

            localSlot.cacheTime = new Date();
            localSlot.list = this.getNewLocalSlotList(brandId, bookId, chapterId, questionId, slotKey);
            localSlot.slotValueList = this.getNewLocalSlotValueList(brandId, bookId, chapterId, questionId, slotKey);

            engFreeTalkLocalSlotMap.put(key, localSlot);
        }

        return localSlot.slotValueList;
    }

    private List<EngFreeTalkLocalSlotVO> getNewLocalSlotList(String brandId, String bookId, String chapterId, String questionId, String slotKey) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("brandId", brandId);
        paramMap.put("bookId", bookId);
        paramMap.put("chapterId", chapterId);
        paramMap.put("questionId", questionId);
        paramMap.put("slotKey", slotKey);

        return engFreeTalkExMapper.getLocalSlotList(paramMap);
    }

    private List<String> getNewLocalSlotValueList(String brandId, String bookId, String chapterId, String questionId, String slotKey) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("brandId", brandId);
        paramMap.put("bookId", bookId);
        paramMap.put("chapterId", chapterId);
        paramMap.put("questionId", questionId);
        paramMap.put("slotKey", slotKey);

        return engFreeTalkExMapper.getLocalSlotValueList(paramMap);
    }

    List<String> getGlobalSlotList(String slotKey) {

        EngFreeTalkGlobalSlot globalSlot = engFreeTalkGlobalSlotMap.get(slotKey);

        if(globalSlot != null) {    // 캐시 존재하는 경우

            long diff = new Date().getTime() - globalSlot.cacheTime.getTime();
            long diffTime = diff / DenominatorTime;

            // 캐시 만료시
            if(diffTime >= 1) {
                globalSlot.cacheTime = new Date();
                globalSlot.list = this.getNewGlobalSlotList(slotKey);
                globalSlot.slotValueList = this.getNewGlobalSlotValueList(slotKey);
            }
        } else {
            globalSlot = new EngFreeTalkGlobalSlot();

            globalSlot.cacheTime = new Date();
            globalSlot.list = this.getNewGlobalSlotList(slotKey);
            globalSlot.slotValueList = this.getNewGlobalSlotValueList(slotKey);

            engFreeTalkGlobalSlotMap.put(slotKey, globalSlot);
        }

        return globalSlot.slotValueList;
    }

    private List<EngFreeTalkGlobalSlotVO> getNewGlobalSlotList(String slotKey) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("slotKey", slotKey);

        return engFreeTalkExMapper.getGlobalSlotList(paramMap);
    }

    private List<String> getNewGlobalSlotValueList(String slotKey) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("slotKey", slotKey);

        return engFreeTalkExMapper.getGlobalSlotValueList(paramMap);
    }
}

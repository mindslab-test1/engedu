package ai.mindslab.engedu.admin.qa.list.service;

import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.qa.list.dao.QAListAdminMapper;
import ai.mindslab.engedu.admin.qa.list.dao.data.QADomainVO;
import ai.mindslab.engedu.admin.qa.list.dao.data.QAListAdminVO;
import ai.mindslab.engedu.admin.qa.list.dao.data.QaAnswerAdminVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.bqa.dao.QaIndexMapper;
import ai.mindslab.engedu.bqa.dao.QaQnaParaphraseMapper;
import ai.mindslab.engedu.bqa.dao.data.QaExcelVo;
import ai.mindslab.engedu.bqa.dao.data.QaIndexVo;
import ai.mindslab.engedu.bqa.dao.data.QaQnaParaphraseVo;
import ai.mindslab.engedu.bqa.dao.data.QaQuestionVo;
import ai.mindslab.engedu.bqa.nlp.NlpAnalyzeClient;
import ai.mindslab.engedu.bqa.service.BqaManagementService;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.ExcelUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class QAListAdminService {

    private Logger logger = LoggerFactory.getLogger(QAListAdminService.class);

    @Autowired
    QAListAdminMapper mapper;

    @Autowired
    private QaIndexMapper indexMapper;

    @Autowired
    private QaQnaParaphraseMapper qnaParaphrase;

    @Autowired
    private NlpAnalyzeClient nlpAnalyzeClient;

    @Autowired
    private BqaManagementService bqaMgService;

    public final String USE_YN_Y = "Y";
    public final String USE_YN_N = "N";

    public List<QADomainVO> getDomainList(Map<String, Object> param) throws EngEduException{
        List<QADomainVO> nodeList = new ArrayList<>();
        List<QADomainVO> list = mapper.getDomainParent(param);
        QADomainVO rootVo = new QADomainVO();

        rootVo.setDomainId(0);
        rootVo.setChildCount(list.size());
        rootVo.setDepth(0);
        rootVo.setSubList(list);
        rootVo.setDomainName("All");
        rootVo.setParentDomainId(0);

        nodeList.add(rootVo);

        for(QADomainVO vo : list){
            if(vo.getChildCount() > 0){
                getChildDomain(vo, vo.getDomainId());
            }
        }

        return nodeList;
    }


    private void getChildDomain(QADomainVO doamin, int domainId) throws EngEduException{
        Map<String, Object> param = new HashMap<>();
        param.put("domainId", domainId);
        List<QADomainVO> list = mapper.getDomainSub(param);

        doamin.setSubList(list);

        for(QADomainVO vo : list){
            if(vo.getChildCount() > 0){
                getChildDomain(vo, vo.getDomainId());
            }
        }
    }



    public int getQAQuestionAdminListCount(Map<String, Object> param) throws EngEduException {
        return mapper.getQAQuestionAdminListCount(param);
    }



    public List<QAListAdminVO> getQAQuestionAdminList(Map<String, Object> param) throws EngEduException {
        return mapper.getQAQuestionAdminList(param);
    }


    public List<QAListAdminVO> getQAQuestionAdminExcelList(Map<String, Object> param) throws EngEduException {
        return mapper.getQAQuestionAdminExcelList(param);
    }

    public List<QADomainVO> getSelectDomainList() throws EngEduException {
        return mapper.getSelectDomainList();
    }

    public QAListAdminVO getQuestionDetailAdmin(Map<String, Object> param) throws EngEduException {
        return mapper.getQuestionDetailAdmin(param);
    }



    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> addQAandAnswer(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();

        try {

            JsonParser parser = new JsonParser();
            JsonObject obj = (JsonObject)parser.parse(paramMap.get("data").toString());
            int answerCount = Integer.parseInt(obj.get("answerCount").getAsString());

            int domainId = Integer.parseInt(obj.get("domainId").getAsString());
            String questionText = obj.get("questionText").getAsString();

            QAListAdminVO vo = new QAListAdminVO();

            vo.setDomainId(domainId);
            vo.setQuestion(questionText);
            vo.setUserId( paramMap.get("userId").toString());

            int result = 0;

            result = mapper.insertQuestionAdmin(vo);

            //qa index 테이블에 넣기.
            if(result > -1) {

                QaQuestionVo qa = new QaQuestionVo();

                qa.setQuestionId(vo.getQuestionId());
                qa.setQuestion(vo.getQuestion());
                qa.setCreatorId(vo.getUserId());
                qa.setDomainId(vo.getDomainId());
                qa.setUseYn(USE_YN_Y);
                qa.setMainYn(USE_YN_Y);

                result = this.createQna(qa);
            }

            //질문 업데이트가 잘끝나야 진행.
            if(result > -1) {
                //응답이 여러개 일 경우....
                if (answerCount > 1) {
                    //json 배열을 object 배열로 변경
                    QaAnswerAdminVO[] answerArray = new Gson().fromJson(obj.get("answerData"), QaAnswerAdminVO[].class);
                    //오브젝트 배열이 존재한다면...
                    if(answerArray !=null && answerArray.length > 0 ){
                        //답변 추가
                        for(QaAnswerAdminVO answer : answerArray){
                            answer.setQuestionId(vo.getQuestionId());
                            answer.setUserId(paramMap.get("userId").toString());
                            result = mapper.insertQaAnswerAdmin(answer);
                        }
                    }
                } else {
                    QaAnswerAdminVO answerVo = new Gson().fromJson(obj.get("answerData"), QaAnswerAdminVO.class);
                    answerVo.setQuestionId(vo.getQuestionId());
                    answerVo.setUserId(paramMap.get("userId").toString());

                    result = mapper.insertQaAnswerAdmin(answerVo);
                }
            }


            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }


    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> updateQAandAnswer(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();

        try {

            JsonParser parser = new JsonParser();
            JsonObject obj = (JsonObject)parser.parse(paramMap.get("data").toString());
            int answerCount = Integer.parseInt(obj.get("answerCount").getAsString());

            int questionId = Integer.parseInt(obj.get("questionId").getAsString());
            int domainId = Integer.parseInt(obj.get("domainId").getAsString());
            String questionText = obj.get("questionText").getAsString();

            QAListAdminVO vo = new QAListAdminVO();

            vo.setDomainId(domainId);
            vo.setQuestionId(questionId);
            vo.setQuestion(questionText);
            vo.setUserId( paramMap.get("userId").toString());
            vo.setQuestionType(obj.get("questionType").getAsString());
            int result = mapper.updateQuestionAdmin(vo);

            //qa index 테이블에 넣기.
            if(result > -1) {

                QaQuestionVo qa = new QaQuestionVo();

                qa.setQuestionId(vo.getQuestionId());
                qa.setQuestion(vo.getQuestion());
                qa.setQuestionType(vo.getQuestionType());
                qa.setCreatorId(vo.getUserId());
                qa.setDomainId(vo.getDomainId());
                qa.setUseYn(USE_YN_Y);
                qa.setMainYn(USE_YN_Y);

                result = this.updateQna(qa);
            }


            //질문 업데이트가 잘끝나야 진행.
            if(result > -1) {
                //응답이 여러개 일 경우....
                if (answerCount > 1) {
                    //json 배열을 object 배열로 변경
                    QaAnswerAdminVO[] answerArray = new Gson().fromJson(obj.get("answerData"), QaAnswerAdminVO[].class);
                    //오브젝트 배열이 존재한다면...
                    if(answerArray !=null && answerArray.length > 0 ){
                        //응답 업데이트 시작.
                        for(QaAnswerAdminVO answer : answerArray){

                            answer.setUserId(paramMap.get("userId").toString());
                            //id 가 new 인 경우는 새로 만든 응답... insert 한다.
                            if (answer.getAnswerId().equals("new")) {
                                result = mapper.insertQaAnswerAdmin(answer);
                            } else {
                                //new 가 아닌건 원래 있던 응답...업데이트 한다.
                                answer.setAnswerIdNum(Integer.parseInt(answer.getAnswerId()));
                                result = mapper.updateQaAnswerAdmin(answer);
                            }
                        }
                    }

                } else {

                    QaAnswerAdminVO answerVo = new Gson().fromJson(obj.get("answerData"), QaAnswerAdminVO.class);
                    answerVo.setUserId(paramMap.get("userId").toString());

                    if (answerVo.getAnswerId().equals("new")) {
                        result = mapper.insertQaAnswerAdmin(answerVo);
                    } else {
                        answerVo.setAnswerIdNum(Integer.parseInt(answerVo.getAnswerId()));
                        result = mapper.updateQaAnswerAdmin(answerVo);
                    }
                }
            }

            //기존에 있던 answer 를 지웠다면 use_yn 을 'N' 으로 꺾어준다.
            if (result > -1) {
                String deleteAnswerId = obj.get("deleteAnswerId").getAsString();

                if (deleteAnswerId.trim().length() > 0) {
                    String[] deleteAnswerIds = deleteAnswerId.split(",");
                    for (String answerId : deleteAnswerIds) {
                        int id = Integer.parseInt(answerId);
                        Map<String, Object> delMap = new HashMap<>();
                        delMap.put("answerId", id);
                        result = mapper.deleteQAanswerAdmin(delMap);
                    }
                }

                if (result > -1) {
                    resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                    resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
                }
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }


    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> deleteQAQuestionAdmin(Map<String, Object> paramMap){

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {

            int result = mapper.deleteQAQuestionAdmin(paramMap);

            if(result > -1){

                result = mapper.deleteQAanswerAdmin(paramMap);

                if(result > -1){
                    result = mapper.deleteQAIndexAdmin(paramMap);
                }

                if(result > -1){
                    resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                    resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
                }

            }

        } catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }



    @Transactional(rollbackFor = { Exception.class })
    public int createQna(QaQuestionVo q) throws EngEduException {
        logger.info("Service createQna parameter {}", q);

        int count = 0;
        try {
            List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
            List<QaIndexVo> qaQnaIndexList = bqaMgService.addParaphrase(q, paraphraseList);

//            qaQnaIndexList = nlpAnalyzeClient.analyzeList(qaQnaIndexList);

            Map<String, Object> param = new HashMap<>();
            param.put("list", qaQnaIndexList);

            int resultIndex = indexMapper.insertList(param);
            logger.debug("createQna resultIndex {}", resultIndex);

            if (resultIndex < 0) {
                throw new EngEduException(IRestCodes.ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX, IRestCodes.ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX);
            }

        } catch (Exception e) {
            logger.warn(IRestCodes.ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
            throw new EngEduException(IRestCodes.ERR_CODE_MGMT_UPDATE_ERROR_Q, IRestCodes.ERR_MSG_MGMT_UPDATE_ERROR_Q);
        }
        return count;
    }


    @Transactional(rollbackFor = { Exception.class })
    public int updateQna(QaQuestionVo qaQuestionVo) throws EngEduException {
        logger.info("Service updateQna parameter {}", qaQuestionVo);
        int resultCount =0;
        int questionId = qaQuestionVo.getQuestionId();

        try {

            Map<String, Object> param = new HashMap<>();
            param.put("useYn", USE_YN_N);
            param.put("questionId", questionId);
            param.put("updatorId", qaQuestionVo.getCreatorId());

            resultCount = indexMapper.updateUseYn(param);

            logger.debug("indexMapper updateUseYn resultIndex={}", resultCount);

            if (qaQuestionVo.getUseYn().equals(USE_YN_Y)) {

                List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
                List<QaIndexVo> qaQnaIndexList = bqaMgService.addParaphrase(qaQuestionVo, paraphraseList);
//                qaQnaIndexList = nlpAnalyzeClient.analyzeList(qaQnaIndexList);

                Map<String, Object> paramIndex = new HashMap<>();
                paramIndex.put("question", qaQuestionVo.getQuestion());
                paramIndex.put("questionId", questionId);
                paramIndex.put("updatorId", qaQuestionVo.getCreatorId());
                paramIndex.put("questionType", qaQuestionVo.getQuestionType());
                paramIndex.put("list", qaQnaIndexList);

                resultCount = indexMapper.insertList(paramIndex);
                logger.debug("indexMapper updateUseYn resultIndex={}", resultCount);

                if (resultCount < 0) {
                    throw new EngEduException(IRestCodes.ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX, IRestCodes.ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX);
                }
            }
        } catch (EngEduException e) {
            throw e;
        } catch (Exception e) {
            logger.warn(IRestCodes.ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
            throw new EngEduException(IRestCodes.ERR_CODE_MGMT_UPDATE_ERROR_Q, IRestCodes.ERR_MSG_MGMT_UPDATE_ERROR_Q);
        }
        return resultCount;
    }

    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> uploadExcelData(UserVO useVO, MultipartFile file) throws  EngEduException {
        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);

        Workbook workbook = ExcelUtils.getExcelWorkBook(file);
        Sheet sheet = (Sheet) workbook.getSheetAt(0);

        int rowCount = sheet.getPhysicalNumberOfRows();
        int lastCellNum = 0;

        List<QaExcelVo> list = new ArrayList<>();
        try {

            for (int i = 0; i < rowCount; i++) {
                Row row = sheet.getRow(i);

                if (row != null) {
                    if (i == 0) {
                        lastCellNum = row.getLastCellNum();

                        if (lastCellNum != 5) {
                            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                            resp.setMsg("Excel 양식이 잘못되었습니다.");
                            break;
                        }
                    } else {

                        QaExcelVo excelVo = new QaExcelVo();

                        excelVo.setQuestionId(Integer.parseInt(ExcelUtils.getCellValue(row.getCell(0))));
                        excelVo.setDomainId(Integer.parseInt(ExcelUtils.getCellValue(row.getCell(1))));
                        excelVo.setQuestion(ExcelUtils.getCellValue(row.getCell(2)));
                        excelVo.setAnswer(ExcelUtils.getCellValue(row.getCell(3)));
                        excelVo.setQuestionType(ExcelUtils.getCellValue(row.getCell(4)));
                        excelVo.setUserId(useVO.getUserId());
                        list.add(excelVo);

                    }
                }
            }

            int result =  0;

            if(list !=null && list.size() > 0) {
                result = bqaMgService.insertExcel(list);
            }

            if(result > -1){
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }else{
                resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
            }

        }catch (Exception e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
        }

        return resp;
    }

    public List<QAListAdminVO> getQAQuestionList(Map<String, Object> param) throws EngEduException {
        return mapper.getQAQuestionList(param);
    }
}
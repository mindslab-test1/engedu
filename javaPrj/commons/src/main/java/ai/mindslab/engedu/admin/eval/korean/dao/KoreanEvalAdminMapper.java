package ai.mindslab.engedu.admin.eval.korean.dao;

import ai.mindslab.engedu.admin.eval.korean.dao.data.KoreanEvalAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface KoreanEvalAdminMapper {

    int korEvalAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<KoreanEvalAdminVO> korEvalAdminList(Map<String, Object> paramMap) throws EngEduException;
    List<KoreanEvalAdminVO> korEvalAdminExcelList(Map<String, Object> paramMap) throws EngEduException;
    KoreanEvalAdminVO korEvalAdmin(Map<String, Object> paramMap) throws EngEduException;
}

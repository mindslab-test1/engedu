package ai.mindslab.engedu.bqa.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.bqa.dao.data.QaAnswerVo;

@Mapper
public interface QaAnswerMapper {

	int update(QaAnswerVo qaQnaVo);

	int insertList(Map<String, Object> paramMap);

	int updateUseYn(Map<String, Object> paramMap);

	QaAnswerVo getAnswer(Map<String, Object> paramMap);

	List<QaAnswerVo> getsByQuestionId(int questionId);
	
	int getCountByQuestionId(int questionId);

	int updateList(Map<String, Object> paramAnswer);
}

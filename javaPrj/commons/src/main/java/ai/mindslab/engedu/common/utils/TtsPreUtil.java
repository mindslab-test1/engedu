package ai.mindslab.engedu.common.utils;

import ai.mindslab.engedu.common.codes.ILanguageCodes;

import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.common.data.TtsPreVO;
import ai.mindslab.engedu.intent.IntentServiceType;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.regex.Pattern;

@Slf4j
public class TtsPreUtil {

    private static final String ENG_CHECK_PATTERN = "^[a-zA-Z]*$";
    private static final String CHINESE_CHARACTER = "[\u2e80-\u2eff\u31c0-\u31ef\u3200-\u32ff\u3400-\u4dbf\u4e00-\u9fbf\uf900-\ufaff]";
    private static final String SPECIAL_CHARACTER = "[,.!?;@'`’]*";

    public ArrayList<TtsPreVO> getWordLanguageDivision(String paramInputStr,String paramServiceType) {

        String hundredMessage = getTtsConversionMessage(paramInputStr);

        log.info("getTtsConversionMessage() hundredMessage : "+ hundredMessage);

        // 특수문자 -> 제거 처리
        String tempInputStrArr = hundredMessage.replaceAll(SPECIAL_CHARACTER, "");

        /* 특수문자 제거한 문자열을 공백으로 분리 */
        String[] tempStrArr = tempInputStrArr.split(IMarkCodes.ANSWER_SPLIT_TEXT);
        /* 특수문자 제거하지 않은 문자열을 공백으로 분리 */
        String[] inputStrArr = hundredMessage.split(IMarkCodes.ANSWER_SPLIT_TEXT);

        String serviceType = paramServiceType;
        ArrayList<TtsPreVO> result = getWordList(tempStrArr,inputStrArr,serviceType);

        return result;

    }

    private ArrayList<TtsPreVO> getWordList(String[] paramTempStrArr, String[] paramInputStrArr,String paramServiceType) {

        StringBuffer message = new StringBuffer();

        int tempCnt = 0;

        boolean cFlag;

        int inputSplitCnt = paramTempStrArr.length;


        ArrayList<TtsPreVO> result = new ArrayList<>();

        for (int i = 0; i < inputSplitCnt; i++) {

            /* 마지막 순서면 true */
            cFlag = i + 1 == inputSplitCnt;

            /* 문자열 영문이면 true */
            boolean isCurrentInputStrEng = Pattern.matches(ENG_CHECK_PATTERN, paramTempStrArr[i]);

            /* 마지막 순서 아닐 경우 && 현재 순서와 다음 순서열이 같을 경우 */
            if (!cFlag && isCurrentInputStrEng == Pattern.matches(ENG_CHECK_PATTERN, paramTempStrArr[i + 1])) {

                message.append(paramInputStrArr[i]).append(IMarkCodes.ANSWER_SPLIT_TEXT);
                tempCnt++;

            } else {

                // 문자열 언어 check
                String lang = isCurrentInputStrEng ? ILanguageCodes.ENG : ILanguageCodes.KOR;

                String inputMessage = "";
                boolean isKorEngDic = false;

                switch (paramServiceType){
                    case IntentServiceType.KORTOENGDIC :
                    case IntentServiceType.KORTOENGDIC_FLASH :
                        isKorEngDic = true;
                        break;
                     default:
                            isKorEngDic = false;
                }

                if (tempCnt > 0) {

                    inputMessage = isKorEngDic ? message + paramInputStrArr[i].trim().replaceAll(",",","+IMarkCodes.ANSWER_SPLIT_TEXT) :  message + paramInputStrArr[i].trim();

                    result.add(new TtsPreVO(lang, inputMessage));

                } else {
                    inputMessage = isKorEngDic ? paramInputStrArr[i].trim().replaceAll(",",","+IMarkCodes.ANSWER_SPLIT_TEXT) : paramInputStrArr[i].trim();
                   // result.add(new TtsPreVO(lang, inputStrArr[i].trim()));
                    result.add(new TtsPreVO(lang, inputMessage));

                }

                message = new StringBuffer();
                tempCnt = 0;

            }

        }

        return result;

    }

/*    public String getTtsConversionMessage(String paramInputStr){

        StringBuffer returnMessage = new StringBuffer();
        // 연속 공백 -> 공백 처리 && 특수문자 -> 제거 처리 && 한문 -> 공백 처리
        String tempInputStrArr = paramInputStr
                .replaceAll("\\s{2,}", " ")
                .replaceAll("[,.!?;@()~']*", "")
                .replaceAll(CHINESE_CHARACTER,"");

        // 공백 기준으로 문자열 split
        String[] tempStrSplitWhiteSpaceArr = tempInputStrArr.split(IMarkCodes.ANSWER_SPLIT_TEXT);

        // 공백 제거한 문자열 길이
        int tempRemoveWhiteSpaceLengh = 0;

        for(int i=0; i< tempStrSplitWhiteSpaceArr.length; i++){

            if(tempRemoveWhiteSpaceLengh + tempStrSplitWhiteSpaceArr[i].length() > 100){

                String hundredLimitMessage = tempStrSplitWhiteSpaceArr[i].substring(0, 100 - tempRemoveWhiteSpaceLengh);

                returnMessage.append(hundredLimitMessage + IMarkCodes.ANSWER_SPLIT_TEXT);

            }else{

                returnMessage.append(tempStrSplitWhiteSpaceArr[i] + IMarkCodes.ANSWER_SPLIT_TEXT);

            }

            tempRemoveWhiteSpaceLengh = String.valueOf(returnMessage).replaceAll(" ","").length();

            if(tempRemoveWhiteSpaceLengh ==100){

                break;

            }

        }

        return String.valueOf(returnMessage);
    }*/

    private String getTtsConversionMessage(String paramInputStr){

        // 연속 공백 -> 공백 처리 && , -> 공백 처리 && 한문 -> 공백 처리
        /**
         * 1. 한문 -> 공백 처리
         * 2. (, -> ( 처리
         * 3. () -> 제거
         */
        String tempInputStr = paramInputStr
                .replaceAll(CHINESE_CHARACTER,"")
                .replaceAll("\\s{2,}", IMarkCodes.ANSWER_SPLIT_TEXT)
                .replaceAll("\\(,","(")
                .replaceAll("\\(\\)","");

        // deepStudy tts 부분 300자 자르기
        int tempInputStrLength = tempInputStr.length();

        String returnMessage;
        String tempMessage;
        String cutMessage;
        if (tempInputStrLength > 300) {
            tempMessage = tempInputStr.substring(0,300);
            if (tempMessage.contains(".")){
                cutMessage = tempMessage.substring(tempMessage.lastIndexOf(".") +1);
                returnMessage = tempMessage.substring(0,tempMessage.length() - cutMessage.length());
            } else {
                cutMessage = tempMessage.substring(tempMessage.lastIndexOf(" ") +1);
                returnMessage = tempMessage.substring(0,tempMessage.length() - cutMessage.length());
            }

        } else {
            returnMessage = tempInputStr;
        }


//        String returnMessage = tempInputStrLength > 300 ? tempInputStr.substring(0,300) : tempInputStr;

        return returnMessage;

    }

}
package ai.mindslab.engedu.bqa.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.mindslab.engedu.bqa.dao.data.CmDomainVo;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.intent.IntentServiceMsg;
import ai.mindslab.engedu.intent.IntentServiceType;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import com.google.common.primitives.Ints;
import org.apache.solr.client.solrj.SolrServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ai.mindslab.engedu.bqa.dao.CmDomainMapper;
import ai.mindslab.engedu.bqa.dao.QaAnswerMapper;
import ai.mindslab.engedu.bqa.dao.QaIndexMapper;
import ai.mindslab.engedu.bqa.dao.data.QaAnswerVo;
import ai.mindslab.engedu.bqa.dao.data.QaDebugVo;
import ai.mindslab.engedu.bqa.dao.data.QaIndexVo;
import ai.mindslab.engedu.bqa.nlp.NlpAnalyzeClient;
import ai.mindslab.engedu.bqa.solr.BqaSolrClient;
import ai.mindslab.engedu.bqa.commons.codes.IBasicQAConst;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.bqa.commons.data.BaseListObject;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.bqa.commons.utils.KoreanAnalyze;


@Service
public class BqaSearchService implements IRestCodes, IBasicQAConst {

	private Logger logger = LoggerFactory.getLogger(BqaSearchService.class);

	@Autowired
	private QaIndexMapper indexMapper;

	@Autowired
	private QaAnswerMapper answerMapper;

	@Autowired
	private NlpAnalyzeClient nlpAnalyzeClient;

	@Autowired
	private BqaSolrClient bqaSolrClient;

	@Autowired
    private IntentMsgService intentMsgService;

	@Autowired
	private CmDomainMapper cmDomainMapper;

	@Value("${bqa.search.flow.type : 1}")
	private int searchType;

	@Value("${bqa.search.random.result : 1}")
	private int randomFlag;
	
	@Value("${bqa.search.solror.range :2}")
	private int range;

	@Value("${solr.bqa.search.or.result.count:3}")
	int solrOrResultCount;
	
	@Value("${engedu.bqa.dbsearch.type}")
	private String dbSearchType;
	
	public BaseResponse<Object> search(String question,int searchFlowType, int... domains) throws EngEduException {
		BaseResponse<Object> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		logger.info("getBasicAnswer question{}, domains {}",question,domains);

		if(searchFlowType != 0) {
			searchType = searchFlowType;
		}

		logger.info("dbSearchType dbSearchType: {}",dbSearchType);
		
		QaIndexVo qaIndexVo = null;
		
		if (question != null && question.trim().length() > 1) {
			
			if ( question.split(" ").length > 1 ) {
				if ("FULL".equals(dbSearchType)) {
					//qaIndexVo = searchByDbFullText(question, domains);
					qaIndexVo = searchByKeyword(question, domains);
				} else {
					qaIndexVo = searchByDb(question, domains);
				}
				/*
				if (qaIndexVo == null) {
					qaIndexVo = searchBySolr(question, domains);
				}
				*/
			} else {
				/*
				if (qaIndexVo == null) {
					qaIndexVo = searchBySolr(question, domains);
				}
				*/
			}
			
		}
		
		if (qaIndexVo == null  || qaIndexVo.getIndexId() == 0) {
			resp = getBqaNotFoundMsg();
			//throw new EngEduException(ERR_CODE_SEARCH_DB_SOLR_FAILURE, ERR_MSG_SEARCH_DB_SOLR_FAILURE);
		}


		if (qaIndexVo != null) {
			logger.debug("qaIndexVo = {}",qaIndexVo.toString());

			// 솔라 조회 후 qaIndexVo가 없으면
			// resp.code 404
			// resp.msg "NOT FOUND
			// resp.setData : 질문을 알아듣지 못했어.다시 말해 줄 수 있겠니?

			if(resp.getCode() == ERR_CODE_SUCCESS) {
				Map<String, Object> paramMap = new HashMap<>();
				paramMap.put("questionId", qaIndexVo.getQuestionId());
				paramMap.put("randomFlag", randomFlag);

				QaAnswerVo qaAnswerVo = answerMapper.getAnswer(paramMap);

			/*
			if(qaAnswerVo == null) {
				qaAnswerVo = new QaAnswerVo();
				qaAnswerVo.setAnswer("");
			}*/

				resp.setData(qaAnswerVo);
			}
		}

		return resp;
	}

	private QaIndexVo searchByKeyword(String question, int... domainIds) throws EngEduException {
		QaIndexVo qaIndexVo;
		logger.debug("searchByKeyword question{}, domainIds {}",question,domainIds);
		try {

			Map<String, Object> paramMap = new HashMap<>();

			if(domainIds != null && domainIds[0] != 0) {
				paramMap.put("domain", 1);
			}else {
				paramMap.put("domain", 0);
			}

			question = question.replaceAll("[,.!?;]*", "");

			paramMap.put("question", question);
			paramMap.put("domainIds", domainIds);
			paramMap.put("randomFlag", randomFlag);
			paramMap.put("question_type", "QT0001");

			qaIndexVo = indexMapper.searchKeyword(paramMap);
			logger.debug("searchByKeyword qaIndexVo{}",qaIndexVo);
		} catch (Exception e) {

			logger.warn("searchByKeyword DB search Error/{}", e);
			throw new EngEduException(ERR_CODE_SEARCH_DB_FAILURE, ERR_MSG_SEARCH_DB_FAILURE);
		}
		return qaIndexVo;
	}
	
	private QaIndexVo searchByDbFullText(String question, int... domainIds) throws EngEduException {
		QaIndexVo qaIndexVo;
		logger.debug("searchByDb question{}, domainIds {}",question,domainIds);
		try {

			Map<String, Object> paramMap = new HashMap<>();

			if(domainIds != null && domainIds[0] != 0) {
				paramMap.put("domain", 1);
			}else {
				paramMap.put("domain", 0);
			}
			
			question = question.replaceAll("[,.!?;]*", "");
			
			KoreanAnalyze koreanAnalyze = new KoreanAnalyze();
			
			String result = koreanAnalyze.guideWord(question);
			logger.debug("색인어 추출 guideWord : " + result);
			
			String result1 = koreanAnalyze.wordSpaceAnalyze(question,false);
			logger.debug("색인어 추출 wordSpaceAnalyze : " + result1);
			
			String allKeywords = result + "," + result1;
			logger.debug("색인어 추출 allKeywords : " + allKeywords);
			
			String outKeywords = koreanAnalyze.removeDuplicates(allKeywords);
			logger.debug("색인어 추출4 removeDuplicates : " + outKeywords);
			
			String keyword = makeParam(outKeywords);
			
			
//			paramMap.put("question", question);
			paramMap.put("question", keyword);
			paramMap.put("domainIds", domainIds);
			paramMap.put("randomFlag", randomFlag);
			
			qaIndexVo = indexMapper.searchFullText(paramMap);
			logger.debug("searchByDb qaIndexVo{}",qaIndexVo);
		} catch (Exception e) {

			logger.warn("DB search Error/{}", e);
			throw new EngEduException(ERR_CODE_SEARCH_DB_FAILURE, ERR_MSG_SEARCH_DB_FAILURE);
		}
		return qaIndexVo;
	}
	
	private String makeParam(String keyword) {
		
		StringBuffer sb = new StringBuffer();
		String[] keywords = keyword.split(",");
		for (int i=0; i<keywords.length; i++) {
			if (i == (keywords.length -1)) {
				sb.append(keywords[i] + "*");
			} else {
				sb.append(keywords[i] + "* ");
			}
		}
		
		return sb.toString();
	}

	private QaIndexVo searchByDb(String question, int... domainIds) throws EngEduException {
		QaIndexVo qaIndexVo;
		logger.debug("searchByDb question{}, domainIds {}",question,domainIds);
		try {

			Map<String, Object> paramMap = new HashMap<>();

			if(domainIds != null && domainIds[0] != 0) {
				paramMap.put("domain", 1);
			}else {
				paramMap.put("domain", 0);
			}
			paramMap.put("question", question);
			paramMap.put("domainIds", domainIds);
			paramMap.put("randomFlag", randomFlag);
			
			qaIndexVo = indexMapper.search(paramMap);
			logger.debug("searchByDb qaIndexVo{}",qaIndexVo);
		} catch (Exception e) {

			logger.warn("DB search Error/{}", e);
			throw new EngEduException(ERR_CODE_SEARCH_DB_FAILURE, ERR_MSG_SEARCH_DB_FAILURE);
		}
		return qaIndexVo;
	}

	private QaIndexVo searchBySolr(String question, int... domainIds) throws EngEduException{
		QaIndexVo qaIndexVo;
		logger.debug("searchBySolr question{}, domainIds {}",question,domainIds);
		try {
			String questionMorph = nlpAnalyzeClient.analyze(question);
			if(questionMorph == null || questionMorph.length() == 0) {
//				throw new EngEduException(ERR_CODE_SEARCH_NLP_RESULT_NOTHING_ERROR, ERR_MSG_SEARCH_NLP_RESULT_NOTHING_ERROR);
				questionMorph = "\"\"";
			}
			qaIndexVo = bqaSolrClient.search(questionMorph, domainIds);
		} catch (EngEduException e) {
			logger.warn("EngEduException Error/{}", e);
			throw new EngEduException(e.getErrCode(), e.getErrMsg());
		} catch (SolrServerException e) {
			logger.warn("SolrServerException search Error/{}", e);
			throw new EngEduException(ERR_CODE_SEARCH_SOLR_SYSTEM_ERROR, ERR_MSG_SEARCH_SOLR_SYSTEM_ERROR);
		} catch (IOException e) {
			logger.warn("IOException search Error/{}", e);
			throw new EngEduException(ERR_CODE_SEARCH_SOLR_IO_ERROR, ERR_MSG_SEARCH_SOLR_IO_ERROR);
		} catch (Exception e) {
			logger.warn("Solr search Error/{}", e);
			throw new EngEduException(ERR_CODE_SEARCH_SOLR_FAILURE, ERR_MSG_SEARCH_SOLR_FAILURE);
		}
		return qaIndexVo;
	}


	private BaseResponse<Object> getBqaNotFoundMsg() {
		BaseResponse<Object> baseResponse = null;
		try {
			String resultMsg = intentMsgService.getServiceMsg(IntentServiceType.BASICTALK,IntentServiceMsg.BQA_NOT_FOUND);
			baseResponse = new BaseResponse<>(ERR_CODE_NOT_FOUND, ERR_MSG_NOT_FOUND); // TODO
			baseResponse.setData(resultMsg);
		} catch (Exception e ){
			logger.warn("getBqaNotFoundMsg Exception Error/{}", e);
		}

		return baseResponse;
	}

	public int[] getDomainIds() {
		List<Integer> domainIdList = cmDomainMapper.getDomainIds();

		return Ints.toArray(domainIdList);
	}
}

package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Component
public class DateIntentExecute implements IntentExecute {
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();

		try {
			String inputStr = intentExecuteVO.getInputStr();

			StringBuilder stringBuilder = new StringBuilder();
			String[] pStrArr = {"오늘", "내일", "모레", "어제", "그저께"};
			String[] pAddStrArr = {"은", "은", "는", "는", "는"};
			int[] addDayArr = {0, 1, 2, -1, -2};

			Calendar cal = Calendar.getInstance();
			int index = 0;
			for(int i=0; i<pStrArr.length; i++) {
				Pattern p = Pattern.compile(pStrArr[i]);
				Matcher matcher = p.matcher(inputStr);

				if(matcher.find()) {
					index = i;
					cal.set(Calendar.DATE, cal.get(Calendar.DATE) + addDayArr[i]);
					break;
				}
			}

			int month = cal.get(Calendar.MONTH) + 1;
			int date = cal.get(Calendar.DATE);

			stringBuilder.append(pStrArr[index]).append(pAddStrArr[index]).append(" ");
			stringBuilder.append(month).append("월 ").append(date).append("일이야");

			String resultMsg = stringBuilder.toString();
			ExtInfo extInfo = new ExtInfo();
			extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0028.getDomainName());
			log.info("DateIntentExecute ResultMsg:" + resultMsg);
			intentExecuteMessageVO.setResultMsg(resultMsg);
			intentExecuteMessageVO.setExtInfo(extInfo);

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	@Override
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {

		return null;
	}
}
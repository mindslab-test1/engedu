package ai.mindslab.engedu.admin.engtalk.dao;

import ai.mindslab.engedu.admin.engtalk.dao.data.question.EngTalkAdminQuestionSVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.question.EngTalkAdminQuestionVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface EngFreeTalkQuestionAdminMapper {

    int EngTalkQuestionAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<EngTalkAdminQuestionVO> EngTalkQuestionAdminList(Map<String, Object> paramMap) throws EngEduException;
    List<EngTalkAdminQuestionVO> EngTalkQuestionExcelList(Map<String, Object> paramMap) throws EngEduException;
    EngTalkAdminQuestionVO EngTalkQuestionAdmin(Map<String, Object> paramMap) throws EngEduException;
    int updateEngTalkQuestionAdmin(Map<String, Object> paramMap) throws EngEduException;
    int deleteEngTalkQuestionAdmin(Map<String, Object> paramMap) throws EngEduException;
    int insertEngTalkQuestionAdmin(EngTalkAdminQuestionSVO svo) throws EngEduException;
}

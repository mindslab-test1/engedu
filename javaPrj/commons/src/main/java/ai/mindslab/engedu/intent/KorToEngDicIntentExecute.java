package ai.mindslab.engedu.intent;

import java.util.List;

import ai.mindslab.engedu.common.codes.IRestCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.WordExtractUtil;
import ai.mindslab.engedu.dic.dao.data.KorToEngDicVO;
import ai.mindslab.engedu.dic.service.KortoEngDicService;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class KorToEngDicIntentExecute implements IntentExecute {
	
	@Autowired
	private KortoEngDicService kortoEngDicService;

	@Autowired
	private IntentMsgService intentMsgService;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO executeMessageVO = new IntentExecuteMessageVO();

		try {
			WordExtractUtil wordExtractUtil = new WordExtractUtil();
			intentExecuteVO.setInputStr(wordExtractUtil.extractWord(intentExecuteVO.getServiceType(), intentExecuteVO.getInputStr()));

			List<KorToEngDicVO> list = kortoEngDicService.getSearch(intentExecuteVO.getInputStr());

			log.info("list.size():" + list.size());

			StringBuilder resultMsgBuilder = new StringBuilder();
			for (KorToEngDicVO korToEngDicVO : list) {
				log.info("korToEngDicVO.toString():" + korToEngDicVO.toString());
				resultMsgBuilder.append(korToEngDicVO.getEng()).append(",");
			}
			String resultMsg = resultMsgBuilder.toString();


			// 결과가 없으면
			if ("".equals(resultMsg)) {
				resultMsg = intentMsgService.getServiceMsg(IntentServiceType.KORTOENGDIC, IntentServiceMsg.KORTOENGDIC_NOT_FOUND);
			} else {
				resultMsg = resultMsg.substring(0, resultMsg.length() - 1);

				String serviceMsg = intentMsgService.getServiceMsg(IntentServiceType.KORTOENGDIC, IntentServiceMsg.KORTOENGDIC_MAKE_MENT);

				resultMsg = serviceMsg.replaceAll("_question_", intentExecuteVO.getInputStr()).replaceAll("_answer_", resultMsg);

			}
			log.info("KorToEngDicIntentExecute resultMsg:" + resultMsg);

			executeMessageVO.setData(list);
			executeMessageVO.setResultMsg(resultMsg);

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return executeMessageVO;
	}

	@Override
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {
		return null;
	}
}

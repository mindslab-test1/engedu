package ai.mindslab.engedu.intent.vo;

import lombok.Data;

@Data
public class ExtInfo {
	
	private String menuId="";
	private String url="";
	private String printMeans="";
	private String currentDomain = "";

}

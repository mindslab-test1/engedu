package ai.mindslab.engedu.admin.findservice.dao;

import ai.mindslab.engedu.admin.findservice.dao.data.FindRegexVO;
import ai.mindslab.engedu.admin.servicement.dao.data.SelectOptVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface FindServiceAdminMapper {

    List<SelectOptVO>  selectTypeList(Map<String, Object> paramMap) throws EngEduException;
    int getFindServiceAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<FindRegexVO> getFindServiceAdminList(Map<String, Object> paramMap) throws EngEduException;
    List<FindRegexVO> getFindServiceExcelList(Map<String, Object> paramMap) throws EngEduException;
    FindRegexVO getFindServiceAdmin(Map<String, Object> paramMap) throws EngEduException;
    int updateFindRegexAdmin(Map<String, Object> paramMap) throws EngEduException;
    int deleteFindRegexAdmin(Map<String, Object> paramMap) throws EngEduException;
    int insertFindServiceAdmin(Map<String, Object> paramMap) throws EngEduException;

}

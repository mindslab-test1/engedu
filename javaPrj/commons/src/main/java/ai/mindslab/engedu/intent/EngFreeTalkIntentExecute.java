package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkQuestionVO;
import ai.mindslab.engedu.freetalk.service.EngFreeTalkExService;
import ai.mindslab.engedu.freetalk.service.EngFreeTalkService;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Slf4j
@Component
public class EngFreeTalkIntentExecute implements IntentExecute {
	
	@Autowired
	private EngFreeTalkService engFreeTalkService;

	@Autowired
	private EngFreeTalkExService engFreeTalkExService;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();
		ExtInfo extInfo = new ExtInfo();
		try {
			//String userId 	= intentExecuteVO.getUserId();
			String inputStr = intentExecuteVO.getInputStr();

			// TODO: 클라이언트에서 받아오기 - 처음에 받던가 API로 요청하던가
			EngFreeTalkQuestionVO engFreeTalkQuestionVO = intentExecuteVO.getEngFreeTalkQuestionVO();
			String brandId = engFreeTalkQuestionVO.getBrandId();
			String bookId = engFreeTalkQuestionVO.getBookId();
			String chapterId = engFreeTalkQuestionVO.getChapterId();
			String questionId = engFreeTalkQuestionVO.getQuestionId();
			String questionText = "TEST";	// questionId 없이 questionText만 넘어올시
			
			log.info("engFreeTalkQuestionVO:"+engFreeTalkQuestionVO.toString());

			// 질문 존재 여부 확인
			EngFreeTalkQuestionVO questionVO = engFreeTalkService.getQuestion(brandId, bookId, chapterId, questionId, questionText);
			if (ObjectUtils.isEmpty(questionVO)) {
				throw new EngEduException(IRestCodes.ERR_CODE_NOT_FOUND, "QUESTION " + IRestCodes.ERR_MSG_NOT_FOUND);
			}
			questionId = StringUtils.isEmpty(questionId) ? questionVO.getQuestionId() : questionId;

			// Solr 검색
//			boolean isExist = engFreeTalkService.search(brandId, bookId, chapterId, questionId, inputStr);

			// Diff 통한 검색
			boolean isExist = engFreeTalkExService.search(brandId, bookId, chapterId, questionId, inputStr);

			String resultMsg;
			if (isExist) {
				resultMsg = "1";
			} else {
				resultMsg = "0";
			}
			extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0022.getDomainName());
			log.info("EngFreeTalkIntentExecute ResultMsg:" + resultMsg);
			intentExecuteMessageVO.setResultMsg(resultMsg);
			intentExecuteMessageVO.setExtInfo(extInfo);

		} catch (EngEduException e) {
			throw new EngEduException(e.getErrCode(), e.getErrMsg());
		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {

		return null;
	}
}
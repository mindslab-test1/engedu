package ai.mindslab.engedu.common.service;


import ai.mindslab.engedu.common.sttPost.dialog.CalculatePostUtil;
import ai.mindslab.engedu.common.sttPost.dialog.MathDicSttPostUtil;
import ai.mindslab.engedu.common.sttPost.dialog.TimesTableSttPostUtil;
import ai.mindslab.engedu.intent.IntentServiceType;

public class DialogSttPostService {

    /**
     *
     * @param paramIsFirstServiceEntry 게임 처음 시작 여부 true = 게임 첫 시작 false = 게임 진행 중
     * @param paramServiceType 서비스 타입 ex) ST0021 = 구구단
     * @param paramInputStr 사용자 발화 문장 ex) 23
     * @param paramActiveProfile 현재 서버 properties ex) deep
     * @return
     */
    public String getSttPostMessage(
            boolean paramIsFirstServiceEntry,
            String paramServiceType,
            String paramInputStr,
            String paramActiveProfile) throws Exception
    {

        String message = paramInputStr;

        switch (paramServiceType){

            // 구구단 게임
            case IntentServiceType.TIMESTABLE :

                message = message.trim();

                // 문자열 길이가 3자 이상 && 게임 처음 시작이 아닐 경우
                if(message.length() >=3 && !paramIsFirstServiceEntry){
                    TimesTableSttPostUtil timesTableUtil = new TimesTableSttPostUtil();
                    message = timesTableUtil.getFirstPlusLastMessage(message);
                }

                break;
                
                // 수학사전일 경우  "원"으로 발음을 하면 1로 리턴되는 경우 원으로 변경한다 
            case IntentServiceType.MATHDIC :
            case IntentServiceType.MATHDIC_FLASH :

                // 1로 들어오면 원으로 변환
                MathDicSttPostUtil mathDicSttPostUtil = new MathDicSttPostUtil();
                message = mathDicSttPostUtil.getNumberToMessage(message);
            	
            	break;
            	
            case IntentServiceType.CALCULATE :

                // 더 하기 -> 더하기로 변환
                CalculatePostUtil calculatePostUtil = new CalculatePostUtil();
                message = calculatePostUtil.getPlusMessageTrim(message);

                // 게임 시작이 처음이 아닐 경우에만
                if(!paramIsFirstServiceEntry){

                    message = calculatePostUtil.getZeroMessageRemove(message);

                }

            	break;
        }

        return message;

    }
}

package ai.mindslab.engedu.admin.eval.english.dao;

import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalAdminVO;
import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalFileExtVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface EnglishEvalAdminMapper {

    int engEvalAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<EngEvalAdminVO> engEvalAdminList(Map<String, Object> paramMap) throws EngEduException;
    List<EngEvalAdminVO> engEvalAudioFileList(Map<String, Object> paramMap) throws EngEduException;
    List<EngEvalAdminVO> engEvalAdminExcelList(Map<String, Object> paramMap) throws EngEduException;
    EngEvalAdminVO engEvalAdmin(Map<String, Object> paramMap) throws EngEduException;
    List<EngEvalFileExtVO> engEvalFileExt(Map<String, Object> paramMap) throws EngEduException;
    List<EngEvalFileExtVO> engEvalFileExtFilter(Map<String, Object> paramMap) throws EngEduException;
}

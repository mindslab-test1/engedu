package ai.mindslab.engedu.admin.servicement.service;

import ai.mindslab.engedu.admin.math.dao.data.MathDicAdminVO;
import ai.mindslab.engedu.admin.math.service.MathDicAdminService;
import ai.mindslab.engedu.admin.servicement.dao.data.ServiceMentAdminVO;
import ai.mindslab.engedu.common.component.CommonExcelComponent;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class GameMentExcel {

    @Autowired
    CommonExcelComponent excelComponent;

    @Autowired
    GameMentService gameMentService;

    private Workbook workbook;
    private Map<String, Object> model;
    private HttpServletResponse response;
    private String title;


    public Workbook createExcel(Map<String, Object> paramMap) throws EngEduException {

        int[] columnWiths ={300, 2000, 4000, 4000, 7000};
        excelComponent.init();
        excelComponent.setSheetPrintSetting();

        //컬럼 width 세팅
        excelComponent.setColumnWidth(columnWiths);
        //title.
        excelComponent.createTitle(this.title, 1,4);

        List<String> column = new ArrayList<>();

        String[] columnMap ={"No", "게임구분", "조건", "발화텍스트"};

       List<ServiceMentAdminVO> list = gameMentService.getGameMentExcelList(paramMap);

        int rowCount = excelComponent.getRowCount();

        rowCount++;

        excelComponent.createColumnRow(rowCount, columnMap);

        long start = System.currentTimeMillis();


        for(ServiceMentAdminVO vo : list){
            String[] data = {
                    vo.getSeq()+"",
                    vo.getServiceType(),
                    vo.getCode(),
                    vo.getMsg()
            };

            excelComponent.createDataRow(data);
        }

        this.workbook = excelComponent.getWorkBook();

        long end = System.currentTimeMillis();
        System.out.println("writeHSSFWorkbook : "+(end-start));

        return this.workbook;
    }


    public void setTitle(String title){
        this.title = title;
    }
}

package ai.mindslab.engedu.freetalk.service;


import ai.mindslab.engedu.common.utils.DiffMatchPatchUtil;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkAnswerVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class EngFreeTalkExService {

	@Autowired
	private EngFreeTalkCache engFreeTalkCache;

	public boolean search(String brandId, String bookId, String chapterId, String questionId, String inputStr) {
		boolean isExist = false;

		List<EngFreeTalkAnswerVO> answerList = engFreeTalkCache.getAnswerList(brandId, bookId, chapterId, questionId);

		for (EngFreeTalkAnswerVO answer : answerList) {
			String answerStr = answer.getAnswerText();
			DiffMatchPatchUtil dmp = new DiffMatchPatchUtil();
			LinkedList<DiffMatchPatchUtil.Diff> diffs = dmp.diffWordMode(answerStr, inputStr);

			if(diffs.size() == 1 && diffs.get(0).operation == DiffMatchPatchUtil.Operation.EQUAL) {

				isExist = true;
			} else if (this.isSlotsOnly(diffs)) {
				List<String> slotList = this.getArrayListFromDiffs(diffs, 'S');
				List<String> utterWordList = this.getArrayListFromDiffs(diffs, 'U');

				List<List<String>> slotValueLists = this.getSlotValueLists(brandId, bookId, chapterId, questionId, slotList);

				isExist = this.isSlotValuesExist(slotValueLists, utterWordList);
			}

			if(isExist) {
				break;
			}
		}
		return isExist;
	}

	private boolean isSlotsOnly(LinkedList<DiffMatchPatchUtil.Diff> diffs) {
		int slotCnt = 0;
		int nonSlotCnt = 0;
		for (DiffMatchPatchUtil.Diff diff : diffs) {
			switch (diff.operation) {
				case DELETE:
					String[] strArr = diff.text.split(" ");
					Pattern p = Pattern.compile("(<.*?>)|(\\[.*?])");
					for (String str : strArr) {
						Matcher isSlots = p.matcher(str);
						if (!isSlots.matches()) {
							nonSlotCnt++;
						} else {
							slotCnt++;
						}
					}
					break;
				case INSERT:
					break;
				case EQUAL:
					break;
			}
		}

		return nonSlotCnt == 0 && slotCnt > 0;
	}

	private List<String> getArrayListFromDiffs(LinkedList<DiffMatchPatchUtil.Diff> diffs, char listType) {
		List<String> arrList = new ArrayList<>();

		for(DiffMatchPatchUtil.Diff diff : diffs) {
			if(!diff.text.isEmpty()) {
				String[] strArr = diff.text.split(" ");
				switch (diff.operation) {
					case DELETE:
						if(listType == 'S'){
							Collections.addAll(arrList, strArr);
						}
						break;
					case INSERT:
						if(listType == 'U'){
							Collections.addAll(arrList, strArr);
						}
						break;
					case EQUAL:
						break;
				}
			}
		}
		return arrList;
	}

	private List<List<String>> getSlotValueLists(String brandId, String bookId, String chapterId, String questionId, List<String> slotList) {
		List<List<String>> slotValueLists = new ArrayList<>();
		for(String slot : slotList) {
			List<String> slotValueList;

			Matcher isLocalSlot = Pattern.compile("(<.*?>)").matcher(slot);
			slot = slot.replaceAll("[<>\\[\\]]*", "");
			if(isLocalSlot.matches()) {
				// 로컬 슬롯테이블에서
				slotValueList = engFreeTalkCache.getLocalSlotList(brandId, bookId, chapterId, questionId, slot);
			} else {
				// 글로벌 슬롯테이블에서
				slotValueList = engFreeTalkCache.getGlobalSlotList(slot);
			}
			slotValueLists.add(slotValueList);
		}
		return slotValueLists;
	}

	private boolean isSlotValuesExist(List<List<String>> slotValueLists, List<String> utterWordList) {
		boolean isEqual = false;
		int index = 0;
		int totalCount = utterWordList.size();
		// slot 갯수
		int slotCount = slotValueLists.size();
		// slot equal 갯수
		int slotIndexEqualCount = 0;

		outerLoop:
		for (List<String> slotValueList : slotValueLists) {
			for (String slotValue : slotValueList) {
				List<String> slotValueWords = new ArrayList<>(Arrays.asList(slotValue.split(" ")));
				int wordCount = slotValueWords.size();
				int toIndex = index + wordCount >= utterWordList.size() ? utterWordList.size() : index + wordCount;

				List<String> compareWords = utterWordList.subList(index, toIndex);
				if (slotValueWords.equals(compareWords)) {
					index += wordCount;
					slotIndexEqualCount++;
					if (index == totalCount && slotCount == slotIndexEqualCount) {
						isEqual = true;
						break outerLoop;
					}
					break;
				}
			}
		}
		return isEqual;
	}
}
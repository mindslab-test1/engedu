package ai.mindslab.engedu.admin.korengdic.dao;

import ai.mindslab.engedu.admin.korengdic.dao.data.KorToEngDicAdminVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface KorToEngDicAdminMapper {
	public int getCount();

	public List<KorToEngDicAdminVO> getSearch(Map<String, String> hashMap);
	public int getDicAdminCount(Map<String, Object> hashMap);
	public List<KorToEngDicAdminVO>getDicAdminList(Map<String, Object> hashMap);
	public KorToEngDicAdminVO selectDetail(Map<String, Object> hashMap);
	public int updateKorengDetail(Map<String, Object> hashMap);
	public int deleteKorEngDic(Map<String, Object> hashMap);
	public List<KorToEngDicAdminVO> getExcelDicAdminList(Map<String, Object> hashMap);
	public int insertKorEngDic(KorToEngDicAdminVO vo);
}

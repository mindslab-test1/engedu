package ai.mindslab.engedu.dic.service;


import ai.mindslab.engedu.dic.dao.KorDicAntonymMapper;
import ai.mindslab.engedu.dic.dao.data.KorDicAntonymVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class KorDicAntonymService {
	
	@Autowired
	private KorDicAntonymMapper korDicAntonymMapper;
	
	public int getCount() {
		return korDicAntonymMapper.getCount();
	}

	public List<KorDicAntonymVO> getSearch(String searchKey) {

		Map<String, String> hashMap = new HashMap<>();
		hashMap.put("searchKey", searchKey);
		
		return korDicAntonymMapper.getSearch(hashMap);
	}

}

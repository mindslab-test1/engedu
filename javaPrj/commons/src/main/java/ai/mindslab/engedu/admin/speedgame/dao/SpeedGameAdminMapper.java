package ai.mindslab.engedu.admin.speedgame.dao;

import ai.mindslab.engedu.admin.speedgame.dao.data.SpeedGameAdminSVO;
import ai.mindslab.engedu.admin.speedgame.dao.data.SpeedGameAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SpeedGameAdminMapper {

    int getSpeedGameAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<SpeedGameAdminVO> getSpeedGameAdminList(Map<String, Object> paramMap) throws EngEduException;
    SpeedGameAdminVO getSpeedGameAdmin(Map<String, Object> paramMap) throws EngEduException;
    int updateSpeedGameAdmin(Map<String, Object> paramMap) throws EngEduException;
    int deleteSpeedGameAdmin(Map<String, Object> paramMap) throws EngEduException;
    int insertSpeedGameAdmin(SpeedGameAdminSVO svo) throws EngEduException;


}

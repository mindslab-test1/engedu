package ai.mindslab.engedu.bqa.dao.data;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
public class CmDomainVo implements Serializable {

	public static final String SEARCH_USED_Y = "Y";
	public static final String SEARCH_USED_N = "N";
	private int domainId;
	private int teamId;
	private int parentDomainId;
	private int depth;
	private String domainName;
	private String descript;
	private Date createdDtm;
	private Date  updatedDtm;
	private String creatorId;
	private String  updatorId;
	private String status;
}

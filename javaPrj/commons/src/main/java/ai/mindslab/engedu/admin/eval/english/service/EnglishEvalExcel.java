package ai.mindslab.engedu.admin.eval.english.service;

import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalAdminVO;
import ai.mindslab.engedu.admin.kordic.dao.data.KorDicAdminVO;
import ai.mindslab.engedu.admin.kordic.service.KorDicSynonymAdminService;
import ai.mindslab.engedu.common.component.CommonExcelComponent;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class EnglishEvalExcel {

    @Autowired
    CommonExcelComponent excelComponent;

    @Autowired
    EnglishEvalAdminService service;

    private Workbook workbook;
    private String title;


    public Workbook createExcel(Map<String, Object> paramMap) throws EngEduException {

        int[] columnWiths ={300, 3000, 4000, 4000, 6000, 6000,3000,3000,6000, 3000};
        excelComponent.init();
        excelComponent.setSheetPrintSetting();

        //컬럼 width 세팅
        excelComponent.setColumnWidth(columnWiths);
        //title.
        excelComponent.createTitle(this.title, 1,4);

        List<String> column = new ArrayList<>();

        String[] columnMap ={"ID", "학습자 ID", "평가 일시", "정답텍스트", "발화텍스트","문법점수","발음점수", "발음세부점수","평가점수"};

        List<EngEvalAdminVO> list = service.engEvalAdminExcelList(paramMap);

        int rowCount = excelComponent.getRowCount();

        rowCount++;
        excelComponent.createColumnRow(rowCount, columnMap);

        long start = System.currentTimeMillis();


        for(EngEvalAdminVO vo : list){
            String[] data = {
                    vo.getEvalId(),
                    vo.getUserId(),
                    vo.getCreatedTime(),
                    vo.getAnswerText(),
                    vo.getUserText(),
                    vo.getGrammarScore(),
                    vo.getPronunceScore(),
                    vo.getPronounceEtcScore(),
                    vo.getScore()
            };

            excelComponent.createDataRow(data);
        }

        this.workbook = excelComponent.getWorkBook();

        long end = System.currentTimeMillis();
        System.out.println("writeHSSFWorkbook : "+(end-start));

        return this.workbook;
    }


    public void setTitle(String title){
        this.title = title;
    }



}

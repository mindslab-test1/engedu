package ai.mindslab.engedu.admin.eval.english.dao.data;


import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

@Data
public class EngEvalFileExtVO implements Serializable {

    private String evalSearchType;
    private ArrayList<Integer> evalIdList;
    private String userIdSearchType;
    private String userIdSearchText;
    private String grammarScoreSearchType;
    private int grammarScore;
    private String pronounceScoreSearchType;
    private int pronounceScore;
    private String userTextSearchType;
    private String userText;
    private String answerTextSearchType;
    private String answerText;
    private Date startDate;
    private Date endDate;

    private String evalId;
    private String userId;

    private String filePath;

    private String fileTextName;

    private String fileWaveName;

    private String fileFullPath;

}

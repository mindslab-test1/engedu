package ai.mindslab.engedu.game.dao;

import ai.mindslab.engedu.game.dao.data.SpeedGameUsedVO;
import ai.mindslab.engedu.game.dao.data.SpeedGameVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface SpeedGameMapper {

    Integer getMaxSeq(String userId);

    Integer getRandomSpeedGameId();

    SpeedGameVO getGameWord(Map<String, Object> hashMap);

    int insertUsedThumb(Map<String, Object> hashMap);

    SpeedGameUsedVO getUsedThumb(Map<String, Object> hashMap);

    int deleteUsedThumbs(String userId);
}

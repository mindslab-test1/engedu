package ai.mindslab.engedu.intent.service;


import ai.mindslab.engedu.intent.dao.IntentFinderMapper;
import ai.mindslab.engedu.intent.dao.data.UserServiceVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import ai.mindslab.engedu.intent.vo.IntentFinderVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class IntentFinderService {
	
	@Autowired
	private IntentFinderMapper intentFinderMapper ;
	
	public UserServiceVO getCurrentIntent(String userId, String timeOut) {
		
		Map<String, String> serviceMap = new HashMap<>();
		serviceMap.put("userId", userId);
		serviceMap.put("timeOut", timeOut);
		
		return intentFinderMapper.getCurrentIntent(serviceMap);
	}

	public int updateServiceType(String serviceType, String userId) {
		Map<String, String> serviceMap = new HashMap<>();
		serviceMap.put("userId", userId);
		serviceMap.put("serviceType", serviceType);

		return intentFinderMapper.updateServiceType(serviceMap);
	}

	public int purgeServiceType(String userId) {
		return intentFinderMapper.purgeServiceType(userId);
	}

	public String getRegexOfServiceType(String serviceType) {
		return intentFinderMapper.getRegexOfServiceType(serviceType);
	}
	
	public ArrayList<String> getServiceTypeList() {
		return intentFinderMapper.getServiceTypeList();
	}
	
	public int updateDicsTimeOut(String userId, String timeOut) {
		
		Map<String, String> serviceMap = new HashMap<>();
		serviceMap.put("userId", userId);
		serviceMap.put("timeOut", timeOut);
		
		return intentFinderMapper.updateDicsTimeOut(serviceMap);
	}
}

package ai.mindslab.engedu.freetalk.dao.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EngFreeTalkLocalSlotVO implements Serializable {
	private String brandId;
	private String bookId;
	private String chapterId;
	private String questionId;
	private String slotKey;
	private String slotValue;
	private Date createdDtm;
	private String creatorId;
	private Date updatedDtm;
	private String updatorId;
}

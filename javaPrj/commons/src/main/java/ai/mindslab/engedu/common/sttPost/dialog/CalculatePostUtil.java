package ai.mindslab.engedu.common.sttPost.dialog;

import ai.mindslab.engedu.common.codes.IMathCodes;
import ai.mindslab.engedu.common.codes.IMarkCodes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CalculatePostUtil {

    /**
     * 계산기 후처리
     * @param paramMessage 발화 문장
     * @return 후처리한 발화 문장
     */
    public String getPlusMessageTrim(String paramMessage){


        String result = paramMessage.replaceAll("더 하기", IMathCodes.PLUS_TEXT);

        return result;

    }

    /**
     * 계산기 후처리 규칙
     * - 숫자 사이 공백, 숫자 뒤 은, 는 텍스트는 제외함
     * - 숫자가 a00b0c 형태인 경우, abc 값으로 변경 (세자리)
     * - 숫자가 a00b0 형태인 경우, ab0 값으로 변경 (세자리)
     * - 숫자가 a00c 형태인 경우, a0c 값으로 변경 (세자리)
     * - 숫자가 a00 형태인 경우, a00 값으로 유지 (세자리)
     * - 숫자가 b0c 형태인 경우, bc 값으로 변경 (두자리)
     * - 숫자가 b0 형태인 경우, b0 값으로 유지 (두자리)
     * - 숫자가 c 형태인 경우, c 값으로 유지 (한자리)
     * @param paramMessage 발화 문장
     * @return 후처리한 발화 문장
     * @throws Exception 예외
     */
    public String getZeroMessageRemove(String paramMessage) throws Exception{

        String[] keyStrArr 		= {
                IMathCodes.PLUS_TEXT,
                IMathCodes.MINUS_TEXT,
                IMathCodes.MUL_TEXT,
                IMathCodes.DIV_TEXT
        };

        String result;
        String etcStr ="";

        List<String> tempList = new ArrayList<>();
        String key = "";

        for (String keyArr : keyStrArr) {

            if (paramMessage.contains(keyArr)) {

                key = keyArr;
                tempList = Arrays.asList(paramMessage.split(keyArr));
                break;

            }

        }

        for(int j=0; j< tempList.size(); j++){

            String str = tempList.get(j);

            if(j==1){

                etcStr = str.replaceAll(IMarkCodes.ANSWER_SPLIT_TEXT,IMarkCodes.EMPTY_TEXT)
                        .replaceAll("[0-9]", IMarkCodes.EMPTY_TEXT);

            }

            str = str.replaceAll(IMarkCodes.ANSWER_SPLIT_TEXT,IMarkCodes.EMPTY_TEXT)
                    .replaceAll("[^0-9]", IMarkCodes.EMPTY_TEXT);


            str = getCaseConvertText(str);

            tempList.set(j,str);

        }

        // 정상적인 계산식이 들어왔을때만 후처리 한 것을 리턴 ( 앞 숫자 + 사칙연산 + 뒷 숫자 )
        if(tempList.size() == 2){

            result = tempList.get(0) + IMarkCodes.ANSWER_SPLIT_TEXT + key + IMarkCodes.ANSWER_SPLIT_TEXT + tempList.get(1)+etcStr;

        }else{

            result = paramMessage;

        }

        return result;

    }

    /**
     * 계산기 후처리 규칙
     *      - 숫자 사이 공백, 숫자 뒤 은, 는 텍스트는 제외함
     *      - 숫자가 a00b0c 형태인 경우, abc 값으로 변경 (세자리)
     *      - 숫자가 a00b0 형태인 경우, ab0 값으로 변경 (세자리)
     *      - 숫자가 a00c 형태인 경우, a0c 값으로 변경 (세자리)
     *      - 숫자가 a00 형태인 경우, a00 값으로 유지 (세자리)
     *      - 숫자가 b0c 형태인 경우, bc 값으로 변경 (두자리)
     *      - 숫자가 b0 형태인 경우, b0 값으로 유지 (두자리)
     *      - 숫자가 c 형태인 경우, c 값으로 유지 (한자리)
     * @param paramText 발화 문장
     * @return 규칙 조건에 맞게 변경된 발화 문장
     */
    private String getCaseConvertText(String paramText) throws  Exception{

        String zeroStr;
        String result = paramText;

        int tempLen = result.length();

        switch (tempLen){

            case 6 :

                String[] sixStrExtract = result.split(IMarkCodes.EMPTY_TEXT);

                zeroStr = sixStrExtract[1] + sixStrExtract[2] + sixStrExtract[4];

                // 숫자가 a00b0c 형태인 경우, abc 값으로 변경 (세자리)
                if(zeroStr.equals(IMathCodes.TRIPLE_ZERO_COUNT_TEXT)){

                    result = sixStrExtract[0] + sixStrExtract[3] + sixStrExtract[5];

                }

                break;

            case 5:

                String[] fiveStrExtract = result.split(IMarkCodes.EMPTY_TEXT);

                zeroStr = fiveStrExtract[1] + fiveStrExtract[2] + fiveStrExtract[4];

                // 숫자가 a00b0 형태인 경우, ab0 값으로 변경 (세자리)
                if(zeroStr.equals(IMathCodes.TRIPLE_ZERO_COUNT_TEXT)){

                    result = fiveStrExtract[0] + fiveStrExtract[3] + fiveStrExtract[4];

                }

                break;

            case 4:

                String[] fourStrExtract = result.split(IMarkCodes.EMPTY_TEXT);

                zeroStr = fourStrExtract[1] + fourStrExtract[2];

                // 숫자가 a00c 형태인 경우, a0c 값으로 변경 (세자리)
                if(zeroStr.equals(IMathCodes.TWO_ZERO_COUNT_TEXT)){

                    result = fourStrExtract[0] + fourStrExtract[1] + fourStrExtract[3];

                }

                break;

            case 3:

                String[] threeStrExtract = result.split(IMarkCodes.EMPTY_TEXT);

                zeroStr = threeStrExtract[1] + threeStrExtract[2];

                /*
                 * 숫자 3자리 변경
                 *    - a00 형태인 경우 , a00 값 유지
                 *    - b0c 형태인 경우, bc 값으로 변경
                 *
                 */

                // a00 형태
                if(zeroStr.equals(IMathCodes.TWO_ZERO_COUNT_TEXT)){

                    // 문자열 그대로 둔다.
                    // str = str;

                    // b0c 형태
                }else if(threeStrExtract[1].equals(IMathCodes.ONE_ZERO_COUNT_TEXT)
                        && !threeStrExtract[2].equals(IMathCodes.ONE_ZERO_COUNT_TEXT))
                {

                    result = threeStrExtract[0] + threeStrExtract[2];

                }

            case 2:
            case 1:

                // 문자열 그대로 둔다.
                // str = str;

                break;

            default:

                // 문자열 그대로 둔다.
                // str = str;
        }

        return result;

    }

}

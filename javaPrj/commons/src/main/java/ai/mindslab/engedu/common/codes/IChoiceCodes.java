package ai.mindslab.engedu.common.codes;

public interface IChoiceCodes {

    public static final String RESULT_Y = "Y";
    public static final String RESULT_N = "N";
}

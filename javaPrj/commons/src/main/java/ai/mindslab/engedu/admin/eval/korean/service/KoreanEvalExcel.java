package ai.mindslab.engedu.admin.eval.korean.service;

import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalAdminVO;
import ai.mindslab.engedu.admin.eval.english.service.EnglishEvalAdminService;
import ai.mindslab.engedu.admin.eval.korean.dao.data.KoreanEvalAdminVO;
import ai.mindslab.engedu.common.component.CommonExcelComponent;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class KoreanEvalExcel {

    @Autowired
    CommonExcelComponent excelComponent;

    @Autowired
    KoreanEvalAdminService service;

    private Workbook workbook;
    private String title;


    public Workbook createExcel(Map<String, Object> paramMap) throws EngEduException {

        int[] columnWiths ={300, 3000, 4000, 4000, 6000, 6000, 3000};
        excelComponent.init();
        excelComponent.setSheetPrintSetting();

        //컬럼 width 세팅
        excelComponent.setColumnWidth(columnWiths);
        //title.
        excelComponent.createTitle(this.title, 1,4);

        List<String> column = new ArrayList<>();

        String[] columnMap ={"ID", "학습자 ID", "평가 일시", "정답텍스트", "발화텍스트", "평가점수"};

        List<KoreanEvalAdminVO> list = service.korEvalAdminExcelList(paramMap);

        int rowCount = excelComponent.getRowCount();

        rowCount++;
        excelComponent.createColumnRow(rowCount, columnMap);

        long start = System.currentTimeMillis();


        for(KoreanEvalAdminVO vo : list){
            String[] data = {
                    vo.getReadId(),
                    vo.getUserId(),
                    vo.getCreatedTime(),
                    vo.getAnswerText(),
                    vo.getUserText(),
                    vo.getScore()
            };

            excelComponent.createDataRow(data);
        }

        this.workbook = excelComponent.getWorkBook();

        long end = System.currentTimeMillis();
        System.out.println("writeHSSFWorkbook : "+(end-start));

        return this.workbook;
    }


    public void setTitle(String title){
        this.title = title;
    }



}

package ai.mindslab.engedu.game.dao.data;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class EndToEndUsedWordVO implements Serializable {
	private String userId;
	private String word;
	private String lastThumb;
	private String kind;
	private int seq;
	private Date createDt;
	
}

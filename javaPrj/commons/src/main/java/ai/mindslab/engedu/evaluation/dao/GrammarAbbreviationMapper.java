package ai.mindslab.engedu.evaluation.dao;

import ai.mindslab.engedu.evaluation.dao.data.GrammarEvaluationAbbreviationVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GrammarAbbreviationMapper {

    List<GrammarEvaluationAbbreviationVO> getGrammarAbbreviationList(String brandId);


}

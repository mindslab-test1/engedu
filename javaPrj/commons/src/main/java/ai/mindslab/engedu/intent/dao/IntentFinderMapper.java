package ai.mindslab.engedu.intent.dao;

import ai.mindslab.engedu.intent.dao.data.UserServiceVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.Map;

@Mapper
public interface IntentFinderMapper {
	UserServiceVO getCurrentIntent(Map<String, String> serviceMap);
	int updateServiceType(Map<String, String> serviceMap);
	int purgeServiceType(String userId);
	String getRegexOfServiceType(String serviceType);
	ArrayList<String> getServiceTypeList();
	int updateDicsTimeOut(Map<String, String> serviceMap);
}

package ai.mindslab.engedu.intent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;

import ai.mindslab.engedu.game.dao.data.EndToEndUsedWordVO;
import ai.mindslab.engedu.game.dao.data.EndToEndVO;
import ai.mindslab.engedu.game.service.EndToEndService;
import ai.mindslab.engedu.intent.service.IntentFinderService;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class EndToEndIntentExecute implements IntentExecute {
	
	@Autowired
	private EndToEndService endToEndService;
	
	@Autowired
	private IntentFinderService intentFinderService;

	@Autowired
	private IntentMsgService intentMsgService;

	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {

		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();

		try {
			String userId 	= intentExecuteVO.getUserId();
			String inputStr = intentExecuteVO.getInputStr();
			inputStr = inputStr.replace(" ", "");
			Integer seq 	= endToEndService.getMaxSeq(userId);
			boolean isFirst = ObjectUtils.isEmpty(seq);
			String startMsg = null;
			ExtInfo extInfo = new ExtInfo();
			extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0018.getDomainName());
			if (isFirst) {
				// 끝말잇기 시작 문구 가져오기
				startMsg = intentMsgService.getServiceMsg(IntentServiceType.ENDTOEND, IntentServiceMsg.ENDTOEND_START);
				seq = 0;

				extInfo.setPrintMeans( intentMsgService.getServiceMsg(IntentServiceType.ENDTOEND, IntentServiceMsg.ENDTOEND_START_PRINTMENT) );
				
				
			} else {

				EndToEndUsedWordVO previousAiWord = endToEndService.getUsedWord(userId, seq, null);
			/*
			// 5초 체크
			Date currentTime = new Date();
			Date previousWordTime = previousAiWord.getCreateDt();
			Calendar c = Calendar.getInstance(); c.setTime(previousWordTime); c.add(Calendar.SECOND, 5); previousWordTime = c.getTime();

			if(currentTime.compareTo(previousWordTime) > 0) {
				// 학습자 답변이 5초 동안 없는 경우
				// 종료 프로세스
				return gameEndProcess(IntentServiceMsg.ENDTOEND_TIMELIMIT, null, userId);
			}
			*/

				// 단어 음절 체크
				if(!validateLastThumb(inputStr, previousAiWord)) {
					// 끝말을 잘못 이은 경우
					// 종료 프로세스
					return gameEndProcess(IntentServiceMsg.ENDTOEND_WRONGWORD, previousAiWord.getLastThumb(), userId);
				}


				// 단어 DB에서 체크
				EndToEndVO userWord = endToEndService.getSearch(inputStr);
				if(ObjectUtils.isEmpty(userWord)) {
					// 올바르지 않은 단어를 말한 경우
					// 종료 프로세스
					return gameEndProcess(IntentServiceMsg.ENDTOEND_NOWORD_IN_DB, null, userId);
				}

				// 사용자 단어를 세션에서 체크
				EndToEndUsedWordVO usedWord = endToEndService.getUsedWord(userId, null, inputStr);
				if(!ObjectUtils.isEmpty(usedWord)) {
					// 한 세션에서 같은 단어를 두 번 말한 경우
					// 종료 프로세스
					return gameEndProcess(IntentServiceMsg.ENDTOEND_DUPLICATEWORD, inputStr, userId);
				}

				// 15회 이상시에만 체크
				if(seq >= 15) {
					// 30회 이상시에는 반드시 또는 15~30일시에는 절반으로 랜덤
					if(Math.random() < 0.5 || seq >= 30) {
						// 종료 프로세스 - 사용자 승리
						return gameEndProcess(IntentServiceMsg.ENDTOEND_USERWIN, null, userId);
					}
				}

				// 사용자 단어 저장
				endToEndService.insertUsedWord(userId, userWord, "U", ++seq);
			}

			intentExecuteMessageVO.setExtInfo(extInfo);


			// 나이 가져오기 - Client에게 API로 가져오기
			int userAge = this.getUserAge(userId);

			List<String> levelList = getLevelList(userAge);

			EndToEndUsedWordVO previousUserWord = endToEndService.getUsedWord(userId, seq, null);
			List<String> lastThumb = null;
			if(!ObjectUtils.isEmpty(previousUserWord)) {
				lastThumb = new ArrayList<>();
				String[] lastThumbArr = previousUserWord.getLastThumb().split(",");

				Collections.addAll(lastThumb, lastThumbArr);
			}

			EndToEndVO aiWord = endToEndService.getEndToEndWord(userId, levelList, lastThumb);
			if(ObjectUtils.isEmpty(aiWord)) {
				// 종료 프로세스 - 사용자 승리 (DB의 유효 단어없음)
				return gameEndProcess(IntentServiceMsg.ENDTOEND_USERWIN, null, userId);
			}
			// AI 단어 저장
			endToEndService.insertUsedWord(userId, aiWord, "A", ++seq);


			String resultMsg = aiWord.getWord();
			if(isFirst && startMsg != null) {	// 게임 처음 시작시
				resultMsg = startMsg.replace("_endToendword_", aiWord.getWord());
			}

			log.info("EndToEndIntentExecute ResultMsg:"+resultMsg);
			intentExecuteMessageVO.setResultMsg(resultMsg);

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) throws EngEduException {

		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();

		try {
			String exitType = intentExecuteVO.getExitType();
			if(!StringUtils.isEmpty(exitType) && StringUtils.equalsIgnoreCase(exitType, "T")) {
				intentExecuteMessageVO = new IntentExecuteMessageVO();

				String resultMsg = intentMsgService.getServiceMsg(IntentServiceType.ENDTOEND, IntentServiceMsg.ENDTOEND_TIMELIMIT);
				intentExecuteMessageVO.setResultMsg(resultMsg);
			}

			// 사용 세션 단어들 삭제
			endToEndService.deleteUsedWords(intentExecuteVO.getUserId());

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	// userLevel에 따른 단어 레벨 리스트 생성
	private List<String> getLevelList(int userAge) {
		List<String> levelList = new ArrayList<>();
		levelList.add("A");
		if(userAge > 8) {
			levelList.add("B");
		}
		if(userAge > 11) {
			levelList.add("C");
		}
		
		return levelList;
	}
	
	// 마지막 음절 체크 로직
	private boolean validateLastThumb(String inputStr, EndToEndUsedWordVO previousAiWord) {
		boolean isValid = false;
		String[] lastThumbList = previousAiWord.getLastThumb().split(",");
		
		for(String s : lastThumbList) {
			if(StringUtils.startsWith(inputStr, s)) {
				isValid = true;
			}
		}
		
		return isValid;
	}
	
	// 종료 프로세스
	private IntentExecuteMessageVO gameEndProcess(String type, String word, String userId) {
		
		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();
		String resultMsg = intentMsgService.getServiceMsg(IntentServiceType.ENDTOEND, type);
		
		switch(type)
		{
			case IntentServiceMsg.ENDTOEND_DUPLICATEWORD:
				resultMsg = resultMsg.replace("_endToendword_", word);
				break;
			case IntentServiceMsg.ENDTOEND_WRONGWORD:
				resultMsg = resultMsg.replace("_last_thumb_", word);
				break;
		}
		
		// 사용 세션 단어들 삭제
		endToEndService.deleteUsedWords(userId);
		
		// 게임 종료시 서비스 타입 null처리
		intentFinderService.purgeServiceType(userId);
		
		log.info("EndToEndIntentExecute ResultMsg:"+resultMsg);
		ExtInfo extInfo = new ExtInfo();
		extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0018.getDomainName());
		intentExecuteMessageVO.setResultMsg(resultMsg);
		intentExecuteMessageVO.setExtInfo(extInfo);

		return intentExecuteMessageVO;
	}

	// 사용자 나이 Client에 API로 가져오기
	private int getUserAge(String userId) {

		int age=0;
		String ageStr = "";
		String uri = "https://stu.ideepstudy.com/appv2/api/member/age.do";

		HttpHeaders headers = new HttpHeaders();
		headers.set("charset", "UTF-8");

		MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<>();
		paramMap.add("stuId", userId);

		HttpEntity<MultiValueMap<String, String>> httpEntityRequest = new HttpEntity<>(paramMap, headers);

		try {
			ResponseEntity<Map> mapResponse = restTemplate.exchange(uri, HttpMethod.POST, httpEntityRequest, Map.class);

			log.info("getUserAge getStatusCode:"+mapResponse.getStatusCode());
			log.info("getUserAge getBody:"+mapResponse.getBody());
			String clientResult = (String) mapResponse.getBody().get("result");
			Object obj = mapResponse.getBody().get("data");

			if (HttpStatus.OK == mapResponse.getStatusCode() && "success".equals(clientResult)) {
				ageStr = (String) obj;
				
				try {
					age = Integer.parseInt(  ageStr);
				} catch (Exception e) {
					age = -1;
				}
			}
		} catch (Exception e){
			log.warn("EndToEndIntentExecute ClientApiException", e);
			age = -1;
		}

		return age;
	}

}

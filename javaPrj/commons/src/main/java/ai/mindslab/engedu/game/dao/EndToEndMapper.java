package ai.mindslab.engedu.game.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.game.dao.data.EndToEndUsedWordVO;
import ai.mindslab.engedu.game.dao.data.EndToEndVO;

@Mapper
public interface EndToEndMapper {

	EndToEndVO getSearch(Map<String, String> hashMap);
	
	Integer getMaxSeq(String userId);
	
	EndToEndVO getEndToEndWord(Map<String, Object> hashMap);
	
	int insertUsedWord(Map<String, Object> hashMap);
	
	EndToEndUsedWordVO getUsedWord(Map<String, Object> hashMap);
	
	int deleteUsedWords(String userId);
}

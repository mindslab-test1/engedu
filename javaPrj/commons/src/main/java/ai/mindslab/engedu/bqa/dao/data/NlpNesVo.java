package ai.mindslab.engedu.bqa.dao.data;

import lombok.Data;

@Data
public class NlpNesVo {
	private int textSeq;
	private String nes;
	private int nesSeq;
	private String nesType;
	private String nesDesc;
}

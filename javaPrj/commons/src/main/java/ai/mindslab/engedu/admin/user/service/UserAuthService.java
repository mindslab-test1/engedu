package ai.mindslab.engedu.admin.user.service;

import ai.mindslab.engedu.admin.user.dao.UserAuthMapper;
import ai.mindslab.engedu.admin.user.dao.data.UserAuthVO;
import ai.mindslab.engedu.admin.user.dao.data.UserInfoSVO;
import ai.mindslab.engedu.admin.user.dao.data.UserRoleVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class UserAuthService {

    @Autowired
    UserAuthMapper mapper;


    public int selectUserAuthCount(Map<String, Object> paramMap) throws EngEduException{
        return mapper.selectUserAuthCount(paramMap);
    }


    public List<UserAuthVO> selectUserAuthInfo(Map<String, Object> paramMap) throws EngEduException{
        return mapper.selectUserAuthInfo(paramMap);
    }


    public List<UserRoleVO> getUserRole() throws EngEduException{
        return mapper.getUserRole();
    }

    public UserAuthVO selectUserAuth(Map<String, Object> paramMap) throws EngEduException{
        return mapper.selectUserAuth(paramMap);
    }



    public BaseResponse<Object> updateUserAuth(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.updateUserAuth(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }

    public int checkExistUser(Map<String, Object> paramMap) throws EngEduException{
        return mapper.checkExistUser(paramMap);
    }


    public BaseResponse<Object> addUserAuth(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.addUserAuth(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }




     public BaseResponse<Object> deleteUserAuth(Map<String, Object> paramMap) throws EngEduException {

         BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
         JsonObject obj = new JsonObject();

         try {

             String unique = paramMap.get("unique").toString();

             UserInfoSVO[] keys = new Gson().fromJson(unique, UserInfoSVO[].class);

             int result = 0;

             for(UserInfoSVO svo : keys){
                 paramMap.put("userId", svo.getUserId());

                 result = mapper.deleteUserAuth(paramMap);

                 if (result > 0) {
                     resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                     resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
                 }else{
                     resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                     resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
                     throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
                 }
             }

         } catch (Exception e) {
             resp.setCode(IRestCodes.ERR_CODE_FAILURE);
             resp.setMsg(e.getMessage());
         }

         return resp;
     }

}

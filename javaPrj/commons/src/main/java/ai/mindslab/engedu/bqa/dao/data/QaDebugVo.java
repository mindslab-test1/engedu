package ai.mindslab.engedu.bqa.dao.data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ai.mindslab.engedu.bqa.commons.codes.IBasicQAConst;
import lombok.Data;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({ "createdDtm" })
@Data
public class QaDebugVo implements Serializable, IBasicQAConst {

	@Override
	public String toString() {
		return "QaDebugVo [usedDomainName=" + usedDomainName + ", usedDomainId=" + usedDomainId + ", answerId="
				+ answerId + ", answer=" + answer + ", question=" + question + ", questionMorph=" + questionMorph
				+ ", createdDtm=" + createdDtm + ", list=" + list + ", totalCount=" + totalCount + ", SearchFlowType="
				+ SearchFlowType + ", SearchFlowTypeName=" + SearchFlowTypeName + ", morphRange=" + morphRange
				+ ", debugYn=" + debugYn + ", status=" + status + ", result=" + result + "]";
	}

	private String usedDomainName;
	private int usedDomainId;
	private int answerId;
	private String answer;
	private String question;
	private String questionMorph;
	private LocalDateTime createdDtm;
	private List<QaIndexVo> list;
	private int totalCount;
	private int SearchFlowType;
	private String SearchFlowTypeName;
	private int morphRange;
	private String debugYn = "N";
	private int status;
	private String result;

	public QaDebugVo() {
		this.createdDtm = LocalDateTime.now();
	}

	public void setSearchFlowType(int searchFlowType) {
		SearchFlowType = searchFlowType;
		if (searchFlowType == SEARCH_TYPE_DB_SOLRAND) {
			SearchFlowTypeName = "DB,SOLRAND";
		} else if (searchFlowType == SEARCH_TYPE_DB) {
			SearchFlowTypeName = "DB";
		} else if (searchFlowType == SEARCH_TYPE_SOLRAND) {
			SearchFlowTypeName = "SOLRAND";
		} else if (searchFlowType == SEARCH_TYPE_DB_SOLRAND_SOLROR) {
			SearchFlowTypeName = "DB,SOLRAND,SOLROR";
		}
	}

	public void setStatus(int status) {
		this.status = status;

		switch (status) {
		case -1:
			setResult(" SEARCH RESULT : DB FAIL");
			break;
		case 1:
			setResult(" SEARCH RESULT : DB SUCCESS");
			break;
		case -2:
			setResult(" SEARCH RESULT : SOLRAND FAIL");
			break;
		case 2:
			setResult(" SEARCH RESULT : SOLRAND SUCCESS");
			break;
		case -3:
			setResult(" SEARCH RESULT : SOLROR FAIL");
			break;
		case 3:
			setResult(" SEARCH RESULT : SOLROR SUCCESS");
			break;

		default:
			break;
		}

	}

	public void setList(List<QaIndexVo> list) {
		this.totalCount = list.size();
		this.list = list;
	}
}

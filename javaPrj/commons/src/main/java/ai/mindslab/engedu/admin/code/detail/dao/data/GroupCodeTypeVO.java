package ai.mindslab.engedu.admin.code.detail.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class GroupCodeTypeVO implements Serializable {
    private String groupCode;
    private String codeName;
}

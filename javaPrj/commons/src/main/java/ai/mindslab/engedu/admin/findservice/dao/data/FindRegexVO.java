package ai.mindslab.engedu.admin.findservice.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class FindRegexVO implements Serializable {

    private String serviceType;
    private String detailName;
    private String regexWord;
}

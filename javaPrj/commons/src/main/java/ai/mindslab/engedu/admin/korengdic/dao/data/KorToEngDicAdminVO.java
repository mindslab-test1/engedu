package ai.mindslab.engedu.admin.korengdic.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class KorToEngDicAdminVO implements Serializable {
    private String korId;
    private String kor;
    private String eng;
    private int priority;
}

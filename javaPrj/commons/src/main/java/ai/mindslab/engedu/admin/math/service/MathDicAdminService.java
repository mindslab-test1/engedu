package ai.mindslab.engedu.admin.math.service;

import ai.mindslab.engedu.admin.korengdic.dao.data.KorToEngDicAdminVO;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.math.dao.MathDicAdminMapper;
import ai.mindslab.engedu.admin.math.dao.data.MathDicAdminSVO;
import ai.mindslab.engedu.admin.math.dao.data.MathDicAdminVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.ExcelUtils;
import com.google.gson.JsonObject;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@Service
public class MathDicAdminService {

    @Autowired
    MathDicAdminMapper mathDicAdminMapper;

    public int getMathDicAdminCount(Map<String, Object> paramMap) throws EngEduException {
        //log.info(new Object(){}.getClass().getEnclosingMethod().getName()+">>>>"+kortoEngDicMapper.getCount());
        return mathDicAdminMapper.getMathDicAdminCount(paramMap);
    }

    public List<MathDicAdminVO> getMathDicAdminList(Map<String, Object> paramMap) throws EngEduException {
        return mathDicAdminMapper.getMathDicAdminList(paramMap);
    }

    public MathDicAdminVO selectDetail(Map<String, Object> paramMap) throws EngEduException {
        return mathDicAdminMapper.selectDetail(paramMap);
    }

    public BaseResponse<Object> updateMathDicAdmin(Map<String, Object> paramMap) throws EngEduException {

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {
            int result = mathDicAdminMapper.updateMathDicAdmin(paramMap);

            if(result > -1){
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        } catch (Exception e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }

    public BaseResponse<Object> deleteMathDicAdmin(Map<String, Object> paramMap) throws EngEduException {

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {
            int result = mathDicAdminMapper.deleteMathDicAdmin(paramMap);

            if(result > -1){
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        } catch (Exception e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }

    public List<MathDicAdminVO> getExcelMathDicAdminList(Map<String, Object> paramMap) throws EngEduException {
        return mathDicAdminMapper.getExcelMathDicAdminList(paramMap);
    }



    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> uploadExcelData(UserVO vo, MultipartFile file) throws  EngEduException {
        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);

        Workbook workbook = ExcelUtils.getExcelWorkBook(file);
        Sheet sheet = (Sheet) workbook.getSheetAt(0);

        int rowCount = sheet.getPhysicalNumberOfRows();
        int cellCount = 0;
        int lastCellNum = 0;

        try {

            for (int i = 0; i < rowCount; i++) {
                Row row = sheet.getRow(i);

                if (row != null) {
                    if (i == 0) {
                        lastCellNum = row.getLastCellNum();

                        if (lastCellNum != 3) {
                            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                            resp.setMsg("Excel 양식이 잘못되었습니다.");
                            break;
                        }
                    } else {

                        MathDicAdminSVO mVo = new MathDicAdminSVO();

                        mVo.setWord(ExcelUtils.getCellValue(row.getCell(0)));
                        mVo.setMeans(ExcelUtils.getCellValue(row.getCell(1)));
                        mVo.setUrl(ExcelUtils.getCellValue(row.getCell(2)));
                        mVo.setUserId(vo.getUserId());

                        mathDicAdminMapper.insetMathDicAdmin(mVo);

                    }
                }
            }
        }catch (Exception e){
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
        }

        return resp;
    }

}

package ai.mindslab.engedu.dic.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.dic.dao.data.KorDicAntonymVO;

@Mapper
public interface KorDicAntonymMapper {
	int getCount();

	List<KorDicAntonymVO> getSearch(Map<String, String> hashMap);
}

package ai.mindslab.engedu.admin.tts.dao.data;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TtsLanguageVO {

    private String langText;

    private String langValue;

}

package ai.mindslab.engedu.bqa.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.bqa.dao.data.QaQuestionVo;

@Mapper
public interface QaQuestionMapper {

	int getCount();

	int getCountByDomain(Map<String, Object> paramMap);
	
	int update(QaQuestionVo qaQnaVo);

	int updateUseYn(Map<String, Object> paramMap);

	List<QaQuestionVo> gets(Map<String, Object> paramMap);
	
	int insertList(Map<String, Object> paramMap);
}

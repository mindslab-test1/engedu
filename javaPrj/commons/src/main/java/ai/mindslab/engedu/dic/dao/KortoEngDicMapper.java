package ai.mindslab.engedu.dic.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.dic.dao.data.KorToEngDicVO;

@Mapper
public interface KortoEngDicMapper {
	int getCount();
	List<KorToEngDicVO> getSearch(Map<String, String> hashMap);

}

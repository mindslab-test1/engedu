package ai.mindslab.engedu.admin.engtalk.dao;

import ai.mindslab.engedu.admin.engtalk.dao.data.answer.EngTalkAdminAnswerSVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.answer.EngTalkAdminAnswerVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface EngFreeTalkAnswerAdminMapper {

    int EngTalkAnswerAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<EngTalkAdminAnswerVO> EngTalkAnswerAdminList(Map<String, Object> paramMap) throws EngEduException;
    List<EngTalkAdminAnswerVO> EngTalkAnswerExcelList(Map<String, Object> paramMap) throws EngEduException;
    EngTalkAdminAnswerVO EngTalkAnswerAdmin(Map<String, Object> paramMap) throws EngEduException;
    int updateEngTalkAnswerAdmin(Map<String, Object> paramMap) throws EngEduException;
    int deleteEngTalkAnswerAdmin(Map<String, Object> paramMap) throws EngEduException;
    int getEngTalkAnswerId()throws EngEduException;
    int insertEngTalkAnswerAdmin(EngTalkAdminAnswerSVO svo) throws EngEduException;
}

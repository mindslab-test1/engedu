package ai.mindslab.engedu.admin.qa.domain.service;

import ai.mindslab.engedu.admin.qa.domain.dao.QADomainAdminMapper;
import ai.mindslab.engedu.admin.qa.domain.dao.data.QADomainTreeVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class QADomainAdminService {

    @Autowired
    QADomainAdminMapper mapper;

     public List<QADomainTreeVO> getDomainParent(Map<String, Object> param) throws EngEduException{
         return mapper.getDomainParent(param);
     }


     public List<QADomainTreeVO> getDomainSub(Map<String, Object> param) throws EngEduException{
         return mapper.getDomainSub(param);
     }

     public  QADomainTreeVO selectDomain(Map<String, Object> param) throws EngEduException{
          return mapper.selectDomain(param);
     }



     public BaseResponse<Object> insertDomainAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.insertDomainAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }



     public BaseResponse<Object> updateDomainAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.updateDomainAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }


     @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> deleteDoaminAdmin(Map<String, Object> paramMap){

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {

            int result = mapper.deleteDomainAdmin(paramMap);

            if(result > -1){
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }
        } catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }

}

package ai.mindslab.engedu.common.codes;

public interface IMathCodes {

    public static final String PLUS_SYMBOL = "+";
    public static final String PLUS_TEXT = "더하기";
    public static final String MINUS_SYMBOL = "-";
    public static final String MINUS_TEXT = "빼기";
    public static final String MUL_SYMBOL = "*";
    public static final String MUL_TEXT = "곱하기";
    public static final String DIV_SYMBOL = "/";
    public static final String DIV_TEXT= "나누기";

    public static final String ONE_ZERO_COUNT_TEXT= "0";
    public static final String TWO_ZERO_COUNT_TEXT= "00";
    public static final String TRIPLE_ZERO_COUNT_TEXT = "000";

    public static final String ONE_NUMBER_TEXT = "1";

    public static final String ONE_STRING_TEXT = "원";

}

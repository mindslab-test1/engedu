package ai.mindslab.engedu.admin.qa.domain.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class QADomainTreeVO implements Serializable {
    private Long domainId;
    private String domainName;
    private String descript;
    private int parent;
    private int parentDomainId;
    private int depth;
    private boolean loaded;
    private boolean lev;
    private int zlevel;
    private boolean expanded;
}

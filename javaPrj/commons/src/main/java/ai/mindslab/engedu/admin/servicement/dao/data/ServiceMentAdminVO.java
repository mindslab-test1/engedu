package ai.mindslab.engedu.admin.servicement.dao.data;
import lombok.Data;
import java.io.Serializable;

@Data
public class ServiceMentAdminVO  implements Serializable {

    private int seq;
    private String serviceType;
    private String code;
    private String msg;

}

package ai.mindslab.engedu.common.codes;

public interface IMindsChatBotCodes {

    public static final String NOAH = "Noah";
    public static final String WEATHER = "Weather";
    public static final String DUST = "Dust";
    public static final String EXCHANGE_RATE = "Exchange_rate";
    public static final String SEASONAL_FOOD = "Seasonal_food";
}

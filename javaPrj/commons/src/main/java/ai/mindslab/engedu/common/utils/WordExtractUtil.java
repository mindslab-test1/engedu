package ai.mindslab.engedu.common.utils;

import ai.mindslab.engedu.intent.IntentServiceType;

public class WordExtractUtil {
	
	
	public String extractWord(String serviceType, String input) {
		
		String slice = input;
		String retVal;
		
		// 즉시 사용일 경우 단어만 분리하기 위해서 사용함 
		if ( IntentServiceType.KORTOENGDIC_FLASH.equals(serviceType) || IntentServiceType.KORDIC_FLASH.equals(serviceType) || IntentServiceType.MATHDIC_FLASH.equals(serviceType) || 
				IntentServiceType.ENCYCLOPDIC_FLASH.equals(serviceType) || IntentServiceType.KORDICSYNONYM_FLASH.equals(serviceType) || IntentServiceType.KORDICANTONYM_FLASH.equals(serviceType)  ) {
			if (slice.contains("에서")) {
				slice = slice.substring(slice.indexOf("에서")+2).trim();
			} 
			
			if ( slice.lastIndexOf("찾아") > -1 ) {
				slice = slice.substring(0, slice.lastIndexOf("찾아")).trim();
			} 
			
		} 
		//retVal = slice.replaceAll(" ", "").replaceAll("\\p{Z}", "");
		retVal = slice;
		
		return retVal;
		
	}
	
	/*public List<String> wordSplit(String inputWord) {
		
		List<String> list = new ArrayList<>();

		String strArr [] = inputWord.split(" ");
		
		for (int i = 2; i < (strArr.length); i++) {
			list.add( strArr[i] );
		}
		
		if (strArr.length >= 2) {
			for (int i = 1; i < (strArr.length-1); i++) {
				list.add( strArr[i] );
				
			}
		} else {
			list.add( strArr[0] );
		}
		
		return list;
	}
	
	public String removeLastIndex(String str) {
		
		String retVal = "";
		
		if ( str.lastIndexOf("찾아") > -1 ) {
			retVal = str.substring(0, str.lastIndexOf("찾아")).trim();
		} else {
			retVal = str;
		}
		
		return retVal;
	}
*/
}

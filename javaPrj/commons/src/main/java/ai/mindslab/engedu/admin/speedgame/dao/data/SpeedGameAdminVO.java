package ai.mindslab.engedu.admin.speedgame.dao.data;

import lombok.Data;

import java.io.Serializable;


@Data
public class SpeedGameAdminVO implements Serializable {

	private Integer speedId;
	private String word;
	private String sttModel;
	private String useYn;

}

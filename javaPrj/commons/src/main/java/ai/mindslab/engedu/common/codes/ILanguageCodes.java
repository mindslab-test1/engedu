package ai.mindslab.engedu.common.codes;

public interface ILanguageCodes {

    public static final String KOR = "KOR";
    public static final String ENG = "ENG";

    public static final String KO_KR = "ko_KR";
    public static final String EN_US = "en_US";

}

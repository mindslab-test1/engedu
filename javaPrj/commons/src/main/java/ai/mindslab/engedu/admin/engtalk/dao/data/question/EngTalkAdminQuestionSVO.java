package ai.mindslab.engedu.admin.engtalk.dao.data.question;

import lombok.Data;

import java.io.Serializable;

@Data
public class EngTalkAdminQuestionSVO implements Serializable {

    private String brandId;
    private String bookId;
    private String chapterId;
    private String questionId;
    private String questionText;
    private String useYn;
    private String creatorId;
    private String updatorId;

}

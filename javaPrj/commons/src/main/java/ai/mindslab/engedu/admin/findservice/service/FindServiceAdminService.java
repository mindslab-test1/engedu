package ai.mindslab.engedu.admin.findservice.service;

import ai.mindslab.engedu.admin.findservice.dao.FindServiceAdminMapper;
import ai.mindslab.engedu.admin.findservice.dao.data.FindRegexVO;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.servicement.dao.data.SelectOptVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.ExcelUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FindServiceAdminService {

    @Autowired
    FindServiceAdminMapper mapper;

    public List<SelectOptVO> selectTypeList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.selectTypeList(paramMap);
    }

    public int getFindServiceAdminCount(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getFindServiceAdminCount(paramMap);
    }

    public List<FindRegexVO> getFindServiceAdminList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getFindServiceAdminList(paramMap);
    }

    public List<FindRegexVO> getFindServiceExcelList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getFindServiceExcelList(paramMap);
    }


    public FindRegexVO getFindServiceAdmin(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getFindServiceAdmin(paramMap);
    }


    public BaseResponse<Object> updateFindServiceAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.updateFindRegexAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }



    public BaseResponse<Object> deleteFindServiceAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.deleteFindRegexAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }


    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> uploadExcelData(UserVO vo, MultipartFile file) throws  EngEduException {
        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);

        Workbook workbook = ExcelUtils.getExcelWorkBook(file);
        Sheet sheet = (Sheet) workbook.getSheetAt(0);

        int rowCount = sheet.getPhysicalNumberOfRows();
        int lastCellNum = 0;

        try {

            for (int i = 0; i < rowCount; i++) {
                Row row = sheet.getRow(i);

                if (row != null) {
                    if (i == 0) {
                        lastCellNum = row.getLastCellNum();

                        if (lastCellNum != 2) {
                            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                            resp.setMsg("Excel 양식이 잘못되었습니다.");
                            break;
                        }
                    } else {

                       Map<String, Object> mapVO = new HashMap<>();

                        mapVO.put("service_type", ExcelUtils.getCellValue(row.getCell(0)));
                        mapVO.put("regex_word",ExcelUtils.getCellValue(row.getCell(1)));
                        mapVO.put("user_id", vo.getUserId());

                        mapper.insertFindServiceAdmin(mapVO);

                    }
                }
            }
        }catch (Exception e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
        }

        return resp;
    }

}

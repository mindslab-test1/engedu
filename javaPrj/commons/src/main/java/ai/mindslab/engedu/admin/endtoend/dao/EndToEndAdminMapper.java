package ai.mindslab.engedu.admin.endtoend.dao;

import ai.mindslab.engedu.admin.endtoend.dao.data.EndToEndAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.game.dao.data.EndToEndVO;

import java.util.List;
import java.util.Map;

public interface EndToEndAdminMapper {

    int getEndToEndAdminListCount(Map<String, Object> paramMap) throws EngEduException;
    List<EndToEndAdminVO> getEndToEndAdminList(Map<String, Object> paramMap) throws EngEduException;
    EndToEndAdminVO selectEndToEndAdmin(Map<String, Object> paramMap) throws EngEduException;
    int updatEndtoEndAdmin(Map<String, Object> paramMap) throws EngEduException;
    int deleteEndToEndAdmin(Map<String, Object> paramMap) throws EngEduException;
    List<EndToEndAdminVO> getEndToEndExcelList(Map<String, Object> paramMap) throws EngEduException;
    int insertEndToEndAdmin(EndToEndAdminVO vo) throws EngEduException;
}

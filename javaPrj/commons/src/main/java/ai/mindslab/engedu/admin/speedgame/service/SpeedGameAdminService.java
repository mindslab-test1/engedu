package ai.mindslab.engedu.admin.speedgame.service;


import ai.mindslab.engedu.admin.speedgame.dao.SpeedGameAdminMapper;
import ai.mindslab.engedu.admin.speedgame.dao.data.SpeedGameAdminSVO;
import ai.mindslab.engedu.admin.speedgame.dao.data.SpeedGameAdminVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SpeedGameAdminService {

    @Autowired
    SpeedGameAdminMapper mapper;

    public  int getSpeedGameAdminCount(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getSpeedGameAdminCount(paramMap);
    }

    public List<SpeedGameAdminVO> getSpeedGameAdminList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getSpeedGameAdminList(paramMap);
    }


    public SpeedGameAdminVO getSpeedGameAdmin(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getSpeedGameAdmin(paramMap);
    }



    public BaseResponse<Object> updateSpeedGameAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.updateSpeedGameAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }


    public BaseResponse<Object> deleteSpeedGameAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.deleteSpeedGameAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }

    public BaseResponse<Object> insertSpeedGameAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            String insertData = paramMap.get("data").toString();
            String userId =  paramMap.get("user_id").toString();

            SpeedGameAdminSVO svo = new Gson().fromJson(insertData, SpeedGameAdminSVO.class);

            svo.setCreatorId(userId);
            svo.setUpdatorId(userId);

            int result = mapper.insertSpeedGameAdmin(svo);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }

}
